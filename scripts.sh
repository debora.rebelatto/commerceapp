#!/bin/bash

GREEN=`tput setaf 2`
reset=`tput sgr0`

PS3='Please enter your choice: '
options=(
  "One time generation *.g.dart files"
  "Continuously build generation"
  "Splash Screen"
  "Launcher Icon"
  "Clean"
  "Quit"
)

select opt in "${options[@]}"
do
  case $opt in
    "One time generation *.g.dart files")
      echo "${GREEN}https://flutter.dev/docs/development/data-and-backend/json#one-time-code-generation ${reset}"
      flutter clean
      flutter packages pub upgrade
      flutter pub run build_runner build --delete-conflicting-outputs
      ;;
    "Continuously build generation")
      echo "${GREEN}https://flutter.dev/docs/development/data-and-backend/json#generating-code-continuously ${reset}"
      flutter clean
      flutter packages pub upgrade
      flutter pub run build_runner watch --delete-conflicting-outputs
      ;;
    "Splash Screen")
      echo "${GREEN}More https://pub.dev/packages/flutter_native_splash ${reset}"
      flutter pub pub run flutter_native_splash:create
      break
      ;;
    "Launcher Icon")
      echo "${GREEN}https://pub.dev/packages/flutter_launcher_icons ${reset}"
      flutter pub run flutter_launcher_icons:main -f flutter_launcher_icons.yaml
      break
      ;;
    "Clean")
      flutter clean
      break
      ;;
    "Quit")
      break
      ;;
    *) echo "invalid option $REPLY";;
  esac
done