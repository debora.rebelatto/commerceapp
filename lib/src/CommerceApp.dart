import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:commerce_app/src/Controllers/SharedPrefController.dart';
import 'AppThemes.dart';
import 'Templates/Mobile/Login/Login.dart';
import 'Templates/Web/InitialScreen/InitialScreenWidget.dart';
import 'Templates/Web/UndefinedRoute.dart';

class CommerceApp extends StatefulWidget {
  @override
  _CommerceAppState createState() => _CommerceAppState();
}

class _CommerceAppState extends State<CommerceApp> {
  var initialwidget;

  @override
  void initState() {
    super.initState();
  }

  void removeData() async {
    await SharedPrefController.excluiArquivoUsuario();
  }

  @override
  Widget build(BuildContext context) {

    return AdaptiveTheme(
      light: AppThemes.lightTheme,
      dark: AppThemes.darkTheme,
      initial: AdaptiveThemeMode.light,
      builder: (theme, darkTheme) => MaterialApp(
        title: 'Commerce App',
        theme: theme,
        debugShowCheckedModeBanner: false,
        home: Scaffold( body: kIsWeb ? InitialScreenWidget() : Login() ),
        onUnknownRoute: (settings) => MaterialPageRoute(
          builder: (context) => UndefinedRoute(name: settings.name,)
        ),
      )
    );
  }
}
