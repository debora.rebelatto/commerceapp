import 'dart:convert';
import '../API/URL.dart';
import '../Model/ItemCarrinho/ItemCarrinho.dart';

class ItemCarrinhoServices{
  static Future getItemCarrinhoPreco() async => await URL.httpGET('/api/ItemCarrinhos');

  static Future postItemCarrinhoPreco() async => await URL.httpPOST('/api/ItemCarrinhos');

  static Future getItemCarrinhoById(String id) async => await URL.httpGET('/api/ItemCarrinhos/$id');

  static Future updateItem(String id, { var body } ) async =>
    await URL.httpPUT('/api/ItemCarrinhos/' + id, body: jsonEncode(body));

  static Future deleteItemCarrinhoById(String id) async => await URL.httpDELETE('/api/ItemCarrinhos/' + id);

  static Future postIncrementoCarrinho(int quantidade, int produtoId, double? valor) async {
    var response = await URL.httpPOST('/api/ItemCarrinhos/InsereIncrementaItemCarrinhoEmAberto', body: jsonEncode({
      'quantidade': quantidade, 'unitario': valor, 'produtoId': produtoId
    }));
    return response.statusCode;
  }

  static Future updateItemCarrinho(String id, int quantAtual) async {
    var item = ItemCarrinho.fromJson(await getItemCarrinhoById(id));
    item.quantidade = quantAtual.toDouble();

    return await URL.httpPUT('/api/ItemCarrinhos/' + id, body: jsonEncode(item));
  }
}