import '../API/URL.dart';

class TabelaPrecoServices{
  static Future getTabelaPreco() async => await URL.httpGET('/api/ItemTabelaPrecos');
}
