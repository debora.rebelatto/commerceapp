import 'dart:convert';
import 'package:http/http.dart';
import '../API/URL.dart';

abstract class DistribuidorServices{
  static Future getDistribuidor(String email, String senha) async {
    Response response =
      await URL.httpPOST('/api/Distribuidores/Usuario', body: jsonEncode({'email': email, 'senha': senha}));
    return jsonDecode(response.body);
  }
}