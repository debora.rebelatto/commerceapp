import 'dart:convert';

import '../API/URL.dart';

abstract class UsuariosServices {
  static Future postAutenticar(String email, String senha, { int? distribuidor }) async {
    var body = distribuidor != null
      ? jsonEncode({ 'email': email, 'senha': senha, 'distribuidor': distribuidor })
      : jsonEncode({ 'email': email, 'senha': senha });

    try {
      return await URL.httpPOST( '/api/Usuarios/autenticar', body: body );
    } catch (err) {
      return jsonEncode({'err': 'Erro de conexão com o servidor'});
    }
  }

  static Future postForgotPassword(String email) async {
    var response = await URL.httpPOST('/api/Usuarios/validarEmail', body: jsonEncode({'email': email}));
    if(response.statusCode == 200) {
      var resDecode = json.decode(response.body);
      return await putGerarAcesso(body: jsonEncode({'usuario': resDecode['usuario'], 'email': resDecode['email']}));
    } else {
      return response;
    }
  }

  static Future postValidaEmail(String email) async {
    try {
      return await URL.httpPOST('/api/Usuarios/validarEmail', body: jsonEncode({'email': email}));
    } catch(err) {
      return jsonEncode({ 'err': err });
    }
  }

  static Future putGerarAcesso({ dynamic body }) async => await URL.httpPUT('/api/Usuarios/gerarAcesso', body: body );

  static Future getUsuarioData(String id) async => await URL.httpGET('/api/Usuarios/$id');

  static Future updatePassword(int id, String oldPassword, String newPassword) async =>
    await URL.httpPUT( '/api/Usuarios/alterarSenha/$id', body: jsonEncode({ 'senha': oldPassword, 'novaSenha': newPassword }));
}
