import '../API/URL.dart';

abstract class GruposServices {
  static Future getGrupos() async => await URL.httpGET('/api/Grupos');
}