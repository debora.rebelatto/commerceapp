import '../API/URL.dart';

abstract class UsuariosClienteServices{
  static Future postUsuariosCliente(dynamic body) async => await URL.httpPOST('/api/Usuarios/cliente', body: body);
}
