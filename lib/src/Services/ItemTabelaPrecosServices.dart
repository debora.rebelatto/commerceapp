import '../API/URL.dart';

abstract class ItemTabelaPrecosServices {
  static Future getItemTabelaPrecos() async => await URL.httpGET('/api/ItemTabelaPrecos');
}