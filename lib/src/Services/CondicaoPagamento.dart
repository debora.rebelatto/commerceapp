import '../API/URL.dart';

abstract class CondicaoPagamentoServices {
  static Future getCondicaoPagamento() async => await URL.httpGET('/api/CondicaoPagamentos');
}