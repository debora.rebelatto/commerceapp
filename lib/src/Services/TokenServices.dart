import 'dart:convert';
import 'dart:io';
import '../API/URL.dart';
import 'package:http/http.dart' as http;

class TokenServices {
  static Future postToken(var token) async => await http.post(
    await URL.buildHost() + '/api/Token/atualizar',
    headers: {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.acceptHeader: 'application/json',
      HttpHeaders.authorizationHeader: 'Bearer $token',
    },
    body: jsonEncode({ 'token': token })
  );
}
