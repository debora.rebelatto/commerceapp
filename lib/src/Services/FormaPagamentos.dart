import '../API/URL.dart';

abstract class FormaPagamentosServices {
  static Future getFormaPagamentos() async => await URL.httpGET('/api/FormaPagamentos');
}