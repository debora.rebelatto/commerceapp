import 'dart:convert';

import '../API/URL.dart';
import '../Model/Carrinho/Carrinho.dart';

class CarrinhoServices{
  static Future getCarrinho() async => await URL.httpGET('/api/Carrinhos');

  static Future retornaCarrinhoAberto() async => await URL.httpGET('/api/Carrinhos/CarrinhoEmAberto');

  static Future getCarrinhoById(String id) async => await URL.httpGET('/api/Carrinhos/' + id);

  static Future updateCarrinhoById(String idCarrinho, int idCond, int idForm, String date) async {
    var carrinho = Carrinho.fromJson(await getCarrinhoById(idCarrinho));

    carrinho.condicaoPagamentoId = idCond;
    carrinho.formaPagamentoId = idForm;
    carrinho.dataEntrega = date;

    return await URL.httpPUT('/api/Carrinhos/' + idCarrinho, body: jsonEncode(carrinho));
  }

  static Future deleteCarrinho(String id) async => await URL.httpDELETE('/api/Carrinhos/' + id);

  static Future postFinalizeCarrinho(String id) async {
    try {
      var response = await URL.httpPOST('/api/Carrinhos/Finalizar/' + id);
      return response.statusCode;
    } catch (err) {
      return err;
    }
  }
}