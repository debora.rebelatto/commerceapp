import '../API/URL.dart';

abstract class MarcasServices{
  static Future getMarcas() async => await URL.httpGET('/api/Marcas');
}

