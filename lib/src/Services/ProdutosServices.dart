import '../API/URL.dart';

abstract class ProductsServices{
  static Future getProdutos() async => await URL.httpGET('/api/Produtos/ItemTabelaPreco');

  static Future getProdutosById(String id) async => await URL.httpGET('/api/Produtos/$id');

  static Future getItemTabelaPreco() async => await URL.httpGET('/api/Produtos/ItemTabelaPreco');

  static Future getItemTabelaPrecoById(String id) async => await URL.httpGET('/api/Produtos/ItemTabelaPreco/' + id);

  static Future getProdutoItemTabelaPreco({String? query}) async => query != null
    ? await URL.httpGET('/api/Produtos/ItemTabelaPreco' + query)
    : await URL.httpGET('/api/produtos/itemtabelapreco');
}
