import '../API/URL.dart';

abstract class ClienteUsuariosServices {
  static Future getClienteUsuarioDados() async =>
    await URL.httpGET('/api/ClienteUsuarios/cliente');

  static Future getClienteUsuario() async =>
    await URL.httpGET('/api/ClienteUsuarios');

  static Future postClienteUsuarioLiberacao(int id) async =>
    await URL.httpPOST('/api/ClienteUsuarios/$id/liberacao');

  static Future postClienteUsuarioBloqueio(int id) async =>
    await URL.httpPOST('/api/ClienteUsuarios/$id/bloqueio');
}