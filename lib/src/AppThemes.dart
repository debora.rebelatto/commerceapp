import 'package:flutter/material.dart';
import 'package:commerce_app/src/core/AppStyles.dart';

class AppThemes {
  static final lightTheme = ThemeData (
    backgroundColor: Colors.white60,
    cardColor: Colors.white,
    floatingActionButtonTheme: FloatingActionButtonThemeData(
      backgroundColor: AppStyles.darkBlue
    ),
    inputDecorationTheme: InputDecorationTheme(
      labelStyle: TextStyle(color: Colors.black)
    ),
    textTheme: TextTheme( headline1: TextStyle(color: Colors.black) ),
    primaryTextTheme: TextTheme(
      headline1: TextStyle( color: Colors.black ),
      subtitle1: TextStyle( color: Colors.black ),
      subtitle2: TextStyle( color: Color(0xff191919) )
    ),
    appBarTheme: AppBarTheme( color: AppStyles.darkBlue ),
    bottomAppBarColor: Colors.white,
    iconTheme: IconThemeData(color: Colors.black87),
    brightness: Brightness.light,
  );

  static final darkTheme = ThemeData (
    floatingActionButtonTheme: FloatingActionButtonThemeData(
      backgroundColor: AppStyles.darkBlue
    ),

    appBarTheme: AppBarTheme( color: Color.fromRGBO(64, 75, 96, 1) ),
    backgroundColor: Colors.grey[200],
    cardColor: Color(0xFF353941),
    inputDecorationTheme: InputDecorationTheme(
      labelStyle: TextStyle(color: Colors.black)
    ),
    primaryTextTheme: TextTheme(
      headline1: TextStyle( color: Colors.white ),
      subtitle1: TextStyle( color: Colors.white ),
      subtitle2: TextStyle( color: Colors.white )
    ),
    bottomAppBarColor: Colors.black,
    brightness: Brightness.dark
  );
}

