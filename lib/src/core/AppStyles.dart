import 'package:flutter/material.dart';

class AppStyles {
  static TextStyle boldWebText(size, { color }) => TextStyle(
    fontSize: size,
    fontFamily: 'Axiforma-Black',
    fontWeight: FontWeight.bold,
    color: color ?? Colors.black
  );

  static final darkBlue = Color.fromRGBO(20, 33, 61, 1);
  static final opacityBlue = Color.fromRGBO(20, 33, 61, .9);
  static final red = Color(0xfff33733);
  static final slightlyDarkerRed = Color(0xffDD3930);
}