import 'package:commerce_app/src/Model/CondicaoPagamento/CondicaoPagamento.dart';
import '../API/URL.dart';
import '../Model/FormaPagamento/FormaPagamento.dart';

class PaymentController {
  static Future<List<CondicaoPagamento>> fetchCondicaoPagamento(String id) async {
    var response = await URL.httpGET('/api/CondicaoPagamentos/Carrinho/$id');
    var parsed = response.cast<Map<String, dynamic>>();
    return parsed.map<CondicaoPagamento> ((json) => CondicaoPagamento.fromJson(json)).toList();
  }

  static Future<List<FormaPagamento>> fetchFormaPagamento(String id) async {
    var response = await URL.httpGET('/api/FormaPagamentos/Carrinho/$id');
    final parsed = response.cast<Map<String, dynamic>>();
    return parsed.map<FormaPagamento> ((json) => FormaPagamento.fromJson(json)).toList();
  }

  static Future<List<FormaPagamento>> fetchFormaPagamentoByCondicaoPagamento(cardId, selectedCondicaoId) async {
    var response = await URL.httpGET('/api/formapagamentos/carrinho/$cardId/condicaopagamento/$selectedCondicaoId');
    final parsed = response.cast<Map<String, dynamic>>();
    return parsed.map<FormaPagamento> ((json) => FormaPagamento.fromJson(json)).toList();
  }
}