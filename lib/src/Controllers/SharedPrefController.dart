import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:commerce_app/src/Model/Usuario/Usuario.dart';
import 'package:commerce_app/src/Services/UsuariosServices.dart';

abstract class SharedPrefController {
  static Future<bool?> saveSharedPreferences(String email, senha, remember) async {
    var pref = await SharedPreferences.getInstance();

    await pref.setBool('logged', true);
    await pref.setBool('remember', remember);

    remember == true ? await pref.setString('cacheEmail', email) : pref.remove('cacheEmail');

    await pref.setString('usuario', 'D');
    await pref.setBool('remember', remember);
    remember == true ? await pref.setString('cacheEmail', email) : pref.remove('cacheEmail');

    await pref.setInt('id', 1);
    await pref.setString('nome', 'Debora');
    await pref.setString('email', 'debora@email.com');

    return pref.getBool('logged');
  }

  static dynamic getToken() async {
    var pref = await SharedPreferences.getInstance();
    return pref.getString('token');
  }

  static void userInfo(distribuidor, remember, email) async {
    var pref = await SharedPreferences.getInstance();
    await pref.setBool('remember', remember);
    remember == true ? await pref.setString('cacheEmail', email) : pref.remove('cacheEmail');
    await pref.setString('nomeDist', distribuidor[0].pessoaJuridica.fantasia);
  }

  static Future<String?> checkIfEmailSaved() async {
    var pref = await SharedPreferences.getInstance();
    return pref.getString('cacheEmail');
  }

  static Future<bool?> checkRemember() async {
    var pref = await SharedPreferences.getInstance();
    return pref.getBool('remember');
  }

  static void  setDistribuidor(distribuidor) async {
    var pref = await SharedPreferences.getInstance();
    await pref.setInt('distibuidor', distribuidor);
  }

  static dynamic excluiArquivoUsuario() async {
    var pref = await SharedPreferences.getInstance();

    await pref.setBool('logged', false);
    await pref.remove('usuario');
    await pref.remove('nome');
    await pref.remove('email');
    await pref.remove('token');
    await pref.remove('id');
    await pref.remove('nomeDist');
  }
}