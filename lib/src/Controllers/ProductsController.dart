import 'package:shared_preferences/shared_preferences.dart';
import '../Services/ProdutosServices.dart';
import '../Model/ProdutoItemTabelaPreco/ProdutoItemTabelaPreco.dart';

class ProductsController {
  static Future<List<ProdutoItemTabelaPreco>>
  fillProdutoList({ int? page, int? grupo, int? marca }) async {
    var pref = await SharedPreferences.getInstance();

      var query = '?';
    var response;
    
    if(pref.getString('quantityFilter') != null) {
      query = query + 'Registros=${pref.getString('quantityFilter')}&';
    } else {
      query = query + 'Registros=20&';
    }

    query = query + 'Pagina=${page ?? 1}&';

    if(pref.getString('orderFilter') != null) { query = query + 'Tipo=${pref.getString('orderFilter')}&'; }
    if(pref.getString('fieldFilter') != null) { query = query + 'Ordem=${pref.getString('fieldFilter')}&'; }
    if(grupo != null) { query = query + 'GrupoId=$grupo'; }
    if(marca != null) { query = query + 'MarcaId=$marca'; }

    response = await ProductsServices.getProdutoItemTabelaPreco(query: query);

    print(response);

    final parsed = response.cast<Map<String, dynamic>>();

    print(parsed);

    return parsed.map<ProdutoItemTabelaPreco> ((json) => ProdutoItemTabelaPreco.fromJson(json)).toList();
  }

  static Future<ProdutoItemTabelaPreco> getItem(String id) async =>
    ProdutoItemTabelaPreco.fromJson(await ProductsServices.getItemTabelaPrecoById(id));

  static Future<List<ProdutoItemTabelaPreco>> fetchProdutoItemTabelaPreco(int? page, { int? grupo, int? marca }) async {
    var query = '?Registros=20&Pagina=${page ?? 1}&';

    if(grupo != null) { query = query + 'GrupoId=$grupo'; }
    if(marca != null) { query = query + 'MarcaId=$marca'; }


    var response = await ProductsServices.getProdutoItemTabelaPreco(query: query);
    final parsed = response.cast<Map<String, dynamic>>();
    return parsed.map<ProdutoItemTabelaPreco> ((json) => ProdutoItemTabelaPreco.fromJson(json)).toList();
  }

  static Future<List<ProdutoItemTabelaPreco>> search(String query) async {
    var response = await ProductsServices.getProdutoItemTabelaPreco(query: '?Nome=$query');
    final parsed = response.cast<Map<String, dynamic>>();
    return parsed.map<ProdutoItemTabelaPreco> ((json) => ProdutoItemTabelaPreco.fromJson(json)).toList();
  }
}