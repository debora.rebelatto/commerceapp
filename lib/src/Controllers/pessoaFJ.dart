import 'dart:convert';

String pessoaFisica(widget) {
  return jsonEncode({
    'usuario': {
      'pessoa': {
        'tipo': widget.type,
        'pessoaFisica': {
          'cpf': widget.userCpfController.text,
          'rg': widget.rgController.text,
          'nome': widget.nameController.text,
          'apelido': widget.nameController.text
        }
      },
      'email': widget.emailController.text,
      'senha': widget.passwordController.text
    },
    'cliente': {
      'pessoa': {
        'tipo': widget.type,
        'pessoaFisica': {
          'cpf': widget.cpfController.text,
          'rg': widget.rgController.text,
          'nome': widget.nameController.text
        }
      },
      'codigo': 0
    }
  });
}

String pessoaJuridica(widget){
  return jsonEncode({
    'usuario': {
      'pessoa': {
        'tipo': 'F',
        'pessoaFisica': {
          'cpf': widget.userCpfController.text,
          'rg': widget.rgController.text,
          'nome': widget.nameController.text,
          'apelido': widget.nameController.text
        }
      },
      'email': widget.emailController.text,
      'senha': widget.passwordController.text
    },
    'cliente': {
      'pessoa': {
        'tipo': widget.type,
        'pessoaJuridica': {
          'cnpj': widget.cnpjController.text
        }
      },
      'codigo': 0
    }
  });
}