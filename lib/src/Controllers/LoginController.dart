import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:commerce_app/src/Templates/Web/HomePageWeb/WidgetWebHomePage.dart';
import 'package:commerce_app/src/Templates/Mobile/HomePage/WidgetPageView.dart';
import 'package:commerce_app/src/Templates/Web/InitialScreen/InitialScreenWidget.dart';
import 'SharedPrefController.dart';

abstract class LoginController {
  static Future login(BuildContext context, String email, String senha, bool remember) async {
    try {
      // var response = await UsuariosServices.postAutenticar(email, senha);

      // if(response.runtimeType == String && jsonDecode(response)['err'] != null) {
      //   return jsonDecode(response)['err'];
      // }

      // if(response.statusCode == 200) {
        await authenticate(context, email, senha, remember);
      // }
    } catch (err) {
      return 'Não foi possível concluir Login';
    }
  }

  static Future<dynamic> authenticate(BuildContext context, String email, senha, remember) async {
    await SharedPrefController.saveSharedPreferences( email, senha, remember);
    kIsWeb
      ? await Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (context) => InitialScreenWidget())
      )
      : await Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (context) => WidgetPageView() )
      );
  }

  static dynamic checkTokenValidity(context) async {
    var token = await SharedPrefController.getToken();

    try {
      if(!JwtDecoder.isExpired(token)) {
        if(kIsWeb) {
          await Navigator.of(context).push(
            MaterialPageRoute(builder: (context) => WidgetWebHomePage())
          );
        } else {
          await Navigator.of(context).push(
            MaterialPageRoute(builder: (context) {
              return WidgetPageView();
            })
          );
        }
      }
    } catch (err) {
      await SharedPrefController.excluiArquivoUsuario();
    }
  }
}
