import 'package:easy_debounce/easy_debounce.dart';
import 'package:commerce_app/src/Model/Carrinho/Carrinho.dart';
import 'package:commerce_app/src/Model/ItemCarrinho/ItemCarrinho.dart';
import 'package:commerce_app/src/Model/ProdutoItemTabelaPreco/ProdutoItemTabelaPreco.dart';
import 'package:commerce_app/src/Services/CarrinhoServices.dart';
import 'package:commerce_app/src/Model/ItensCarrinho/ItensCarrinho.dart';
import 'package:commerce_app/src/Services/ItemCarrinhoServices.dart';
import 'package:commerce_app/src/Services/ProdutosServices.dart';
import 'package:commerce_app/src/Templates/Mobile/Carrinho/Components/snack.dart';

class CarrinhoController {
  static Future<List<Carrinho>> getCarrinhos() async {
    var response = await CarrinhoServices.getCarrinho();
    var parsed = response.cast<Map<String, dynamic>>();
    return parsed.map<Carrinho> ((json) => Carrinho.fromJson(json)).toList();
  }

  static Future<ItensCarrinho> getItensCarrinho() async {
    var carrinho = await CarrinhoServices.retornaCarrinhoAberto();
    return ItensCarrinho.fromJson(carrinho);

  }

  static Future<double?> getValorVenda(String id) async {
    var produtoItemTabelaPreco = ProdutoItemTabelaPreco.fromJson(await ProductsServices.getItemTabelaPrecoById(id));
    return produtoItemTabelaPreco.itemTabelaPreco!.valorVenda;
  }

  static dynamic decreaseDebounce(ItemCarrinho item, int quantidade, context, carrinho) async {
    EasyDebounce.debounce('reduce', Duration(milliseconds: 400), () async {
      var res = await ItemCarrinhoServices.updateItemCarrinho(item.id.toString(), quantidade);
      snack(context, item.produto!.nome, res, carrinho: carrinho);
    });
  }

  static dynamic increaseDebounce(item, int quantidade, context, carrinho) async {
    var unitario = await CarrinhoController.getValorVenda(item.produto.id.toString());

    EasyDebounce.debounce('add', Duration(milliseconds: 500), () async {
      var res = await ItemCarrinhoServices.postIncrementoCarrinho(
        quantidade - item.quantidade as int, item.produto.id, unitario
      );
      snack(context, item.produto.nome, res, carrinho: carrinho);
    });
  }
}