import 'package:shared_preferences/shared_preferences.dart';
import 'package:commerce_app/src/API/API_URL.dart';

class PortController {
  static void updateHostPort({ required String url, required String port }) async {
    var pref = await SharedPreferences.getInstance();
    await pref.setString('url', url);
    await pref.setString('port', port);
  }

  static Future<String> getHostPort() async {
    var pref = await SharedPreferences.getInstance();
    var url = pref.getString('url') ?? APIURL.defaultIp;
    var port = pref.getString('port') ?? APIURL.defaultPort;
    return url + port;
  }
}
