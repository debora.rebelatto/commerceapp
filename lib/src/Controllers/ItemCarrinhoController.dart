import 'package:commerce_app/src/Model/ItemCarrinho/ItemCarrinho.dart';
import 'package:commerce_app/src/Services/ItemCarrinhoServices.dart';

class ItemCarrinhoController {
  static Future updateItemCarrinhoReduce(String id, int quantAtual) async {
    var item = ItemCarrinho.fromJson(await ItemCarrinhoServices.getItemCarrinhoById(id));
    item.quantidade = quantAtual.toDouble();

    return await ItemCarrinhoServices.updateItem(id, body: item);
  }
}