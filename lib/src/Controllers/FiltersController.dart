import 'package:shared_preferences/shared_preferences.dart';

abstract class FiltersController {
  static dynamic cleanFilters() async {
    var pref = await SharedPreferences.getInstance();
    await pref.remove('fieldFilter');
    await pref.remove('orderFilter');
    await pref.remove('quantityFilter');
  }

  static dynamic filter(var field, var order, var quantity) async {
    var pref = await SharedPreferences.getInstance();
    await pref.setString('fieldFilter', field);
    await pref.setString('orderFilter', order);
    await pref.setString('quantityFilter', quantity);
  }

  static dynamic quantity(int quantity) async {
    var pref = await SharedPreferences.getInstance();
    await pref.setString('quantityFilter', quantity.toString());
  }
}