import 'package:flutter/material.dart';

Container emptySearchList(String name) {
  return Container(
    alignment: Alignment.center,
    padding: EdgeInsets.only(left: 10, right: 10),
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image.asset('images/search.png', height: 100 ),
        Text('\"$name\" não foi encontrado',
          textAlign: TextAlign.center, style: TextStyle(fontSize: 20)
        ),
        Container(
          padding: EdgeInsets.only(top: 10),
          child: Text('Tente procurar novamente!',
            textAlign: TextAlign.center, style: TextStyle(fontSize: 20, color: Colors.grey),
          )
        )
      ]
    )
  );
}