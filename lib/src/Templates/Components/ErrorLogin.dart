import 'package:flutter/material.dart';
import 'package:commerce_app/src/core/AppStyles.dart';

Widget errorLogin(var error) => error != ''
  ? Container(
    padding: EdgeInsets.fromLTRB(15, 20, 15, 0),
    child: Container(
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
        border: Border.all( color: AppStyles.red, width: 1.0 ),
        borderRadius: BorderRadius.all( Radius.circular(6) ),
        color: Colors.red[100],
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Icon(Icons.error, color: AppStyles.red),
          SizedBox(height: 10),
          Text(error,
            style: AppStyles.boldWebText(20, color: AppStyles.red),
            textAlign: TextAlign.center,
          ),
        ]
      )
    )
  )
  : Container();
