import 'package:flutter/material.dart';

// ignore: must_be_immutable
class ProfileInfo extends StatelessWidget {
  final String text;
  var info;

  ProfileInfo(this.text, this.info);

  @override
  Widget build(BuildContext context) {
    info ??= '';
    return Container(
      padding: EdgeInsets.all(5),
      child: RichText(
        overflow: TextOverflow.ellipsis,
        softWrap: false,
        maxLines: 2,
        text: TextSpan(
          text: text,
          style: TextStyle(
            color: Theme.of(context).primaryTextTheme.headline1!.color,
            fontWeight: FontWeight.bold,
            fontSize: 17
          ),
          children: [
            TextSpan( text: '$info',
              style: TextStyle(
                color: Theme.of(context).primaryTextTheme.headline1!.color,
                fontWeight: FontWeight.normal, fontSize: 17
              )
            ),
          ],
        ),
      ),
    );
  }
}