import 'dart:convert';
import 'dart:typed_data';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:commerce_app/src/API/URL.dart';
import 'package:http/http.dart' as http;
import 'package:commerce_app/src/Templates/Components/WidgetImages/errorImage.dart';

// ignore: must_be_immutable
class DownloadImage extends StatefulWidget {
  final String id;
  double? width;
  double? height;

  DownloadImage( this.id, { Key? key, this.width, this.height, }) : super(key: key);

  @override
  _DownloadImageState createState() => _DownloadImageState();
}

class _DownloadImageState extends State<DownloadImage>{
  var imageWidget;
  String? _base64;
  Uint8List? bytes;
  late bool waiting;

  void getImageMobile() async {
    Widget image = Image.network(
      '${await URL.buildHost()}/api/Imagens/Produto/${widget.id}',
      headers: await URL.buildHeader(),
      errorBuilder: (BuildContext context, Object exception, StackTrace? stackTrace) {
        return errorImage();
      },
    );
    setState(() { imageWidget = image; });
  }

  @override
  void initState() {
    super.initState();
    waiting = true;
    if(kIsWeb) {
      (() async {
        http.Response response = await URL.httpGETImage('/api/Imagens/Produto/${widget.id}');
        if (mounted){
          setState(() {
            _base64 = base64Encode(response.bodyBytes);
            bytes = base64Decode(_base64!);
          });
        }
      })();
    } else {
      getImageMobile();
    }
    waiting = false;
  }

  @override
  Widget build(BuildContext context) {
    if(!kIsWeb) {
      setState(() {
        widget.width = 150;
        widget.height = 150;
      });
    }
    return Container(
      padding: EdgeInsets.all(18),
      child: ConstrainedBox(
        constraints: BoxConstraints(
          maxWidth: widget.width ?? 200,
          maxHeight: widget.height ?? 200
        ),
        child: bytes == null && imageWidget == null ? loadingShimmer() : Center(
          child: !kIsWeb ? imageWidget : Image.memory(bytes!,
            errorBuilder: (BuildContext context, Object exception, StackTrace? stackTrace) {
              return errorImage();
            },
          )
        )
      )
    );
  }

  Widget loadingShimmer() {
    return Shimmer.fromColors(
      enabled: true,
      baseColor: Colors.grey[100]!,
      highlightColor: Colors.grey[300]!,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all( Radius.circular(6) ),
          color: Colors.red[100],
        ),
      ),
    );
  }
}

