import 'package:flutter/material.dart';

Widget errorImage() {
  return Column(
    mainAxisAlignment: MainAxisAlignment.center,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [
      Icon(Icons.photo_camera, color: Color(0xffc7c7c7), size: 30,),
      Text('Imagem Indisponível',
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 18),
      )
    ]
  );
}