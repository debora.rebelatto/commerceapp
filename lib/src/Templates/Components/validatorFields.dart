import 'package:dart_date/dart_date.dart';
import 'package:intl/intl.dart';

class FieldValidator {
  static String? validateIfEmpty(value) => value.isEmpty ? 'Preencher campo' : null;

  static String? validateDropDown(value) => value == null ? 'Preencher campo' : null;

  static String? validateCPF(value) {
    if(value.isEmpty) { return validateIfEmpty(value);
    } else if(RegExp(
      '[0-9]{3}[.][0-9]{3}[.][0-9]{3}[-][0-9]{2}'
    ).hasMatch(value) == false ) {
      return 'Formato Inválido';
    }
    return null;
  }

  static String? validateEmail(value) {
    if(value.isEmpty) {
      return 'Preencher campo.';
    } else if(RegExp(
      r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+"
    ).hasMatch(value) == false) {
      return 'Formato inválido.';
    }
    return null;
  }

  static String? validateCpfCnpj(value) {
    if(value.isEmpty) {
      return 'Preencher campo';
    } else if(RegExp(
      '([0-9]{3}[.][0-9]{3}[\/]?[0-9]{4}[-]?[0-9]{2})|([0-9]{3}[\.]?[0-9]{3}[\.]?[0-9]{3}[-]?[0-9]{2})'
    ).hasMatch(value) == false ) {
      return 'Formato Inválido';
    }
    return null;
  }

  static String? validatePassword(value){
    if(value.isEmpty) {
      return 'Preencher campo';
    } else if (value.length < 7) {
      return 'Mínimo de 7 caracteres';
    } else if ( RegExp('^(?=.*[0-9])').hasMatch(value) == false ) {
      return 'A senha deve conter um número';
    } else if ( RegExp( '(?=.*[A-Z])').hasMatch(value) == false ) {
      return 'A senha deve conter um caractere maiúsculo';
    }
    return null;
  }

  static String? confirmPasswordSignup(password, confirmPassword) {
    if(confirmPassword.isEmpty) {
      return 'Preencher campo';
    } else if(confirmPassword != password) {
      return 'As senhas precisam ser iguais!';
    }
    return null;
  }

  static String? confirmPassword(passwordField, value) {
    if(value.isEmpty) {
      return 'Preencher campo';
    } else if(value != passwordField) {
      return 'As senhas precisam ser iguais!';
    }
    return null;
  }

  static String? dateField(value) {
    try{
      var dateFormatted = DateFormat('dd/MM/yyyy').parse(value);
      if(value.isEmpty) {
        return 'Preencher campo';
      } else if(dateFormatted.isPast && !dateFormatted.isToday) {
        return 'Data Inválida';
      }
    } catch(err) {
      return 'Formato Inválido';
    }
    return null;
  }
}