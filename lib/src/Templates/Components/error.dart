import 'package:flutter/material.dart';

Widget error() {
  return Padding(
    padding: const EdgeInsets.all(15),
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(Icons.warning_outlined),
        SizedBox(height: 10),
        Text('Ocorreu um erro!')
      ],
    ),
  );
}