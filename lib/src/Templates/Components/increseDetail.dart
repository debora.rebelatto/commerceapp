import 'package:commerce_app/src/Templates/Mobile/Components/Snackbar.dart';
import 'package:commerce_app/src/Services/ItemCarrinhoServices.dart';

dynamic increse(item, quantidade) async {
  var mult = item.produto.quantidadeMultipla == 0 ? 1 : item.produto.quantidadeMultipla;

  if(int.parse(quantidade.text) % mult == 0 ) {
    var response = await ItemCarrinhoServices.postIncrementoCarrinho(
      int.parse(quantidade.text), item.produto.id, item.valorVenda
    );

    switch (response) {
      case 201:
        return showThisSnackBar('${item.produto.nome} adicionado ao carrinho.');
      case 204:
        return showThisSnackBar('${item.produto.nome} adicionado ao carrinho.');
      default:
        return showThisSnackBar('Erro ao Adicionar produto ao carrinho.');
    }
  } else {
    return showThisSnackBar('Quantidade múltipla deste produto é ${item.produto.quantidadeMultipla}.\nVerificar quantidade.');
  }
}