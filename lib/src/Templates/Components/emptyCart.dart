import 'package:flutter/material.dart';

class EmptyCart extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset('assets/images/shopping_cart.png', height: 100),
          SizedBox(height: 25),
          Text('Carrinho vazio!', style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold, color: Colors.black87)),
          SizedBox(height: 10),
          Text('Adicione itens para continuar', style: TextStyle(fontSize: 18, color: Colors.grey))
        ]
      )
    );
  }
}