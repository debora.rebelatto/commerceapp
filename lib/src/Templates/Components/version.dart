import 'package:flutter/material.dart';
import 'package:yaml/yaml.dart';
import 'package:flutter/services.dart';

Widget version() {
  return FutureBuilder(
    future: rootBundle.loadString('pubspec.yaml'),
    builder: (context, snapshot) {
      var version;
      if (snapshot.hasData) {
        var yaml = loadYaml(snapshot.data.toString());
        version = yaml['version'].split('+');
      }

      switch (snapshot.connectionState) {
        case ConnectionState.waiting: return Container();
        case ConnectionState.done:
          return Padding(
            padding: EdgeInsets.only(bottom: 20),
            child: Align(
              alignment: Alignment.bottomCenter,
              child: Text('${version[0]}.${version[1]}', style: TextStyle(color: Colors.grey)),
            )
          );
        default: return Container();
      }
    }
  );
}


