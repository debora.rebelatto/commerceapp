import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:commerce_app/src/Controllers/PaymentController.dart';
import 'package:commerce_app/src/Model/Carrinho/Carrinho.dart';
import 'package:commerce_app/src/Model/CondicaoPagamento/CondicaoPagamento.dart';
import 'package:commerce_app/src/Templates/Components/validatorFields.dart';
import 'package:commerce_app/src/Templates/provider/FinalizeData.dart';

import 'Components/FinalizeBottomNavigation.dart';
import 'Components/FinalizeDate.dart';

// ignore: must_be_immutable
class FinalizeCart extends StatefulWidget {
  final Carrinho cart;

  FinalizeCart({
    Key? key,
    required this.cart,
  }) : super(key: key);

  @override
  _FinalizeState createState() => _FinalizeState();
}

class _FinalizeState extends State<FinalizeCart>{
  var items;
  int? selectedFormaId;
  int? selectedCondicaoId;
  bool waiting = false;
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    Provider.of<FinalizeData>(context, listen: false).setCartId(widget.cart.id.toString());
    Provider.of<FinalizeData>(context, listen: false).setCondicaoId('');
    Provider.of<FinalizeData>(context, listen: false).setFormaId('');
    Provider.of<FinalizeData>(context, listen: false).setDate('');
  }

  void setCondicaoProvider(newValue) {
    Provider.of<FinalizeData>(context, listen: false).setCondicaoId(newValue);
  }

  void setFormaProvider(newValue) {
    Provider.of<FinalizeData>(context, listen: false).setFormaId(newValue);
  }

  void getData() async {
    setState(() { waiting = !waiting; });
    var data = await PaymentController.fetchFormaPagamentoByCondicaoPagamento(widget.cart.id, selectedCondicaoId!);
    setState(() {
      items = data.map((map) => DropdownMenuItem(value: map.id, child: Text(map.nome!))).toList();
      waiting = !waiting;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar( title: Text('Finalizar compra'), ),
      body: Form(
        key: _formKey,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        child: ListView(
          physics: BouncingScrollPhysics(),
          children: [
            ListTile(subtitle: Text('Condição de Pagamento')),

            ListTile(
              subtitle: FutureBuilder<List<CondicaoPagamento>>(
                future: PaymentController.fetchCondicaoPagamento(widget.cart.id.toString()),
                builder: (context, snapshot) => snapshot.hasError
                ? Container(child: Center(child: Icon(Icons.warning)))
                : snapshot.hasData
                  ? DropdownButtonFormField<dynamic>(
                    value: selectedCondicaoId,
                    hint: Text('Condição de Pagamento'),
                    onChanged: (dynamic newValue) {
                      setState(() {
                        selectedCondicaoId = newValue;
                        getData();
                        selectedFormaId = null;
                      });
                      setCondicaoProvider(selectedCondicaoId.toString());
                    },
                    validator: (value) => FieldValidator.validateDropDown(value),
                    items: snapshot.data!.map((map) => DropdownMenuItem(
                      value: map.id, child: Text(map.nome!)
                    )).toList(),
                  )
                  : Center(child: CircularProgressIndicator())
              ),
            ),

            ListTile(subtitle: Text('Forma de Pagamento')),

            ListTile(
              subtitle: Ink(child: InkWell(
                onTap: () { getData(); },
                child: DropdownButtonFormField<dynamic>(
                  value: selectedFormaId,
                  hint: Text('Forma de Pagamento'),
                  onChanged: (dynamic newValue) {
                    setState(() { selectedFormaId = newValue; });
                    setFormaProvider(selectedFormaId.toString());
                  },
                  validator: (value) => FieldValidator.validateDropDown(value),
                  items: items,
                )
              ))
            ),
            ListTile(subtitle: Text('Data de Entrega')),
            FinalizeDate(),
          ]
        )
      ),

      bottomNavigationBar: Container(
        color: Theme.of(context).bottomAppBarColor,
        padding: EdgeInsets.all(20),
        child: MaterialButton(
          color: Colors.blue,
          minWidth: 10,
          shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(20) ),
          onPressed: () async {
            if (_formKey.currentState!.validate()) {
              _formKey.currentState!.save();
              finalizeCart(context);
            }
          },
          child: Text('Finalizar', style: TextStyle(color: Colors.white))
        )
      )
    );
  }
}