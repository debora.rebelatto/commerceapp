import 'package:flutter/material.dart';
import 'package:jiffy/jiffy.dart';
import 'package:provider/provider.dart';
import 'package:commerce_app/src/Services/CarrinhoServices.dart';
import 'package:commerce_app/src/Templates/provider/FinalizeData.dart';
import '../../HomePage/HomePage.dart';

dynamic finalizeCart(BuildContext context) async {
  var res;

  if(res != 201) {
    dialog(
      context,
      CircularProgressIndicator(),
      'Aguarde. Finalizando carrinho.\nVocê será redirecionado para a página inicial.',
      false
    );
  }

  res = await onPressedSubmit(context);

  if(res == 201) {
    await Navigator.pushAndRemoveUntil(context, MaterialPageRoute(
      builder: (BuildContext context) => HomePage()),
      (Route<dynamic> route) => false
    );
  } else {
    Navigator.pop(context);
    dialog(context, Text('Erro!'), 'Ocorreu um erro. Tente novamente.', true);
  }
}

dynamic dialog(BuildContext context, Widget title, String content, bool dismiss) {
  showDialog(
    barrierDismissible: dismiss,
    context: context,
    builder: (context) {
      return AlertDialog(
        title: Center( child: title ),
        content: Text(content, textAlign: TextAlign.center )
      );
    },
  );
}

dynamic onPressedSubmit(context) async {
  var res;

  var resUpdate = await CarrinhoServices.updateCarrinhoById(
    Provider.of<FinalizeData>(context, listen: false).getCartId,
    int.parse(Provider.of<FinalizeData>(context, listen: false).getCondicaoId),
    int.parse(Provider.of<FinalizeData>(context, listen: false).getFormaId),
    Jiffy(Provider.of<FinalizeData>(context, listen: false).getDate, "y-MM-dd hh:mm:ss").format("y-MM-dd")
  );

  if(resUpdate == 204) {
    res = await CarrinhoServices.postFinalizeCarrinho(
      Provider.of<FinalizeData>(context, listen: false).getCartId
    );
  }

  return res;
}