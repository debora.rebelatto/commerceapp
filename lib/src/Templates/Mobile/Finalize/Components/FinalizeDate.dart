import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:provider/provider.dart';
import 'package:commerce_app/src/Templates/provider/FinalizeData.dart';

class FinalizeDate extends StatefulWidget{
  FinalizeDate({ Key? key }) : super(key: key);

  @override
  _DateState createState() => _DateState();
}

class _DateState extends State<FinalizeDate> {
  var date;

  @override
  void initState() {
    super.initState();
    date = DateTime.now();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    Provider.of<FinalizeData>(context, listen: false).setDate(date.toString());
  }

  void setDateProvider(String value) {
    Provider.of<FinalizeData>(context, listen: false).setDate(value);
  }

  @override
  Widget build(BuildContext context) {
    return ListTile(
      subtitle: Column( children: [
        DateTimeField(
          validator: (dateTime) => dateTime == null ? 'Date Time Required' : null,
          format: DateFormat('dd, MMMM, y'),
          initialValue: DateTime.now(),
          onChanged: (value) {
            setState(() { date = value!; });
            setDateProvider(date);
          },
          onShowPicker: (context, currentValue) async => showDatePicker(
            context: context,
            firstDate: DateTime.now(),
            initialDate: currentValue ?? DateTime.now(),
            lastDate: DateTime(2100)
          )
        ),
      ])
    );
  }
}