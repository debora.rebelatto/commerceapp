import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:commerce_app/src/Templates/Components/ProfileInfo.dart';
import 'package:commerce_app/src/core/AppStyles.dart';

class WidgetProfile extends StatefulWidget {
  WidgetProfile({ Key? key }) : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<WidgetProfile> {
  String? nome;
  String? email;
  String? distribuidor;

  @override
  void initState() {
    super.initState();
    setInfo();
  }

  dynamic setInfo() async {
    var pref = await SharedPreferences.getInstance();
    setState(() {
      nome = pref.getString('nome');
      email = pref.getString('email');
      distribuidor = pref.getString('nomeDist');
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Perfil')),
      body: Padding(
        padding: EdgeInsets.only(top: 20),
        child: SingleChildScrollView(
          child: Stack(
            alignment: Alignment.topCenter,
            children: [
              Container(
                constraints: BoxConstraints(minWidth: 200),
                width: double.infinity,
                padding: EdgeInsets.fromLTRB(20, 60, 20, 0),
                child: Align(
                  alignment: Alignment.topCenter,
                  child: Material(
                    elevation: 10,
                    color: Theme.of(context).cardColor,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.all( Radius.circular(30) )),
                    child: Stack(children: [ info() ])
                  ),
                ),
              ),
              profilePicture()
            ],
          )
        )
      )
    );
  }

  Padding info() => Padding(
    padding: EdgeInsets.fromLTRB(15, 110, 10, 20),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ProfileInfo('Nome: ', nome ?? ''),
        ProfileInfo('Email: ', email ?? ''),
      ]
    )
  );


  Align profilePicture() {
    return Align(
      alignment: Alignment.topCenter,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(20.0),
        child: Container(
          height: 150.0, width: 150.0,
          color: AppStyles.darkBlue,
          child: Center(
            child: Text(
              nome != null ? '${nome![0]}' : '0',
              style: TextStyle(color: Colors.white, fontSize: 50)
            )
          ),
        )
      )
    );
  }
}