import 'package:flutter/material.dart';

SnackBar showThisSnackBar(String text) => SnackBar(
  behavior: SnackBarBehavior.floating, duration: Duration(seconds: 1), content: Text(text)
);
