import 'package:flutter/material.dart';

Builder productInfo(String info, { bool? isBold, double? size }) {

  return Builder(builder: (context) =>
    Padding(
      padding: EdgeInsets.only(top: 5),
      child: SizedBox(
        width: MediaQuery.of(context).size.width - 168,
        child: Text( info,
          overflow: TextOverflow.ellipsis,
          softWrap: false,
          maxLines: 2,
          textAlign: TextAlign.left,
          style: TextStyle(
            fontSize: size ?? 18,
            color: Theme.of(context).primaryTextTheme.headline1!.color,
            fontWeight: isBold == true ? FontWeight.w600 : FontWeight.w500
          )
        )
      )
    )
  );
}
