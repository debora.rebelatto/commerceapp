import 'package:connectivity/connectivity.dart';

Future<bool> getConnection() async {
  var connectivityResult = await (Connectivity().checkConnectivity());
  return connectivityResult == ConnectivityResult.none ? false : true;
}