import 'package:flutter/material.dart';

dynamic cancel() => Builder(builder: (context) => TextButton(
  style: TextButton.styleFrom(
    shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(15.0) ),
    backgroundColor: Colors.red[700],
  ),
  onPressed: () async { Navigator.of(context).pop(); },
  child: Text('Cancelar', style: TextStyle( color: Colors.white) ),
));
