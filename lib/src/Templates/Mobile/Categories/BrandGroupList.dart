import 'package:flutter/material.dart';
import 'package:commerce_app/src/Controllers/ProductsController.dart';
import 'package:commerce_app/src/Model/ProdutoItemTabelaPreco/ProdutoItemTabelaPreco.dart';
import 'package:commerce_app/src/Templates/Mobile/Carrinho/FutureCarrinho.dart';
import 'package:commerce_app/src/Templates/Mobile/Products/ProdutoBody.dart';

class BrandGroupList extends StatefulWidget{
  final dynamic data;

  BrandGroupList(this.data, { Key? key }) : super(key: key);

  @override
  _BrandGroupListState createState() => _BrandGroupListState();
}

class _BrandGroupListState extends State<BrandGroupList> {
  List<ProdutoItemTabelaPreco> data = [];
  int currentPage = 1;
  bool loading = false;
  bool finished = false;
  final _controller = ScrollController();

  void _getData() async {
    setState(() { loading = true; });

    var list = widget.data == 'Marcas'
      ? await ProductsController.fetchProdutoItemTabelaPreco( currentPage, marca: widget.data.id )
      : await ProductsController.fetchProdutoItemTabelaPreco( currentPage, grupo: widget.data.id );

    list.isEmpty
      ? setState(() { finished = true; })
      : list.forEach((e) => { setState(() { data.add(e); }) });

    setState(() { loading = false; });
  }

  @override
  void initState() {
    super.initState();
    _getData();
    _controller.addListener(() async {
      if (_controller.position.pixels == _controller.position.maxScrollExtent) {
        setState(() { loading = true; });
        currentPage++;
        _getData();
        setState(() { loading = false; });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.data.nome),
        actions: [
          IconButton(
            tooltip: 'Abrir Carrinho', iconSize: 30,
            padding: EdgeInsets.only(left: 10, right: 10),
            icon: Icon(Icons.shopping_cart_outlined),
            onPressed: () async {
              await Navigator.of(context).push(MaterialPageRoute(builder: (context) => FutureCarrinho()));
            },
          )
        ]
      ),
      body: SingleChildScrollView(
        controller: _controller,
        physics: AlwaysScrollableScrollPhysics(),
        child: Column(
          children: [
            list(),

            finished
              ? Center(child: Padding(
                padding: const EdgeInsets.all(15),
                child: Text('Não há mais nada para mostrar'),
              ))
              : SizedBox(height: 0),

            loading && data.isNotEmpty
              ? CircularProgressIndicator()
              : SizedBox(height: 20)
          ],
        ),
      )
    );
  }


  Widget list() {
    if(loading == true && data.isEmpty) {
      return Center(child: CircularProgressIndicator());
    } else {
      return data.isEmpty
        ? Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(Icons.warning),
              Container(
                padding: EdgeInsets.only(top: 10),
                child: Text('Nenhum produto encontrado',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                )
              )
            ],
          )
        )
        : ProdutoBody(data);
    }
  }
}
