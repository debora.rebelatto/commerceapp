import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:commerce_app/src/Model/Configuracao/Configuracao.dart';
import 'package:commerce_app/src/Templates/Mobile/Categories/BrandGroupList.dart';
import 'package:commerce_app/src/Templates/Web/CategoriesWeb/FutureListWeb.dart';
import 'package:commerce_app/src/core/AppStyles.dart';

class Categories extends StatefulWidget {
  Categories({ Key? key }) : super(key: key);
  @override
  _CategoriesState createState() => _CategoriesState();
}

class _CategoriesState extends State<Categories> {
  Configuracao? conf;
  List<dynamic> data = [];
  bool loading = true;

  void _getData() async {
    setState(() { loading = true; });
    // await ConfiguracaoController.fetchConfiguracao().then((value) { setState(() { conf = value; }); });
    // var fetched = conf!.tipoMenuHome == 'G'
    //   ? await GrupoMarcaController.fetchGrupo()
    //   : await GrupoMarcaController.fetchMarca();
    setState(() {
      // data = fetched;
      loading = false;
    });
  }

  @override
  void initState() {
    super.initState();
    loading = true;
    _getData();
  }

  @override
  Widget build(BuildContext context) {
    return data.isEmpty && !loading
      ? Center(child: Icon(Icons.warning),)
      : SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Container(
          margin: EdgeInsets.only(top: 20.0),
          height: 100.0,
          child: Row(
            children: [
              Container(width: 30),
              Align(
                alignment: Alignment.topCenter,
                child: ListView.builder(
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  physics: BouncingScrollPhysics(),
                  itemCount: loading == true ? 20 : data.length,
                  itemBuilder: (context, index) {
                    return loading == true
                    ? Container(
                      height: 100,
                      width: 100,
                      margin: EdgeInsets.only(left: 5, right: 5),
                      child: loadingShimmer()
                    ) : clickable(context, index, data[index]);
                  }
                )
              ),
              Container(width: 30),
            ],
          )
        )
      );
  }

  Container clickable(BuildContext context, int index, dynamic data) {
    return Container(
      margin: EdgeInsets.only(left: 5, right: 5),
      width: 100.0,
      child: Material(
        child: Ink(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: AppStyles.darkBlue,
          ),
          child: InkWell(
            onTap: () {
              kIsWeb
              ? Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => FutureListWeb(data)
              ))
              : Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => BrandGroupList(data)
              ));
            },
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  padding: EdgeInsets.only(left: 10, right: 10),
                  child: Text( data.nome,
                    maxLines: 3, textAlign: TextAlign.center, overflow: TextOverflow.ellipsis,
                    style: TextStyle( color: Colors.white, fontWeight: FontWeight.bold )
                  )
                )
              ]
            )
          )
        )
      )
    );
  }
}

Widget loadingShimmer() {
  return Shimmer.fromColors(
    enabled: true,
    baseColor: Colors.grey[100]!,
    highlightColor: Colors.grey[300]!,
    child: Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all( Radius.circular(6) ),
        color: Colors.red[100],
      ),
    ),
  );
}