import 'package:flutter/material.dart';
import 'package:commerce_app/src/Controllers/LoginController.dart';
import 'package:commerce_app/src/Controllers/SharedPrefController.dart';
import 'package:commerce_app/src/Templates/Components/ErrorLogin.dart';
import 'package:commerce_app/src/Templates/Components/version.dart';
import 'package:commerce_app/src/Templates/Mobile/Components/getConnection.dart';
import 'Components/ForgotPasswordButton.dart';
import 'Components/LoginFields.dart';
import 'Components/Logo.dart';
import 'Components/SignupButton.dart';

class LoginForm extends StatefulWidget {
  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final _email = TextEditingController(text: '');
  final _password = TextEditingController(text: '');
  final _formKey = GlobalKey<FormState>();
  String error = '';
  bool waiting = false;
  bool remember = false;

  dynamic checkConnection() async {
    await getConnection().then((value) {
      setState(() {
        if(value == false) error = 'Erro de conexão com a internet!';
      });
    });
  }

  @override
  void initState() {
    super.initState();
    checkConnection();
    getEmail();
  }

  void getEmail() async {
    var email = await SharedPrefController.checkIfEmailSaved();
    var newRemember = await SharedPrefController.checkRemember();
    setState(() {
      _email.text = email ?? '';
      remember = newRemember ?? false;
    });
  }

  @override
  void dispose() {
    _email.dispose();
    _password.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox( height: 20 ),
        Logo(),
        Container(
          width: MediaQuery.of(context).size.width,
          child: errorLogin(error),
        ),
        SizedBox( height: 5 ),
        Form(
          key: _formKey,
          child: LoginFields(_email, _password)
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [ rememberUser(), ForgotPasswordButton(_email.text) ]
        ),
        SizedBox( height: 4 ), login(),
        SizedBox( height: 8 ), SignupButton(),
        SizedBox( height: 10 ), version(),
        SizedBox( height: 10 ),
      ]
    );
  }

  Widget rememberUser() {
    return Expanded( child: Material(
      color: Colors.transparent,
      child: Ink(
        child: InkWell(
        onTap: () { setRemember(); },
        child: Row(children: [
          Checkbox( value: remember, onChanged: (value) { setRemember(); }),
          Text('Lembrar-se de mim', style: TextStyle(color: Colors.white),)
        ],)
      ),),
    ));
  }

  void setRemember() { setState(() { remember = !remember; }); }

  Container login() => Container(
    width: MediaQuery.of(context).size.width * 0.75,
    constraints: BoxConstraints(maxWidth: 300),
    child: TextButton(
      style: TextButton.styleFrom(
        backgroundColor: Colors.white,
        shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(40) ),
      ),
      onPressed: waiting == true ? null : pressedLogin,
      child: waiting != false
        ? Center(child: SizedBox(height: 20.0, width: 20.0, child: CircularProgressIndicator()))
        : Text( 'Entrar', style: TextStyle( color: Colors.black87 ) )
    ),
  );

  dynamic pressedLogin() async {
    if (_formKey.currentState!.validate()) {
      setState(() { waiting = !waiting; });
      var err = await LoginController.login(context, _email.text, _password.text, remember);
      setState(() {
        error = err ?? '';
        waiting = !waiting;
      });
    }
  }
}