import 'package:flutter/material.dart';
import 'package:commerce_app/src/Controllers/LoginController.dart';
import 'package:commerce_app/src/core/AppStyles.dart';
import '../Drawer/Drawer.dart';
import './Components/confirmDialog.dart';
import 'LoginForm.dart';

class Login extends StatefulWidget {
  final bool? fromSignup;

  Login({ Key? key, this.fromSignup }) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  @override
  void initState() {
    super.initState();
    if(widget.fromSignup == true) concludedDialog(context);
    LoginController.checkTokenValidity(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: LeftDrawer(),
      appBar: AppBar(title: Text('Commerce App')),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/gradient2.jpg'), fit: BoxFit.cover
          )
        ),
        child: Center(
          child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Container(
              constraints: BoxConstraints(maxWidth: 500),
              width: MediaQuery.of(context).size.width * 0.9,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(50),
                color: AppStyles.opacityBlue,
              ),
              child: LoginForm(),
            )
          )
        ),
      )
    );
  }
}
