import 'package:flutter/material.dart';
import 'package:commerce_app/src/Templates/Mobile/Signup/Signup.dart';

class SignupButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width * 0.75,
      constraints: BoxConstraints(maxWidth: 300),
      child: TextButton(
        style: TextButton.styleFrom(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(40),
            side: BorderSide(color: Colors.white, width: 1)
          ),
        ),
        onPressed: () { Navigator.of(context).push(MaterialPageRoute(builder: (context) => Signup())); },
        child: Text('Não possuí conta? Cadastre-se', style: TextStyle(color: Colors.white)),
      )
    );
  }
}




