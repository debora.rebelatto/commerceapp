import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:commerce_app/src/Templates/Components/validatorFields.dart';

class LoginFields extends StatefulWidget {
  final email;
  final password;

  LoginFields(this.email, this.password);

  @override
  _LoginFieldsState createState() => _LoginFieldsState();
}

class _LoginFieldsState extends State<LoginFields> {
  bool _obscureText = true;

  void _toggle() { setState(() { _obscureText = !_obscureText; }); }

  @override
  Widget build(BuildContext context) => Padding(
    padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
    child: Column(children: [
      field(widget.email, 'E-mail'), field(widget.password, 'Senha', suffix: true)
    ])
  );

  Container field(var controller, String label, { bool? suffix }) {
    var suffixIcon = suffix == null ? null : IconButton(
      icon: Icon( _obscureText ? Icons.visibility : Icons.visibility_off, color: Colors.white ),
      onPressed: _toggle,
    );

    var inputDecoration = InputDecoration(
      prefixIcon: Icon(
        label == 'Senha' ? Icons.lock_outline : Icons.mail_outline_rounded,
        color: Colors.white
      ),
      suffixIcon: suffixIcon,
      labelText: label,
      labelStyle: TextStyle(color: Colors.white),
      fillColor: Colors.white,
    );

    return Container(
      padding: EdgeInsets.only(top: 10),
      child: TextFormField(
        obscureText: label == 'Senha' ? _obscureText : false,
        keyboardType: TextInputType.emailAddress,
        controller: controller,
        style: TextStyle(color: Colors.white),
        decoration: inputDecoration,
        textInputAction: TextInputAction.next,
        onEditingComplete: () => FocusScope.of(context).nextFocus(),
        validator: (value) => validator(value, label)
      ),
    );
  }
}

String? validator(value, label) {
  switch(label) {
    case 'E-mail': return FieldValidator.validateEmail(value);
    case 'Senha': return FieldValidator.validateIfEmpty(value);
    default: return FieldValidator.validateIfEmpty(value);
  }
}
