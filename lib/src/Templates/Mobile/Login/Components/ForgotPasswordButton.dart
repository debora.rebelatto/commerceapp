import 'package:flutter/material.dart';
import 'package:commerce_app/src/Templates/Mobile/ForgotPassword/ForgotPassword.dart';

class ForgotPasswordButton extends StatelessWidget {
  final String email;
  ForgotPasswordButton(this.email);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: TextButton(
        onPressed: () {
          Navigator.of(context).push(MaterialPageRoute( builder: (context) => WidgetForgotPassword(email: email)));
        },
        child: Text('Esqueceu a senha?', style: TextStyle(color: Colors.white)),
      )
    );
  }
}