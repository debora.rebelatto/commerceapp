import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

dynamic concludedDialog(BuildContext context) async {
  await Future.delayed(Duration(milliseconds: 50));
  await showDialog(
    context: context,
    builder: (context) {
      return AlertDialog(
        title: Lottie.asset(
          'assets/lottie/GreenCheckmark.json',
          height: 100, repeat: false, animate: true,
        ),
        content: Padding(
          padding: EdgeInsets.only(top: 10, bottom: 5),
          child: Text('Cadastro concluído',
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 20)
          )
        ),
        actions: [
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text('Continuar'),
          )
        ],
      );
    },
  );
}