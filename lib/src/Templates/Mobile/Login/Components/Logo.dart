import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Logo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Center(
        child: Text('Login',
          style: GoogleFonts.inriaSans(
            textStyle: TextStyle( color: Colors.white, fontWeight: FontWeight.bold, fontSize: 30 )
          )
        )
      )
    );
  }
}
