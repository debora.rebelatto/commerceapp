import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Filters extends StatefulWidget {
  @override
  _FiltersState createState() => _FiltersState();
}

class _FiltersState extends State<Filters> {
  @override
  Widget build(BuildContext context) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Padding(
            padding: EdgeInsets.only(left: 10),
            child: Text('Produtos',
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
                color: Theme.of(context).primaryTextTheme.headline1!.color
              )
            ),
          ),

          Row(children: [
            IconButton( icon: Icon(Icons.filter_alt), onPressed: filterDialog ),
            IconButton( icon: Icon(Icons.filter_list), onPressed: openDialog ),
          ])
        ]
      );
  }

  dynamic filter(var field, var order, var quantity) {
    Navigator.of(context).pop();
    setState(() {
      /*filters.field = field;
      filters.order = order;
      filters.quantity = quantity;
      _future = _getData(values: filters);*/
    });
  }

  dynamic openDialog() async{
    /*final values = await Navigator.of(context).push(MaterialPageRoute(
      builder: (context) => Filter(filters: filters)
    ));
    setState(() {
      filters.field = values.field;
      filters.order = values.order;
      filters.quantity = values.quantity;
      _future = values != null ? _getData(values: values) : _getData(values: values);
    });*/
  }

  Future filterDialog() {
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          content: Container(
            height: MediaQuery.of(context).size.height * 0.25,
            child: SingleChildScrollView(child: Column( children: [
              TextButton( onPressed: () { filter('Nome', 'asc', '50'); }, child: Text('A-Z') ),
              TextButton( onPressed: () { filter('Nome', 'desc', '50'); }, child: Text('Z-A') ),
              TextButton( onPressed: () {
                filter('ValorVenda', 'desc', '50');
              }, child: Text('Preço maior para menor')),
              TextButton( onPressed: () {
                filter('ValorVenda', 'asc', '50');
              }, child: Text('Preço menor para maior')),
            ]))
          )
        );
      }
    );
  }
}