import 'package:flutter/material.dart';
import 'package:commerce_app/src/Controllers/CarrinhoController.dart';
import 'package:commerce_app/src/Model/ItemTabelaPreco/ItemTabelaPreco.dart';
import 'package:commerce_app/src/Model/Produto/Produto.dart';
import 'package:commerce_app/src/Services/ItemCarrinhoServices.dart';

class AdicionaProduto extends StatefulWidget {
  final double quantidade;
  final ItemTabelaPreco? item;
  final Produto? produto;
  final Function(bool)? rebuild;

  AdicionaProduto(this.quantidade, this.item, { this.produto, this.rebuild });

  @override
  _AdicionaProdutoState createState() => _AdicionaProdutoState();
}

class _AdicionaProdutoState extends State<AdicionaProduto> {
  @override
  Widget build(BuildContext context) {
    return IconButton(
      tooltip: 'Adicionar produto ao carrinho',
      icon: Icon( Icons.add_shopping_cart, color: Colors.red[700]),
      onPressed: () async {
        var snackBar = await add();
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
      }
    );
  }

  Future add() async {
    var response;
    var snackBar;
    var valor = await getValor();

    var mult = widget.produto!.quantidadeMultipla == 0
      ? 1
      : widget.produto!.quantidadeMultipla;

    if(widget.quantidade % mult! == 0) {
      response = await ItemCarrinhoServices.postIncrementoCarrinho(widget.quantidade.round(), widget.produto!.id, valor);

      if(response == 201 || response == 204) {
        snackBar = SnackBar(
          behavior: SnackBarBehavior.floating,
          duration: Duration(seconds: 1),
          content: Text('${widget.produto!.nome} adicionado ao carrinho.')
        );


      } else if (response == 0) {
        snackBar = SnackBar(
          behavior: SnackBarBehavior.floating,
          duration: Duration(seconds: 1),
          content: Text('Quantidade não pode ser 0')
        );
      } else {
        snackBar = SnackBar(
          behavior: SnackBarBehavior.floating,
          duration: Duration(seconds: 1),
          content: Text('Erro ao Adicionar produto ao carrinho')
        );
      }
    } else {
      snackBar = SnackBar(
        behavior: SnackBarBehavior.floating,
        duration: Duration(seconds: 1),
        content: Text('Quantidade múltipla deste produto é ${widget.produto!.quantidadeMultipla}.\nVerificar quantidade.')
      );
    }
    return snackBar;
  }

  Future<double?>? getValor() async => widget.produto != null
    ? await CarrinhoController.getValorVenda(widget.produto!.id.toString())
    : widget.item!.valorVenda;
}