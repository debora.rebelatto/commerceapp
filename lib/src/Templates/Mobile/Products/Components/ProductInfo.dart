import 'package:flutter/material.dart';
import 'package:commerce_app/src/Model/ProdutoItemTabelaPreco/ProdutoItemTabelaPreco.dart';
import 'package:commerce_app/src/Templates/Components/WidgetImages/DownloadImage.dart';
import 'package:commerce_app/src/Templates/Components/currencyFormat.dart';
import '../../Components/ProductInfo.dart';
import 'QuantidadeProduto.dart';

class ProductInfo extends StatelessWidget {
  final ProdutoItemTabelaPreco item;

  ProductInfo(this.item);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        DownloadImage(item.produto.id.toString()),
        Container(
          padding: EdgeInsets.only(left: 20, right:10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              productInfo('${item.produto.nome}', isBold: true),
              Padding(
                padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                child: productInfo('R\$ ${currencyFormat.format(item.itemTabelaPreco!.valorVenda)}', isBold: true, size: 16),
              ),
              Padding( padding: EdgeInsets.only( top: 15 ), child: QuantidadeProduto(item))
            ]
          )
        ),
      ]
    );
  }
}
