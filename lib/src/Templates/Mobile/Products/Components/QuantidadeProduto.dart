import 'package:flutter/material.dart';
import 'package:commerce_app/src/Model/ProdutoItemTabelaPreco/ProdutoItemTabelaPreco.dart';
import 'package:commerce_app/src/core/AppStyles.dart';

import 'AdicionaProduto.dart';

class QuantidadeProduto extends StatefulWidget {
  final ProdutoItemTabelaPreco item;

  QuantidadeProduto(this.item, { Key? key }) : super(key: key);

  @override
  _QuantidadeProdutoState createState() => _QuantidadeProdutoState();
}

class _QuantidadeProdutoState extends State<QuantidadeProduto>{
  TextEditingController myController = TextEditingController(text: '');
  String? mult;
  bool loading = false;

  @override
  void initState() {
    super.initState();

    setState(() {
      mult = widget.item.produto.quantidadeMultipla == 0
        ? '1' : widget.item.produto.quantidadeMultipla.toString();

      myController.text = (widget.item.produto.quantidadePadrao! < int.parse(mult!)
        ? mult : widget.item.produto.quantidadePadrao.toString())!;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          decoration: BoxDecoration(
            border: Border.all( color: Colors.black26, width: 1.0 ),
            borderRadius: BorderRadius.all( Radius.circular(5.0) ),
          ),
          child: Row(children:[
            IconButton(
              icon: Icon( Icons.remove ),
              color: myController.text == mult ? Colors.grey : AppStyles.slightlyDarkerRed,
              onPressed: myController.text == mult ? null : remove,
            ),
            Text(myController.text, style: TextStyle( fontSize: 20 )),
            IconButton(
              icon: Icon( Icons.add ),
              color: AppStyles.slightlyDarkerRed,
              onPressed: add
            ),
          ]),
        ),
        loading
        ? CircularProgressIndicator()
        : AdicionaProduto(int.parse(myController.text).toDouble(), widget.item.itemTabelaPreco, produto: widget.item.produto,),
      ],
    );
  }

  dynamic add() {
    var atual = int.parse(myController.text);
    atual += int.parse(mult!);
    setState(() {
      myController.text = atual.toString();
    });
  }

  dynamic remove() {
    var atual = int.parse(myController.text);
    if(atual - int.parse(mult!) <= 1) {
      setState(() { myController.text = mult.toString(); });
    } else {
      atual -= int.parse(mult!);
      setState(() { myController.text = atual.toString(); });
    }
  }
}