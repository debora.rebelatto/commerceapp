import 'package:flutter/material.dart';
import 'package:commerce_app/src/Model/ProdutoItemTabelaPreco/ProdutoItemTabelaPreco.dart';
import 'package:commerce_app/src/Templates/Mobile/ProductDetail/WidgetDetalheProduto.dart';
import 'Components/ProductInfo.dart';

class ProdutoBody extends StatelessWidget {
  final List<ProdutoItemTabelaPreco> listProdutoItemTabelaPreco;
  final scrollController;

  ProdutoBody(this.listProdutoItemTabelaPreco, { Key? key, this.scrollController }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      physics: BouncingScrollPhysics(),
      itemCount: listProdutoItemTabelaPreco.length,
      itemBuilder: (context, index) {
        var itemTabela = listProdutoItemTabelaPreco[index];
        return itemTabela.itemTabelaPreco != null
          ? Container(
            padding: EdgeInsets.fromLTRB(15, 5, 15, 0),
            child: Card(
              elevation: 5,
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.all( Radius.circular(20) )),
              child: Material(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.all( Radius.circular(20) )),
                color: Theme.of(context).cardColor,
                child: Ink(
                  child: InkWell(
                    onTap: () => Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => WidgetDetalhesProduto(itemTabela.produto.id.toString()))
                    ),
                    child: FittedBox(
                      child: Padding(
                        padding: EdgeInsets.only(bottom: 20, top: 20),
                        child: ProductInfo(itemTabela)
                      )
                    )
                  )
                )
              )
            )
          )
          : Container();
      },
    );
  }
}
