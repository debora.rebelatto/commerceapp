import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:commerce_app/src/core/AppStyles.dart';

class OrderFilter extends StatefulWidget {
  final order;

  OrderFilter({ Key? key, this.order }) : super(key: key);

  @override
  OrderState createState() => OrderState();
}

class OrderState extends State<OrderFilter> {
  var order;

  @override
  void initState() {
    super.initState();
    setOrder();
  }

  dynamic setOrder() async {
    var pref = await SharedPreferences.getInstance();
    if(pref.getString('orderFilter') != null) setState(() { order = pref.getString('orderFilter'); });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Ordenação'),),
      body: Container(child: Column(children:[
        radioField('asc', 'Crescente'),
        radioField('desc', 'Decrescente'),
      ])),
      bottomNavigationBar: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.all(10),
        child: TextButton(
          style: TextButton.styleFrom(
            elevation: 0,
            backgroundColor: AppStyles.darkBlue,
            shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(40) ),
          ),
          onPressed: () { Navigator.pop(context); },
          child: Text('Continuar', style: TextStyle(color: Colors.white),)
        )
      ),
    );
  }

  Material radioField(String type, String text) {
    return Material( child: Ink( child: InkWell(
      onTap: () => clickable(type),
      child: ListTile(
        leading: Radio( value: type, groupValue: order, onChanged: (value) => clickable(type) ),
        title: Text(text)
      ),
    )));
  }

  dynamic clickable(var norder) async {
    var pref = await SharedPreferences.getInstance();
    await pref.setString('orderFilter', norder);

    setState(() { order = norder; });
  }
}

