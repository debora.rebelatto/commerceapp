import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:commerce_app/src/core/AppStyles.dart';

class FieldFilter extends StatefulWidget{
  FieldFilter({ Key? key }) : super(key: key);

  @override
  FieldFilterState createState() => FieldFilterState();
}

class FieldFilterState extends State<FieldFilter> {
  var field;
  var values = ['Nome', 'ValorVenda', 'Codigo', 'Barras', 'NomeComercial', 'DescriçãoEmbalagem'];
  var nameFields = ['Nome', 'Valor', 'Código', 'Barras', 'Nome Comercial', 'Descrição Embalagem'];

  @override
  void initState() {
    super.initState();
    setField();
  }

  dynamic setField() async {
    var pref = await SharedPreferences.getInstance();
    if(pref.getString('fieldFilter') != null) setState(() { field = pref.getString('fieldFilter'); });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Campo')),
      body: Container(
        child: Column(
          children: [
            ListView.builder(
              shrinkWrap: true,
              itemCount: values.length,
              itemBuilder: (context, index) {
                return Material( child: Ink( child: InkWell(
                  onTap: () => clickable(values[index]),
                  child: ListTile(
                    leading: Radio(
                      value: values[index],
                      groupValue: field,
                      onChanged: (value) => clickable(value),
                    ),
                    title: Text(nameFields[index])
                  ),
                )));
              }
            ),
          ]
        )
      ),
      bottomNavigationBar: Container(
        padding: EdgeInsets.all(10),
        child: TextButton(
          style: TextButton.styleFrom(
            padding: EdgeInsets.all(20),
            elevation: 0,
            backgroundColor: AppStyles.darkBlue,
            shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(40) ),
          ),
          onPressed: () { Navigator.pop(context); },
          child: Text('Continuar', style: TextStyle(color: Colors.white),)
        )
      ),
    );
  }

  dynamic clickable(var nfield) async {
    var pref = await SharedPreferences.getInstance();
    await pref.setString('fieldFilter', nfield);
    setState(() { field = nfield; });
  }
}

