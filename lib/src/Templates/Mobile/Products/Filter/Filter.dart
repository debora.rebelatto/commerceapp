import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:commerce_app/src/core/AppStyles.dart';
import 'Components/Orderfilter.dart';
import 'Components/FieldFilter.dart';

class Filter extends StatefulWidget {
  Filter({ Key? key }) : super(key: key);

  @override
  _FilterState createState() => _FilterState();
}

class _FilterState extends State<Filter> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(title: Text('Filtros')),
      body: Container(
        padding: EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ListTile(
              title: Text('Campo'),
              trailing: Icon(Icons.arrow_forward),
              onTap: () async {
                await Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) => FieldFilter()
                ));
              }
            ),
            ListTile(
              title: Text('Ordenação'),
              trailing: Icon(Icons.arrow_forward),
              onTap: () async {
                await Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) => OrderFilter()
                ));
              }
            ),
            cleanFiltersButton(),
          ],
        ),
      ),
      bottomNavigationBar: Container(
        padding: EdgeInsets.all(10),
        child: TextButton(
          style: TextButton.styleFrom(
            shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(40) ),
            elevation: 0,
            backgroundColor: AppStyles.darkBlue,
          ),
          onPressed: () { Navigator.pop(context); },
          child: Text('Filtrar', style: TextStyle(color: Colors.white)),
        )
      ),
    );
  }

  TextButton cleanFiltersButton() => TextButton(
    onPressed: cleanFilters,
    child: Row(children: [
      Padding(padding: EdgeInsets.only(right: 10), child: Icon(Icons.delete)),
      Text('Limpar Filtros')
    ])
  );

  dynamic cleanFilters() async {
    var pref = await SharedPreferences.getInstance();

    await pref.remove('fieldFilter');
    await pref.remove('orderFilter');
    await pref.remove('quantityFilter');

    Navigator.pop(context);
  }
}
