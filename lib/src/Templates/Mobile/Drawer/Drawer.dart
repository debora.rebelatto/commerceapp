import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:commerce_app/src/Templates/Mobile/Carrinho/FutureCarrinho.dart';
import 'package:commerce_app/src/Templates/Mobile/ConfiguracaoHost/HostPorta.dart';
import 'package:commerce_app/src/Templates/Mobile/HomePage/WidgetPageView.dart';
import 'package:commerce_app/src/Templates/Mobile/Login/Login.dart';
import 'package:commerce_app/src/Templates/Mobile/Profile/WidgetProfile.dart';
import 'package:commerce_app/src/Templates/Web/CarrinhoWeb/FutureCarrinhoWeb.dart';
import 'package:commerce_app/src/Templates/Web/HomePageWeb/WidgetWebHomePage.dart';
import 'package:commerce_app/src/Templates/Web/InitialScreen/InitialScreenWidget.dart';
import 'package:commerce_app/src/Templates/Web/ProfileWeb/WidgetProfile.dart';
import './UserDetail.dart';
import './DrawerItems/DrawerItem.dart';

import 'DrawerItems/SwitchTheme.dart';

class LeftDrawer extends StatefulWidget {
  LeftDrawer({ Key? key }) : super(key: key);
  @override
  _DrawerState createState() => _DrawerState();
}

class _DrawerState extends State<LeftDrawer> {
  bool logged = false;

  @override
  void initState() {
    super.initState();
    _getLogged();
  }

  dynamic _getLogged() async {
    var pref = await SharedPreferences.getInstance();
    setState(() {
      logged = pref.getBool('logged') ?? false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(children: [
        Expanded( child: ListView(
          physics: BouncingScrollPhysics(),
          children: <Widget>[
            UserDetail(),
            logged
              ? Container()
              : DrawerItem(
                Icon(Icons.person), 'Login', route: kIsWeb ? InitialScreenWidget() : Login()
              ),
            logged
              ? DrawerItem(
                Icon(Icons.home_outlined), 'Página Inicial', route: kIsWeb ? WidgetWebHomePage() : WidgetPageView()
              )
              : Container(),
            logged
              ? DrawerItem(
                Icon(Icons.person_outline), 'Minha Conta', route: kIsWeb ? WidgetWebProfile() : WidgetProfile()
              )
              : Container(),

            logged
              ? DrawerItem(
                Icon(Icons.shopping_cart_outlined), 'Carrinho', route: kIsWeb ? FutureCarrinhoWeb() : FutureCarrinho()
              )
              : Container(),

            Container(padding: EdgeInsets.only(left: 10, right: 10), child: Divider(color: Colors.grey)),

            DrawerItem(Icon(Icons.settings_outlined), 'Configurações Host', route: HostPorta()),
            logged ? DrawerItem(Icon(Icons.exit_to_app), 'Sair') : Container(),
          ],
        )),
        Container(padding: EdgeInsets.only(left: 10, right: 10), child: Divider(color: Colors.grey)),
        Align( alignment: Alignment.bottomRight, child: SwitchThemeDrawer())
      ],)
    );
  }
}
