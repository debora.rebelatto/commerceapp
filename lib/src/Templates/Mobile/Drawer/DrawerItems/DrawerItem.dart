import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:commerce_app/src/Controllers/SharedPrefController.dart';
import 'package:commerce_app/src/Templates/Mobile/Login/Login.dart';
import 'package:commerce_app/src/Templates/Web/InitialScreen/InitialScreenWidget.dart';

class DrawerItem extends StatelessWidget {
  final Icon _icon;
  final String _name;
  final Widget? route;
  DrawerItem(this._icon, this._name, { this.route });

  @override
  Widget build(BuildContext context) => ListTile(
    leading: _icon,
    title: Text(_name),
    onTap: () async {
      if(route != null) {
        Navigator.pop(context);
        await Navigator.of(context).push(MaterialPageRoute(builder: (context) => route!));
      } else {
        Navigator.pop(context);
        await SharedPrefController.excluiArquivoUsuario();
        kIsWeb
          ? Navigator.push(context, MaterialPageRoute(builder: (conetext) => InitialScreenWidget()))
          : await Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (BuildContext context) => Login()),
            ModalRoute.withName('/'),
          );
      }
    },
  );
}