import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:commerce_app/src/Templates/Mobile/Profile/WidgetProfile.dart';
import 'package:commerce_app/src/core/AppStyles.dart';

class UserDetail extends StatefulWidget {
  UserDetail({ Key? key }) : super(key: key);

  @override
  UserDetailState createState() => UserDetailState();
}

class UserDetailState extends State<UserDetail>{
  String nome = '';
  String email = '';
  String? distribuidor = '';

  @override
  void initState() {
    super.initState();
    getData();
  }

  dynamic getData() async {
    var pref = await SharedPreferences.getInstance();
    setState(() {
      if( pref.getString('nome') != null) { nome = pref.getString('nome')!; }
      if( pref.getString('email') != null) { email = pref.getString('email')!; }
      if( pref.getString('nomeDist') != null) { distribuidor = pref.getString('nomeDist')!; }
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if(nome != '' && email != '') {
          Navigator.pop(context);
          Navigator.of(context).push(MaterialPageRoute(builder: (context) => WidgetProfile()));
        }
      },
      child: UserAccountsDrawerHeader(
        decoration: BoxDecoration( color: Theme.of(context).bottomAppBarColor),
        currentAccountPicture: Container(
          child: CircleAvatar(
            radius: 30,
            backgroundColor: AppStyles.darkBlue,
            child: Text(nome != '' ? '${nome[0]}' : '0', style: TextStyle(fontSize: 30.0, color: Colors.white ))
          )
        ),
        accountName: Container(
          child: Text( nome != '' ? nome : 'Login' ,
            style: TextStyle( color: Theme.of(context).primaryTextTheme.headline1!.color )
          )
        ),

        accountEmail: Container(
          child: Text( email != '' ? email : 'Email' ,
            style: TextStyle( color: Theme.of(context).primaryTextTheme.headline1!.color ),
          )
        )
      ),
    );
  }
}