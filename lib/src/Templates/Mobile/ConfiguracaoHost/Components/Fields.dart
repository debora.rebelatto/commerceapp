import 'package:flutter/material.dart';

TextField fieldsHostPort(TextEditingController controller, String text) => TextField(
  controller: controller,
  maxLines: 1,
  decoration: InputDecoration( labelText: text, ),
);
