import 'package:flutter/material.dart';
import 'package:commerce_app/src/Controllers/PortController.dart';
import 'package:commerce_app/src/Templates/Mobile/Components/cancelButton.dart';

// ignore: must_be_immutable
class Float extends StatelessWidget {
  String host;
  String port;

  Float(this.host, this.port);

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      backgroundColor: Theme.of(context).floatingActionButtonTheme.backgroundColor,
      onPressed: () async {
        await showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              title: Text('Salvar as alterações?'),
              content: Text('Host: $host \nPorta: $port'),
              actions: [
                TextButton(
                  onPressed: () async {
                    PortController.updateHostPort(url: host, port: port);
                    Navigator.of(context).pop();
                  },
                  child: Text( 'Sim', style: TextStyle( color: Colors.white), ),
                ),
                cancel()
              ],
            );
          },
        );
      },
      child: Icon(Icons.save),
    );
  }
}