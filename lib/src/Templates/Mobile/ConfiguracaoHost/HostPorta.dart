import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:commerce_app/src/API/API_URL.dart';
import 'package:commerce_app/src/Controllers/PortController.dart';
import 'package:commerce_app/src/Templates/Mobile/Components/cancelButton.dart';
import 'package:commerce_app/src/core/AppStyles.dart';

class HostPorta extends StatefulWidget {
  HostPorta({Key? key}) : super(key: key);

  @override
  _HostPortaState createState() => _HostPortaState();
}

class _HostPortaState extends State<HostPorta> {
  TextEditingController host = TextEditingController(text: '');
  TextEditingController port = TextEditingController(text: '');

  dynamic _getUrl() async {
    var pref = await SharedPreferences.getInstance();

    setState(() {
      host.text = pref.getString('url') ?? APIURL.defaultIp;
      port.text = pref.getString('port') ?? APIURL.defaultPort;
    });
  }

  @override
  void initState() {
    super.initState();
    _getUrl();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Host/Porta')),
      body: Container(
        constraints: BoxConstraints(maxWidth: 800),
        padding: EdgeInsets.fromLTRB(25, 25, 25, 25),
        child: ListView(
          physics: BouncingScrollPhysics(),
          children: [
            Container(
              child: TextField(
                controller: host,
                maxLines: 1,
                decoration: InputDecoration(labelText: 'IP/Host')
              )
            ),
            Container(
              child: TextField(
                controller: port,
                maxLines: 1,
                decoration: InputDecoration(labelText: 'Port')
              )
            ),
            SizedBox(height: 20),
            TextButton(
              style: TextButton.styleFrom(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
                backgroundColor: AppStyles.darkBlue,
              ),
              onPressed: () async {
                await updateHostPort('customUrl');
              },
              child: Text('Salvar Customizado',
                style: TextStyle(
                  fontWeight: FontWeight.bold, color: Colors.white
                )
              ),
            ),
          ],
        )
      )
    );
  }

  dynamic accept() {
    return TextButton(
      style: TextButton.styleFrom(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
        backgroundColor: Colors.grey,
      ),
      onPressed: () {
        update(host.text, port.text);
        Navigator.of(context).pop();
      },
      child: Text('Sim', style: TextStyle(color: Colors.white)),
    );
  }

  dynamic updateHostPort(String type) {
    switch (type) {
      case 'customUrl':
        showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              title: Text('Salvar as alterações?'),
              content: Text('Host: ${host.text} \nPorta: ${port.text}'),
              actions: [accept(), cancel()],
            );
          },
        );
        break;
    }
  }

  dynamic update(String newHost, String newPort) async {
    PortController.updateHostPort(url: newHost, port: newPort);
    setState(() {
      host.text = newHost;
      port.text = newPort;
    });
  }
}
