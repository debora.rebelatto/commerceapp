import 'package:flutter/material.dart';
import 'package:commerce_app/src/Model/ItemTabelaPreco/ItemTabelaPreco.dart';
import 'package:commerce_app/src/Templates/Mobile/Carrinho/FutureCarrinho.dart';
import './Components/SliverAppBar.dart';
import './Components/HeaderInfo.dart';
import './Components/Detalhes.dart';
import './Components/form.dart';

class DetalheProduto extends StatelessWidget{
  final ItemTabelaPreco? item;

  DetalheProduto(this.item);

  @override
  Widget build(BuildContext context) {
    return Scaffold (
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text('${item!.produto!.codigo}'),
        actions: [
          IconButton(
            tooltip: 'Abrir Carrinho', iconSize: 30,
            padding: EdgeInsets.only(left: 10, right: 10),
            icon: Icon(Icons.shopping_cart_outlined),
            onPressed: () async {
              await Navigator.of(context).push(MaterialPageRoute(builder: (context) => FutureCarrinho()));
            },
          )
        ],
      ),
      body: SafeArea(
        child: Stack(
          children: [
            NestedScrollView(
              headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
                return <Widget>[
                  sliverAppBar(item!.produto!.id.toString(), context)
                ];
              },
              body: SingleChildScrollView(
                physics: BouncingScrollPhysics(),
                child: Padding(
                  padding: EdgeInsets.only(top:10),
                  child: Material(
                    elevation: 12,
                    borderRadius: BorderRadius.circular(54),
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 23),
                      decoration: BoxDecoration(
                        color: Theme.of(context).brightness == Brightness.light
                          ? Colors.white : Color(0xFF353941),
                        borderRadius: BorderRadius.circular(54),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          HeaderInfo(item!),
                          form(context, item!),
                          Padding(
                            padding: EdgeInsets.only(top: 10, bottom: 10),
                            child: Divider(color: Colors.grey)
                          ),
                          detalhes(item!),
                          SizedBox( height: 80 ),
                        ],
                      ),
                    ),
                  )
                )
              ),
            ),
          ],
        ),
      )
    );
  }


}