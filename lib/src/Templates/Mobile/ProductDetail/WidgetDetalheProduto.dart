import 'package:flutter/material.dart';
import 'package:commerce_app/src/Controllers/ProductsController.dart';
import 'package:commerce_app/src/Model/ProdutoItemTabelaPreco/ProdutoItemTabelaPreco.dart';
import 'DetalheProduto.dart';
import 'Components/Erro.dart';

class WidgetDetalhesProduto extends StatefulWidget {
  final String id;
  WidgetDetalhesProduto(this.id, { Key? key }) : super(key: key);

  @override
  _DetalhesProdutoState createState() => _DetalhesProdutoState();
}

class _DetalhesProdutoState extends State<WidgetDetalhesProduto> {
  Future<dynamic>? _future;
  Future<dynamic> _getData() async => await ProductsController.getItem(widget.id.toString());

  @override
  void initState() {
    super.initState();
    _future = _getData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
        future: _future,
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              return Center(child: CircularProgressIndicator());
            case ConnectionState.done: default:
              var item = snapshot.data as ProdutoItemTabelaPreco;
              return snapshot.hasError ? erro() : DetalheProduto(item.itemTabelaPreco);
          }
        },
      ),
    );
  }
}