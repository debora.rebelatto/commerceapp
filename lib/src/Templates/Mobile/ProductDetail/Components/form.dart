import 'package:flutter/material.dart';
import 'package:commerce_app/src/Model/ItemTabelaPreco/ItemTabelaPreco.dart';
import 'package:commerce_app/src/Templates/Components/validatorFields.dart';
import 'AdicionarProduto.dart';

Widget form(BuildContext context, ItemTabelaPreco item) {
  final _formKey = GlobalKey<FormState>();
  var _quantidade = TextEditingController(text: '');

  InputDecoration decoration() => InputDecoration(
    labelText: 'Quantidade',
    fillColor: Colors.white,
    border: OutlineInputBorder( borderRadius: BorderRadius.circular(40.0)),
  );

  return Container(
    padding: EdgeInsets.only(top: 10, bottom: 10),
    child: Form(
      key: _formKey,
      child: Column(children: <Widget> [
        Container(
          child: TextFormField(
            controller: _quantidade,
            maxLines: 1,
            keyboardType: TextInputType.number,
            autofocus: false,
            decoration: decoration(),
            validator: (value) => FieldValidator.validateIfEmpty(value),
          ),
        ),
        Container(
          padding: EdgeInsets.only(top: 10),
          child: AdicionarProduto(_quantidade, item, _formKey),
        )
      ]),
    )
  );
}
