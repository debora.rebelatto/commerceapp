import 'package:flutter/material.dart';
import 'package:commerce_app/src/Model/ItemTabelaPreco/ItemTabelaPreco.dart';
import 'package:commerce_app/src/Templates/Components/increseDetail.dart';
import 'package:commerce_app/src/core/AppStyles.dart';

class AdicionarProduto extends StatefulWidget {
  final ItemTabelaPreco item;
  final GlobalKey<FormState> form;
  final _quantidade;

  AdicionarProduto(this._quantidade, this.item, this.form, { Key? key }) : super(key: key);

    @override
  _AdicionarProdutoState createState() => _AdicionarProdutoState();
}

class _AdicionarProdutoState extends State<AdicionarProduto>{
  @override
  Widget build(BuildContext context) {
    return TextButton(
      style: TextButton.styleFrom(
        backgroundColor: AppStyles.darkBlue,
        shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(40)),
      ),
      onPressed: () async {
        if(!widget._quantidade.text.isEmpty || widget._quantidade.text == '' ) {
          if (widget.form.currentState!.validate()) {
            widget.form.currentState!.save();
            ScaffoldMessenger.of(context).showSnackBar(await increse(widget.item, widget._quantidade));
          }
        }
      },
      child: Container(
        padding: EdgeInsets.all(10),
        child: Row (
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(Icons.add_shopping_cart, color: Colors.white),
            Container(
              margin: EdgeInsets.only(left: 10),
              child: Text('Adicionar ao Carrinho', style: TextStyle(color: Colors.white)),
            )
          ],
        ),
      ),
    );
  }
}