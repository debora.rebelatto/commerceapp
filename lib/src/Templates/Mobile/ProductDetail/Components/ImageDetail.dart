import 'package:flutter/material.dart';
import 'package:commerce_app/src/Templates/Components/WidgetImages/DownloadImage.dart';
import 'ImageDetailScreen.dart';

Widget imageDetail(String id, BuildContext context) {
  return FittedBox(
    child: Container(
      padding: EdgeInsets.all(10),
      child: ConstrainedBox(
        constraints: BoxConstraints( minHeight: 5.0, minWidth: 5.0, maxHeight: 280),
        child: GestureDetector(
          onTap: () {
            Navigator.push(context, MaterialPageRoute(builder: (_) {
              return ImageDetailScreen(id);
            }));
          },
          child: Container( child: DownloadImage(id) ),
        )
      )
    )
  );
}
