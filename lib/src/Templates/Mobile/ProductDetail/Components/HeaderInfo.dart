import 'package:flutter/material.dart';
import 'package:commerce_app/src/Model/ItemTabelaPreco/ItemTabelaPreco.dart';
import 'package:commerce_app/src/Templates/Components/currencyFormat.dart';
import 'package:commerce_app/src/core/AppStyles.dart';

class HeaderInfo extends StatelessWidget {
  final ItemTabelaPreco item;

  HeaderInfo(this.item);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 20, bottom: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            item.produto!.nome!,
            style: AppStyles.boldWebText(22, color: Theme.of(context).primaryTextTheme.headline1!.color),
          ),
          Text('${currencyFormat.format(item.valorVenda)}',
            style: AppStyles.boldWebText(22, color: Theme.of(context).primaryTextTheme.subtitle2!.color),
          ),
        ],
      )
    );
  }
}
