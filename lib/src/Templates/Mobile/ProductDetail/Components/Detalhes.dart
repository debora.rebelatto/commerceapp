import 'package:flutter/material.dart';
import 'package:commerce_app/src/Model/ItemTabelaPreco/ItemTabelaPreco.dart';

Widget detalhes(ItemTabelaPreco item) {
  return Container(
    padding: EdgeInsets.only(top: 10, bottom: 10),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        produtoInfo('Descrição: ', item.produto!.informacaoAdicional),
        produtoInfo('Descrição Embalagem: ', item.produto!.descricaoEmbalagem),
        produtoInfo('Cest: ',item.produto!.cest),
        produtoInfo('Dun: ', item.produto!.dun),
        produtoInfo('Ncm ', item.produto!.ncm),
        produtoInfo('Grupo: ', item.produto!.grupo == null ? '' : item.produto!.grupo!.nome),
        produtoInfo('Marca: ', item.produto!.marca == null ? '' : item.produto!.marca!.nome),
        produtoInfo('Código: ', item.produto!.codigo),
        produtoInfo('Código de Barras ', item.produto!.barras),
      ],
    )
  );
}

Widget produtoInfo(String text, var info) {
  info ??= '';
  return Builder(builder: (context) {
    return RichText(
      text: TextSpan(
        text: text,
        style: TextStyle(
          color: Theme.of(context).primaryTextTheme.subtitle1!.color,
          fontWeight: FontWeight.bold,
          fontSize: 18
        ),
        children: [
          TextSpan( text: '$info',
            style: TextStyle(
              color: Theme.of(context).primaryTextTheme.subtitle1!.color,
              fontWeight: FontWeight.normal,
              fontSize: 18
            )
          ),
        ],
      ),
    );
  });
}

