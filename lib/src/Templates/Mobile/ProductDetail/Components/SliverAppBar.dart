import 'package:flutter/material.dart';
import './ImageDetail.dart';

Widget sliverAppBar(String id, BuildContext context) => SliverAppBar(
  pinned: false,
  elevation: 0,
  backgroundColor: Colors.white,
  expandedHeight: 300,
  automaticallyImplyLeading: false,
  floating: true,
  flexibleSpace: FlexibleSpaceBar(
    collapseMode: CollapseMode.parallax,
    background: imageDetail(id, context)
  ),
);
