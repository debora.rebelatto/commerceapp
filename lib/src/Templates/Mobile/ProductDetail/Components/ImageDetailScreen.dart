import 'package:flutter/material.dart';
import 'package:commerce_app/src/Templates/Components/WidgetImages/DownloadImage.dart';

class ImageDetailScreen extends StatelessWidget {
  final String id;

  ImageDetailScreen(this.id);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Stack(children: [
          Align(
            alignment: Alignment.center,
            child: InteractiveViewer( minScale: 0.1, maxScale: 3, child: _imageContent(context) ),
          ),
          Align(
            alignment: Alignment.topLeft,
            child: Container( padding: EdgeInsets.only(left: 10, top: 10), )
          ),
        ])
      )
    );
  }

  Widget _imageContent(BuildContext context){
    return GestureDetector(
      onTap: () { Navigator.pop(context); },
      child: Center(
        child: Container(
          padding: EdgeInsets.all(20),
          width: MediaQuery.of(context).size.width - 20,
          child: DownloadImage(id)
        )
      ),
    );
  }
}