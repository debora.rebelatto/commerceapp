import 'package:flutter/material.dart';

Widget erro() => Scaffold(
  appBar: AppBar(),
  body: Center(
    child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(Icons.warning),
        Text('Produto não encontrado',
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20)
        )
      ],
    )
  )
);
