import 'package:flutter/material.dart';
import 'package:commerce_app/src/Templates/Components/validatorFields.dart';
import 'package:commerce_app/src/Services/UsuariosServices.dart';
import 'package:commerce_app/src/Templates/Components/version.dart';
import 'package:commerce_app/src/Templates/Mobile/ConfiguracaoHost/HostPorta.dart';
import 'package:commerce_app/src/core/AppStyles.dart';

class WidgetForgotPassword extends StatefulWidget {
  final String? email;

  WidgetForgotPassword({ this.email });

  @override
  _WidgetForgotPasswordState createState() => _WidgetForgotPasswordState();
}

class _WidgetForgotPasswordState extends State<WidgetForgotPassword> {
  final _email = TextEditingController(text: '');
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    _email.text = widget.email!;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: kIsWeb ? AppBar() : AppBar(title: Text('Esqueceu a senha?')),
      body: Stack(
        children: [
          Align(
            alignment: Alignment.topRight,
            child: IconButton(icon: Icon(Icons.settings),
              onPressed: () async {
                await Navigator.push(context, MaterialPageRoute( builder: (context) => HostPorta()));
              },
            ),
          ),

          Align(
            alignment: Alignment.center,
            child: Container(
              padding: EdgeInsets.fromLTRB(25, 10, 25, 10),
              constraints: BoxConstraints(maxWidth: 500),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Align(
                    alignment: Alignment.topLeft,
                    child: Text('Digite seu email:', style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Montserrat',
                    )),
                  ),
                  SizedBox(height: 10),
                  Form(
                    key: _formKey,
                    child: field()
                  ),
                  SizedBox(height: 20),
                  confirmButton()
                ],
              )
            )
          ),

          Padding(
            padding: EdgeInsets.only(bottom:20),
              child: Align(
                alignment: Alignment.bottomCenter,
                child: version(),
              )
          )
        ]
      ),
    );
  }

  Container field() {
    return Container(
      padding: EdgeInsets.only(top: 10),
      child: TextFormField(
        keyboardType: TextInputType.emailAddress,
        controller: _email,
        decoration: InputDecoration(
          labelText: 'Email',
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(20))
        ),
        textInputAction: TextInputAction.next,
        onEditingComplete: () => FocusScope.of(context).nextFocus(),
        validator: (value) => FieldValidator.validateEmail(_email.text)
      ),
    );
  }

  Widget confirmButton() {
    return Container(
      padding: EdgeInsets.only(top: 10),
      child: TextButton(
        style: TextButton.styleFrom(
          backgroundColor: AppStyles.darkBlue,
          shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(40)),
        ),
        onPressed: () async { updatePassword(); },
        child: Container(
          padding: EdgeInsets.all(10),
          child: Center(child: Text('Enviar email', style: TextStyle(color: Colors.white)))
        ),
      ),
    );
  }

  dynamic updatePassword() async {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();
      var response = await UsuariosServices.postForgotPassword(_email.text);
      response == 200
        ? dialog('Email enviado!', 'Verifique sua caixa de entrada, enviamos um email com a nova senha')
        : dialog('Email não encontrado!', 'Verifique o email digitado e tente novamente');
    }
  }

  dynamic dialog(String title, String content) {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text(title),
        content: Text(content)
      )
    );
  }
}