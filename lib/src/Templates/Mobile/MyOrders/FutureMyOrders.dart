import 'package:flutter/material.dart';
import 'package:commerce_app/src/Controllers/CarrinhoController.dart';
import 'package:commerce_app/src/Templates/Mobile/MyOrders/WidgetMyOrders.dart';

class FutureMyOrders extends StatefulWidget {
  @override
  _FutureMyOrdersState createState() => _FutureMyOrdersState();
}

class _FutureMyOrdersState extends State<FutureMyOrders> {
  Future<dynamic>? _future;

  Future<dynamic> _getData() async => CarrinhoController.getCarrinhos();

  @override
  void initState() {
    super.initState();
    _future = _getData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Meus Pedidos'),),
      body: SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: Column(
        children: [
          FutureBuilder(
            future: _future,
            builder: (context, snapshot) {
              switch (snapshot.connectionState) {
                case ConnectionState.waiting:
                  return Center(child: Icon(Icons.error));
                case ConnectionState.done: default:
                  return WidgetMyOrdersWeb();
              }
            },
          )
      ])));
  }
}