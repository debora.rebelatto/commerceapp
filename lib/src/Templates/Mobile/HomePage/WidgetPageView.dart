import 'package:flutter/material.dart';
import 'package:commerce_app/src/Templates/Mobile/Carrinho/FutureCarrinho.dart';
import 'package:commerce_app/src/Templates/Mobile/HomePage/HomePage.dart';

// ignore: must_be_immutable
class WidgetPageView extends StatefulWidget {
  PageController controller = PageController( initialPage: 0 );

  WidgetPageView({ Key? key }) : super(key: key);

  @override
  _PageViewState createState() => _PageViewState();
}

class _PageViewState extends State<WidgetPageView> {
  @override
  void initState() {
    super.initState();
    // Checks if token is still valid
    // checkTokenValidity(context);
  }

  @override
  Widget build(BuildContext context) => PageView (
    controller: widget.controller, children: [ HomePage(), FutureCarrinho(pageView: widget) ],
  );
}