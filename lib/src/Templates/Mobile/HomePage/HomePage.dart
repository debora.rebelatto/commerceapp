import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:commerce_app/src/Controllers/FiltersController.dart';
import 'package:commerce_app/src/Controllers/ProductsController.dart';
import 'package:commerce_app/src/Model/ProdutoItemTabelaPreco/ProdutoItemTabelaPreco.dart';
import 'package:commerce_app/src/Templates/Mobile/Carrinho/FutureCarrinho.dart';
import 'package:commerce_app/src/Templates/Mobile/Categories/Categories.dart';
import 'package:commerce_app/src/Templates/Mobile/Products/Filter/Filter.dart';
import 'package:commerce_app/src/Templates/Mobile/Products/ProdutoBody.dart';
import '../Drawer/Drawer.dart';
import '../Search/CustomSearch.dart';

class HomePage extends StatefulWidget {
  HomePage({ Key? key }) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<ProdutoItemTabelaPreco> data = [];
  int currentPage = 1;
  bool loading = false;
  bool finished = false;
  final _controller = ScrollController();

  @override
  void initState() {
    super.initState();
    // LoginController.checkTokenValidity(context);
    _getData();
    _controller.addListener(() async {
      if (_controller.position.pixels == _controller.position.maxScrollExtent) {
        setState(() { loading = true; });
        currentPage++;
        _getData();
        setState(() { loading = false; });
      }
    });
  }

  void _getData({ bool? clean }) async {
    setState(() { loading = true; });

    if(clean == true) FiltersController.cleanFilters();

    var list = await ProductsController.fillProdutoList(page: currentPage);

    list.isEmpty
      ? setState(() { finished = true; })
      : list.forEach((e) => { setState(() { data.add(e); }) });

    await Future.delayed(Duration(seconds: 5));

    setState(() { loading = false; });
  }

  @override
  Widget build(BuildContext context){
    // if(!kIsWeb) Helper.startFirebaseMessaging(context);
    return Scaffold(
      drawer: LeftDrawer(),
      appBar: AppBar(
        title: Text('Commerce App', style: TextStyle(fontWeight: FontWeight.normal)), centerTitle: true,
        actions: [
          search(context),
          IconButton(
            tooltip: 'Abrir Carrinho', iconSize: 30,
            padding: EdgeInsets.only(left: 10, right: 10),
            icon: Icon(Icons.shopping_cart_outlined),
            onPressed: () async {
              await Navigator.of(context).push(MaterialPageRoute(builder: (context) => FutureCarrinho()));
            },
          )
        ],
      ),
      body: SingleChildScrollView(
        controller: _controller,
        physics: AlwaysScrollableScrollPhysics(),
        child: Column(
          children: [
            Stack(children: [ Categories() ]),

            SizedBox(height: 10),

            filtersRow(),

            loading
            ? ListView.builder(
              shrinkWrap: true,
              physics: BouncingScrollPhysics(),
              itemCount: 10,
              itemBuilder: (context, index) {
                return Container(
                  height: 150,
                  width: 100,
                  padding: EdgeInsets.all(10),
                  child: Shimmer.fromColors(
                    enabled: true,
                    baseColor: Colors.grey[100]!,
                    highlightColor: Colors.grey[300]!,
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all( Radius.circular(20)),
                        color: Colors.grey
                      ),
                    )
                  ),
                );
              },
            )
            : Padding(
              padding: EdgeInsets.only(top: 10, bottom: 20),
              child: ProdutoBody(data)
            ),
            finished
              ? Center(child: Padding(
                padding: const EdgeInsets.all(15),
                child: Text('Não há mais nada para mostrar'),
              ))
              : SizedBox(height: 0),
            // loading ? CircularProgressIndicator() : SizedBox(height: 20)
          ],
        )
      )
    );
  }

  IconButton search(BuildContext context) => IconButton(
    iconSize: 30, padding: EdgeInsets.only(right: 25), icon: Icon(Icons.search),
    onPressed: () async { await showSearch(context: context, delegate: CustomSearch()); },
  );

  Row filtersRow() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Padding(
          padding: EdgeInsets.only(left: 20),
          child: Text( 'Produtos',
            style: TextStyle(
              fontSize: 25,
              fontWeight: FontWeight.bold,
              color: Theme.of(context).primaryTextTheme.headline1!.color
            )
          ),
        ),
        Row(children: [
          IconButton( icon: Icon(Icons.filter_list), onPressed: filterDialog ),
          IconButton( icon: Icon(Icons.filter_alt), onPressed: filterPage ),
        ])
      ]
    );
  }

  dynamic filterDialog() {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          content: Container(
            child: SingleChildScrollView(
              child: Column( children: [
                buttonFilter('A-Z', 'Nome', 'asc', '50'),
                buttonFilter('Z-A', 'Nome', 'desc', '50'),
                buttonFilter('Preço maior para menor', 'ValorVenda', 'desc', '50'),
                buttonFilter('Preço menor para maior', 'ValorVenda', 'asc', '50'),
              ])
            )
          )
        );
      },
    );
  }

  Widget buttonFilter(title, field, order, quantity) {
    return TextButton(
      onPressed: () async {
        Navigator.pop(context);
        FiltersController.filter(field, order, quantity);
        setState(() { _getData(); });
      },
      child: Text(title)
    );
  }

  dynamic filterPage() async{
    await Navigator.of(context).push( MaterialPageRoute( builder: (context) => Filter() ));
    setState(() { _getData(); });
  }
}

class WidgetDetalhesProduto {
}


