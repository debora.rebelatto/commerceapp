import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:commerce_app/src/Controllers/ProductsController.dart';
import 'package:commerce_app/src/Model/ProdutoItemTabelaPreco/ProdutoItemTabelaPreco.dart';
import 'package:commerce_app/src/Templates/Components/emptySearchList.dart';
import '../Products/ProdutoBody.dart';

class SearchResults extends StatefulWidget {
  final String query;

  SearchResults(this.query);

  @override
  _SearchResultsState createState() => _SearchResultsState();
}

class _SearchResultsState extends State<SearchResults> {
  Future<dynamic>? _future;

  Future<List<ProdutoItemTabelaPreco>> _search() async => await ProductsController.search(widget.query);

  @override
  void initState() {
    super.initState();
    _future = _search();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
        future: _future,
        builder: (context, snapshot) {
          var items = snapshot.data as List<ProdutoItemTabelaPreco>;
          switch(snapshot.connectionState) {
            case ConnectionState.waiting:
              return Center(child: CircularProgressIndicator(),);
            case ConnectionState.done: default:
              return items.isEmpty
                ? emptySearchList(widget.query) : ProdutoBody(items);
          }
        }
      )
    );
  }
}

