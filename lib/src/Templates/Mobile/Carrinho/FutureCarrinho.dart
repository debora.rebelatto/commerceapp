import 'package:flutter/material.dart';
import 'package:commerce_app/src/Controllers/CarrinhoController.dart';
import 'package:commerce_app/src/Model/ItemCarrinho/ItemCarrinho.dart';
import 'package:commerce_app/src/Model/ItensCarrinho/ItensCarrinho.dart';
import 'package:commerce_app/src/Templates/Components/emptyCart.dart';
import '../HomePage/WidgetPageView.dart';
import '../Search/CustomSearch.dart';
import 'Carrinho.dart';

class FutureCarrinho extends StatefulWidget {
  final WidgetPageView? pageView;

  FutureCarrinho({ Key? key, this.pageView }) : super(key: key);
  @override
  _FutureCarrinhoState createState() => _FutureCarrinhoState();
}

class _FutureCarrinhoState extends State<FutureCarrinho> {
  Future<dynamic>? _future;
  Future<dynamic> _getData() async => CarrinhoController.getItensCarrinho();

  @override
  void initState() {
    super.initState();
    _future = _getData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Carrinho'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search),
            padding: EdgeInsets.only(right: 25),
            onPressed: () async { await showSearch(context: context, delegate: CustomSearch()); },
          ),
        ],
      ),

      body: FutureBuilder(
        future: _future,
        builder: (context, snapshot) {
          switch(snapshot.connectionState) {
            case ConnectionState.waiting:
              return Center(child: CircularProgressIndicator());
            case ConnectionState.done:
              if(snapshot.data != null) {
                var itensCarrinho = snapshot.data as ItensCarrinho;
                var itemCarrinho = itensCarrinho.itemcarrinho as List<ItemCarrinho>;

                return itemCarrinho.isEmpty
                  ? EmptyCart()
                  : Carrinho(
                    itensCarrinho: itensCarrinho,
                    rebuild: (bool value) { if (value) setState(() { _future = _getData(); }); }
                  );
              } else {
                return EmptyCart();
              }
            default: return Container();
          }
        },
      ),
    );
  }
}