import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:commerce_app/src/Controllers/CarrinhoController.dart';
import 'package:commerce_app/src/Model/ItemCarrinho/ItemCarrinho.dart';
import 'package:commerce_app/src/core/AppStyles.dart';
import '../Carrinho.dart';
import 'Dialogs.dart';

class QuantidadeCarrinho extends StatefulWidget {
  final ItemCarrinho item;
  final Carrinho carrinho;

  QuantidadeCarrinho (this.item, this.carrinho, { Key? key }) : super(key: key);

  @override
  _QuantidadeCarrinhoState createState() => _QuantidadeCarrinhoState();
}

class _QuantidadeCarrinhoState extends State<QuantidadeCarrinho> {
  TextEditingController quantidadeContoller = TextEditingController(text: '');
  int? mult;

  @override
  void initState() {
    super.initState();
    setState(() {
      mult = (widget.item.produto!.quantidadeMultipla == 0 ? 1 : widget.item.produto!.quantidadeMultipla)!;
      quantidadeContoller.text = widget.item.quantidade!.round().toString();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Row( children: [
      Padding(
        padding: EdgeInsets.only(top: 10),
        child: Row( children: [
          Container(
            decoration: BoxDecoration(
              border: Border.all( color: Colors.black26, width: 1.0 ),
              borderRadius: BorderRadius.all( Radius.circular(5.0) ),
            ),
            child: Row(children: [
              IconButton( icon: Icon(Icons.remove), color: AppStyles.slightlyDarkerRed, onPressed: decrease ),
              Text('${quantidadeContoller.text}', style: TextStyle( fontSize: 20 )),
              IconButton( icon: Icon( Icons.add ), color: AppStyles.slightlyDarkerRed, onPressed: increase ),
            ]),
          ),
        ]),
      ),
    ]);
  }

  dynamic increase() async {
    var atual = int.parse(quantidadeContoller.text) + mult!;
    setState(() {
      quantidadeContoller.text = atual.toString();
    });

    var quantidade = int.parse(quantidadeContoller.text);

    if(quantidade % mult! == 0) {
      await CarrinhoController.increaseDebounce(widget.item, quantidade, context, widget.carrinho);
        /* snack(
        widget.carrinho, context, widget.item.produto.nome, erro:
          'Quantidade múltipla deste produto é $mult .\nVerificar quantidade.'
      ); */
    }
  }

  dynamic decrease() {
    if(int.parse(quantidadeContoller.text) == mult) {
      Slidable.of(context)!.open(actionType: SlideActionType.secondary);
      removeProductDialog(widget.item.id.toString(), context, widget.carrinho);
    } else {
      var quantMult = mult;
      var atual = int.parse(quantidadeContoller.text);

      if (atual - quantMult! <= quantMult) {
        setState(() { quantidadeContoller.text = quantMult.toString(); });
      } else {
        atual -= quantMult;
        setState(() { quantidadeContoller.text = atual.toString(); });
      }

      var res = CarrinhoController.decreaseDebounce(widget.item, int.parse(quantidadeContoller.text), context, widget.carrinho);

      if(res == 204) {
        if(widget.carrinho.rebuild != null) widget.carrinho.rebuild!(true);
      } else {
        //snack(widget.carrinho, context, widget.item.produtoId.toString(), erro: 'Erro ao tentar retirar produto');
      }
    }
  }
}
