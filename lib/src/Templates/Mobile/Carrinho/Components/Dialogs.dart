import 'package:flutter/material.dart';
import 'package:commerce_app/src/Templates/Mobile/Components/cancelButton.dart';
import 'package:commerce_app/src/Services/ItemCarrinhoServices.dart';
import '../Carrinho.dart';

Future removeProductDialog(String id, BuildContext context, Carrinho carrinho) => showDialog(
  context: context,
  builder: (context) {
    return AlertDialog(
      title: Text('Remover produto'),
      content: Text('Tem certeza que deseja Remover o Produto?'),
      actions: [
        TextButton(
          style: TextButton.styleFrom(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
            backgroundColor: Colors.grey,
          ),
          onPressed: () async {
            if(await ItemCarrinhoServices.deleteItemCarrinhoById(id) == 200) Navigator.of(context).pop();
            carrinho.rebuild!(true);
          },
          child: Text('Sim', style: TextStyle( color: Colors.white)),
        ),
        cancel()
      ],
    );
  },
);
