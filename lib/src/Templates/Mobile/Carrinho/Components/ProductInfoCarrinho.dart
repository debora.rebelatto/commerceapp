import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:commerce_app/src/Model/ItemCarrinho/ItemCarrinho.dart';
import 'package:commerce_app/src/Templates/Components/WidgetImages/DownloadImage.dart';
import 'package:commerce_app/src/Templates/Components/currencyFormat.dart';
import '../../Components/ProductInfo.dart';
import '../Carrinho.dart';
import './Dialogs.dart';
import './QuantidadeCarrinho.dart';

class ProductInfoCarrinho extends StatelessWidget {
  final ItemCarrinho itemCarrinho;
  final Carrinho carrinho;

  ProductInfoCarrinho(this.itemCarrinho, this.carrinho);

  @override
  Widget build(BuildContext context) {
    return Row(children: [
      DownloadImage(itemCarrinho.produto!.id.toString()),
      Container(
        padding: EdgeInsets.only(left: 20, top: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            productInfo('${itemCarrinho.produto!.nome}', isBold: true),
            productInfo('Quantidade: ${itemCarrinho.quantidade!.round()}', isBold: false, size: 16),
            productInfo('Total: ${currencyFormat.format(itemCarrinho.total)}', isBold: true, size: 16),
            if(kIsWeb) Container(
              child: TextButton(
                onPressed: () {
                  removeProductDialog(itemCarrinho.id.toString(), context, carrinho);
                },
                child: Text('Remover')
              )
            ),
            QuantidadeCarrinho(itemCarrinho, carrinho)
          ]
        ),
      )
    ]);
  }
}