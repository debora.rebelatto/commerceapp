import 'package:flutter/material.dart';
import 'package:commerce_app/src/Templates/Mobile/Components/Snackbar.dart';

dynamic snack(
  BuildContext context,
  String? nome,
  var response,
  { String? erro, var carrinho, }
) async {

  late SnackBar snackbar;

  switch(response) {
    case 201:
      snackbar = showThisSnackBar('$nome adicionado ao carrinho.');
      if(carrinho.rebuild != null) carrinho.rebuild(true);
      break;
    case 204:
      snackbar = showThisSnackBar('$nome adicionado ao carrinho.');
      if(carrinho.rebuild != null) carrinho.rebuild(true);
      break;
    case 0:
      snackbar = showThisSnackBar('Quantidade não pode ser 0');
      break;
    default:
      snackbar = showThisSnackBar('Erro ao Adicionar produto ao carrinho');
      break;
  }

  if(erro != null) { snackbar = showThisSnackBar('$erro'); }
  ScaffoldMessenger.of(context).showSnackBar(snackbar);
}