import 'package:flutter/material.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:commerce_app/src/Model/ItemCarrinho/ItemCarrinho.dart';
import 'package:commerce_app/src/Model/ItensCarrinho/ItensCarrinho.dart';
import 'package:commerce_app/src/Templates/Components/currencyFormat.dart';

class SlideUp extends StatelessWidget {
  final List<ItemCarrinho> item;
  final ItensCarrinho itensCarrinho;

  SlideUp(this.item, this.itensCarrinho);

  @override
  Widget build(BuildContext context) {
    return SlidingUpPanel(
      color: Theme.of(context).bottomAppBarColor,
      maxHeight: MediaQuery.of(context).size.height * .60,
      minHeight: 30,
      parallaxEnabled: true,
      parallaxOffset: .5,
      panelBuilder: (sc) => slidablePanel(sc, item, itensCarrinho),
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(18.0), topRight: Radius.circular(18.0)
      ),
    );
  }
}

Widget slidablePanel(ScrollController sc, List<ItemCarrinho> itemCarrinhoList, ItensCarrinho itensCarrinho) {
  return Builder(builder: (context) {
    return MediaQuery.removePadding(
      context: context,
      removeTop: true,
      child: ListView(
        physics: BouncingScrollPhysics(),
        controller: sc,
        children: [
          SizedBox( height: 12.0 ),

          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: 30,
                height: 5,
                decoration: BoxDecoration(
                  color: Colors.grey[300],
                  borderRadius: BorderRadius.all(Radius.circular(12.0))
                )
              ),
            ]
          ),

          SizedBox(height: 18.0),

          Container(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget> [
                // Header
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    info( MediaQuery.of(context).size.width / 2.5, 'Nome', TextAlign.left),
                    info( MediaQuery.of(context).size.width / 6, 'Quantidade', TextAlign.center),
                    info( MediaQuery.of(context).size.width / 4, 'Total', TextAlign.right),
                  ]
                ),

                // Lista de itens
                ListView.builder(
                  shrinkWrap: true,
                  physics: BouncingScrollPhysics(),
                  itemCount: itemCarrinhoList.length,
                  itemBuilder: (context, index) {
                    var item = itemCarrinhoList[index];
                    var width = MediaQuery.of(context).size.width;
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        info( width / 2.5, item.produto!.nome!, TextAlign.left),
                        info( width / 6, '${item.quantidade!.toInt()}', TextAlign.center),
                        info( width / 4, '${currencyFormat.format(item.total)}', TextAlign.right),
                      ]
                    );
                  },
                ),

                Container(
                  padding: EdgeInsets.only(left: 10, right: 10),
                  child: Divider(color: Colors.grey,)
                ),

                // Total
                Padding(
                  padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Total',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      Text('${currencyFormat.format(itensCarrinho.carrinho.total)}',
                        style: TextStyle(fontWeight: FontWeight.bold)
                      )
                    ]
                  )
                ),
              ],
            ),
          ),
          SizedBox(height: 24,),
        ],
      )
    );
  });
}

dynamic info(var size, String text, align) {
  return Container(
    padding: EdgeInsets.only(top: 5, bottom: 5),
    child: SizedBox(
      width: size,
      child: Text( text,
        overflow: TextOverflow.ellipsis,
        softWrap: false,
        textAlign: align,
      )
    )
  );
}
