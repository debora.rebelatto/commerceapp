import 'package:flutter/material.dart';
import 'package:commerce_app/src/Model/ItensCarrinho/ItensCarrinho.dart';
import 'package:commerce_app/src/Templates/Components/currencyFormat.dart';
import 'FinalizeButton.dart';
import 'LimparCarrinho.dart';

class BottomNavigation extends StatelessWidget {
  final ItensCarrinho itensCarrinho;

  BottomNavigation(this.itensCarrinho);

  @override
  Widget build(BuildContext context) {
    // ignore: unnecessary_null_comparison
    var total = itensCarrinho.itemcarrinho == null ? '' : itensCarrinho.carrinho.total;
    return Container(
      color: Theme.of(context).bottomAppBarColor,
      child: Row(children: <Widget> [
        Expanded(
          child: Container(
            padding: EdgeInsets.only(left: 10),
            child: Text('Total: ${currencyFormat.format(total)}',
              style: TextStyle( fontSize: 20, color: Theme.of(context).primaryTextTheme.headline1!.color)
            )
          )
        ),
        LimparCarrinho( itensCarrinho.itemcarrinho! ),
        FinalizeButton(
          itensCarrinho.carrinho,
          itensCarrinho.itemcarrinho!.isEmpty
          ? true : false
        ),
      ])
    );
  }
}
