import 'package:flutter/material.dart';
import 'package:commerce_app/src/Templates/Mobile/Components/Snackbar.dart';
import 'package:commerce_app/src/Templates/Mobile/HomePage/HomePage.dart';
import 'package:commerce_app/src/Services/CarrinhoServices.dart';

dynamic cleanCart(BuildContext context, String id) async {
  if (await CarrinhoServices.deleteCarrinho(id) == 200) {
    await Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute( builder: (BuildContext context) => HomePage() ),
      (Route<dynamic> route) => false
    );
  } else {
    showThisSnackBar('Erro');
  }
}