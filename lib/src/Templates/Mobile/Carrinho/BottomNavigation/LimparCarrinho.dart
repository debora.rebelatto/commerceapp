// import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:commerce_app/src/Model/ItemCarrinho/ItemCarrinho.dart';
import 'package:commerce_app/src/Templates/Mobile/Components/cancelButton.dart';
import 'package:commerce_app/src/core/AppStyles.dart';

import 'cleanCart.dart';

class LimparCarrinho extends StatelessWidget {
  final List<ItemCarrinho> itemcarrinho;

  LimparCarrinho(this.itemcarrinho);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: EdgeInsets.only(left: 10),
        child: TextButton(
          style: TextButton.styleFrom(
            shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(15.0), ),
            elevation: 0,
            backgroundColor: Theme.of(context).bottomAppBarColor,
          ),
          onPressed: () async => itemcarrinho.isEmpty
            ? null : await cleanCartDialog(context),
          child: Text('Limpar', style: TextStyle(color: AppStyles.slightlyDarkerRed)),
        ),
      )
    );
  }

  dynamic cleanCartDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text('Limpar carrinho'),
          content: Text('Deseja Limpar o carrinho?\nSe sim, em seguida será levado para a pagina incial',
            textAlign: TextAlign.center
          ),
          actions: [
            TextButton(
              style: TextButton.styleFrom(
                shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(15.0), ),
                backgroundColor: Colors.grey[400],
              ),
              onPressed: () { cleanCart(context, itemcarrinho[0].carrinhoId.toString()); },
              child: Text('Sim', style: TextStyle(color: Colors.white) )
            ),
            cancel()
          ],
        );
      }


      /*AlertDialog(
        title: Text('Limpar carrinho'),
        content: Text('Deseja Limpar o carrinho?\nSe sim, em seguida será levado para a pagina incial',
          textAlign: TextAlign.center
        ),
        actions: [
          TextButton(
            shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(15.0), ),
            child: Text('Sim', style: TextStyle(color: Colors.white) ),
            color: Colors.grey[400],
            onPressed: () { cleanCart(context, itemcarrinho[0].carrinhoId.toString()); }
          ),
          cancel()
        ],
      )*/
    );
  }
}