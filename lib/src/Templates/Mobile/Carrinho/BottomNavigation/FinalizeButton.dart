import 'package:flutter/material.dart';
import 'package:commerce_app/src/Model/Carrinho/Carrinho.dart';
import '../../Finalize/Finalize.dart';

class FinalizeButton extends StatelessWidget {
  final bool isCartEmpty;
  final Carrinho carrinho;

  FinalizeButton(this.carrinho, this.isCartEmpty);

  @override
  Widget build(BuildContext context){
    return Expanded(
      child: Padding(
        padding: EdgeInsets.all(5.0),
        child: MaterialButton(
          minWidth: 10,
          shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(40) ),
          color: isCartEmpty ? Colors.grey : Colors.blue,
          onPressed: isCartEmpty
          ? null
          : () { Navigator.push(context, MaterialPageRoute( builder: (context) => FinalizeCart(cart: carrinho))); },
          child: Text('Finalizar', style: TextStyle(color: Colors.white)),
        )
      ),
    );
  }
}
