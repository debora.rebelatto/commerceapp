import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:commerce_app/src/Model/ItemCarrinho/ItemCarrinho.dart';
import 'package:commerce_app/src/Model/ItensCarrinho/ItensCarrinho.dart';
import 'package:commerce_app/src/Templates/Mobile/ProductDetail/WidgetDetalheProduto.dart';
import './BottomNavigation/BottomNavigation.dart';
import './Components/ProductInfoCarrinho.dart';
import './Components/Dialogs.dart';
import './Components/SlidablePanel.dart';

class Carrinho extends StatefulWidget {
  final ItensCarrinho? itensCarrinho;
  final Function(bool)? rebuild;

  Carrinho({ Key? key, this.itensCarrinho, this.rebuild }) : super(key: key);

  @override
  _CarrinhoState createState() => _CarrinhoState();
}

class _CarrinhoState extends State<Carrinho> {
  @override
  Widget build(BuildContext context) {
    var item = widget.itensCarrinho!.itemcarrinho;
    return Scaffold(
      body: Stack(children: [
        item == null || item.isEmpty ? Center(child: Icon(Icons.warning)) : itemsList(item),
        SlideUp(item!, widget.itensCarrinho!)
      ]),
      bottomNavigationBar: BottomNavigation(widget.itensCarrinho!)
    );
  }

  ListView itemsList(item) {
    return ListView.builder(
      padding: EdgeInsets.only(bottom: 50, top: 10),
      physics: BouncingScrollPhysics(),
      itemCount: item.length,
      itemBuilder: (context, index) {
        ItemCarrinho itemCarrinho = item[index];
        return Slidable(
          actionPane: SlidableDrawerActionPane(),
          actionExtentRatio: 0.25,
          secondaryActions: [ delete(itemCarrinho.id.toString(), context) ],
          child: Container(
            padding: EdgeInsets.fromLTRB(15, 5, 15, 0),
            child: Card(
              elevation: 5,
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.all( Radius.circular(20) )),
              child: Material(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.all( Radius.circular(20) )),
                color: Theme.of(context).cardColor,
                child: Ink(
                  child: InkWell(
                    onTap: () => Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => WidgetDetalhesProduto(item.produto.id.toString()))
                    ),
                    child: FittedBox(
                      child: Padding( padding: EdgeInsets.only(bottom: 20, top: 20),
                      child: ProductInfoCarrinho(itemCarrinho, widget)
                      )
                    )
                  )
                )
              )
            )
          )
        );
      }
    );
  }

  IconSlideAction delete(String id, BuildContext context) => IconSlideAction(
    caption: 'Remover',
    color: Color(0xFFFFE6E6),
    iconWidget:
    Container(
      padding: EdgeInsets.only(bottom: 10),
      child: SvgPicture.asset('assets/icons/Trash.svg'),
    ),
    onTap: () => removeProductDialog(id, context, widget)
  );
}
