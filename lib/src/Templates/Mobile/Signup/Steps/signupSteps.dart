import 'package:flutter/material.dart';
import '../Components/ErrorMessage.dart';

Form signupSteps(var formKey, String errorMessage, Widget fields, String title) {
  return Form(
    key: formKey,
    autovalidateMode: AutovalidateMode.onUserInteraction,
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        errormessage(errorMessage),
        Padding(
          padding: EdgeInsets.only(bottom: 15),
          child: Text(title, style: TextStyle( fontWeight: FontWeight.bold, fontSize: 18) ),
        ),
        fields // Widget that receives the forms
      ]
    )
  );
}
