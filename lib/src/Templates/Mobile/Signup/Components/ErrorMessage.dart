import 'package:flutter/material.dart';

Widget errormessage(String errorMessage) => errorMessage != ''
  ? Padding(
    padding: EdgeInsets.only(bottom: 10),
    child: Text('$errorMessage', style: TextStyle(fontSize: 17, color: Colors.red[800]))
  )
  : Container();