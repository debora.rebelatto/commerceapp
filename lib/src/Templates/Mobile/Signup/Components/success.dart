import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:commerce_app/src/Templates/Mobile/Login/Login.dart';

dynamic success(context) {
  if(!kIsWeb) {
    Navigator.of(context).pop();
    Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(builder: (BuildContext context) => Login(fromSignup: true)),
      ModalRoute.withName('/'),
    );

  } else {
    Navigator.pushNamed(context, '/');
  }
}