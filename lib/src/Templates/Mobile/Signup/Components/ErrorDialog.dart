import 'package:flutter/material.dart';

dynamic errorDialog(BuildContext context, String err) {
  showDialog(
    context: context,
    builder: (context) {
      return AlertDialog(
        title: Text('Erro!'),
        content: Text(err , textAlign: TextAlign.center),
        actions: [ OutlinedButton(onPressed: () { Navigator.pop(context); }, child: Text('OK')) ],
      );
    },
  );
}