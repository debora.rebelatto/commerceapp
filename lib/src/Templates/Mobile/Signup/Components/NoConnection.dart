import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class NoConnection extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Image.asset('assets/images/wifi_off.png'),
        Container(
          padding: EdgeInsets.fromLTRB(20, 10, 20, 20),
          child: Text('Verifique sua conexão com a internet!',
            style: GoogleFonts.lato(
              textStyle: TextStyle(fontSize: 25)
            ),
            textAlign: TextAlign.center,
          )
        ),
        Container(
          padding: EdgeInsets.fromLTRB(20, 10, 20, 20),
          child: Text('Sua conexão com a internet não está disponível, favor verificar ou tentar novamente',
            style: GoogleFonts.lato( color: Colors.grey, textStyle: TextStyle(fontSize: 18) ),
            textAlign: TextAlign.center,
          )
        ),
      ],
    ));
  }
}