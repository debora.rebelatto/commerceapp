import 'package:flutter/material.dart';

Widget checkWaitingValidation(bool waitingValidation, int _currentStep) => waitingValidation == true
  ? Center(child: SizedBox(height: 20.0, width: 20.0, child: CircularProgressIndicator()))
  : Text( _currentStep == 3 ? 'Concluir': 'Continuar', style: TextStyle(color: Colors.white),);