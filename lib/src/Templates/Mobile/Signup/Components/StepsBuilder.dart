import 'package:flutter/material.dart';
import 'package:commerce_app/src/Templates/Mobile/Signup/Fields/cpfcnpjField.dart';
import 'package:commerce_app/src/Templates/Mobile/Signup/Fields/fields.dart';
import 'package:commerce_app/src/Templates/Mobile/Signup/Fields/tipoPessoaRadio.dart';
import 'package:commerce_app/src/Templates/Mobile/Signup/Steps/signupSteps.dart';

List<Step> stepsBuilder(widget, stepsStates, stepsActive, _formKeys, errorMessage) {
  var field = [ PersonalDataFields(widget), TipoPessoa( widget ), cpfCnpjField(widget) ];
  var title = [ 'Dados Pessoais', 'Dados de Cliente', 'Dados de Cliente' ];
  var steps = <Step>[];

  for(var i = 0; i < field.length; i++) {
    steps.add(Step(
      title: Text(''),
      state: stepsStates[i],
      isActive: stepsActive[i],
      content: signupSteps(_formKeys[i], errorMessage[i], field[i], title[i])
    ));
  }
  return steps;
}