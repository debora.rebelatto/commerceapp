import 'package:flutter/material.dart';
import '../Signup.dart';

// ignore: must_be_immutable
class TipoPessoa extends StatefulWidget {
  Signup signup;
  TipoPessoa(this.signup, { Key? key }) : super(key: key);

  @override
  _TipoPessoaState createState() => _TipoPessoaState();
}

class _TipoPessoaState extends State<TipoPessoa> {
  dynamic _setType(String value) { setState(() { widget.signup.type = value; }); }

  @override
  Widget build(BuildContext context) =>
    Column( children: [ radio('F', 'Pessoa Física'), radio('J', 'Pessoa Jurídica') ]);

  Widget radio(String type, String text) {
    return Padding(
      padding: EdgeInsets.only(top: 10, bottom: 10),
      child: Material ( child: Ink( child: InkWell(
        onTap: () async { _setType(type); },
        child: ListTile(
          leading: Radio(
            value: type,
            groupValue: widget.signup.type,
            onChanged: (value) { _setType(value.toString()); }
          ),
          title: Text(text, style: TextStyle(fontSize: 15),),
        ),
      )))
    );
  }
}