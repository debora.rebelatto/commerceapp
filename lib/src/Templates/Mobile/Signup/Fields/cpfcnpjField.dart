import 'package:flutter/material.dart';
import 'package:commerce_app/src/Templates/Components/validatorFields.dart';
import '../Signup.dart';

Padding cpfCnpjField(Signup signup) {
  return Padding(
    padding: EdgeInsets.only(bottom: 15),
    child: TextFormField(
      autofocus: true,
      controller: signup.type == 'F' ? signup.cpfController: signup.cnpjController,
      decoration: decoration(signup.type == 'F' ? 'CPF' : 'CNPJ', hintText: '123.123.123-12'),
      textInputAction: TextInputAction.done,
      validator: (value) => FieldValidator.validateCpfCnpj(value)
    ),
  );
}

InputDecoration decoration(var labelText, { String? hintText }) {
  return InputDecoration(
    filled: true,
    labelText: labelText,
    hintText: hintText ?? '',
    fillColor: Colors.grey[200],
    labelStyle: TextStyle(color: Colors.grey[600]),
  );
}
