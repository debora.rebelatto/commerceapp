import 'package:flutter/material.dart';
import 'package:commerce_app/src/Templates/Components/validatorFields.dart';
import '../Signup.dart';

// ignore: must_be_immutable
class PersonalDataFields extends StatefulWidget {
  Signup signup;

  PersonalDataFields(this.signup);

  @override
  _PersonalDataFieldsState createState() => _PersonalDataFieldsState();
}

class _PersonalDataFieldsState extends State<PersonalDataFields> {
  bool obscure1 = true;
  bool obscure2 = true;

  dynamic _toggle(label){
    label == 'Senha *'
    ? setState(() { obscure1 = !obscure1; })
    : setState(() { obscure2 = !obscure2; });
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      signupTextField('Nome *', widget.signup.nameController),
      signupTextField('Apelido', widget.signup.apelidoController),
      signupTextField('E-mail *', widget.signup.emailController),
      signupTextField(
        'Senha *', widget.signup.passwordController, obscure: obscure1
      ),
      signupTextField(
        'Confirmar Senha *', widget.signup.confirmPasswordController, obscure: obscure2
      ),
      signupTextField('CPF *', widget.signup.userCpfController),
      signupTextField('RG *', widget.signup.rgController),
      signupTextField('Telefone *', widget.signup.phoneController),
    ]);
  }

  Widget signupTextField(label, controller, {obscure}) {
    return Padding(
      padding: EdgeInsets.only(bottom: 15),
      child: TextFormField(
        controller: controller,
        obscureText: obscure ?? false,
        decoration: InputDecoration(
          filled: true,
          labelText: label,
          fillColor: Colors.grey[200],
          labelStyle: TextStyle(color: Colors.grey[600]),
          suffixIcon: obscure != null ? IconButton(
            icon: Icon( obscure ? Icons.visibility : Icons.visibility_off),
            onPressed: () { _toggle(label); },
          ) : null
        ),
        textInputAction: TextInputAction.next,
        onEditingComplete: () => FocusScope.of(context).nextFocus(),
        validator: (value) => validate(label, value),
      ),
    );
  }

  dynamic validate(label, value) {
    switch(label) {
      case 'CPF *': return FieldValidator.validateCPF(value);
      case 'Senha *': return FieldValidator.validatePassword(value);
      case 'Confirmar Senha *':
        return FieldValidator.confirmPassword(value, widget.signup.passwordController.text);
      case 'E-mail *': return FieldValidator.validateEmail(value);
      default: return FieldValidator.validateIfEmpty(value);
    }
  }
}
