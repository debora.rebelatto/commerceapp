
import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/material.dart';
import 'package:commerce_app/src/Services/UsuariosServices.dart';
import '../Components/getConnection.dart';
import './Components/checkWaitingValidation.dart';
import 'Components/ErrorDialog.dart';
import 'Components/NoConnection.dart';
import 'Components/StepsBuilder.dart';

// ignore: must_be_immutable
class Signup extends StatefulWidget {

  Signup({Key? key}) : super(key: key);

  var nameController = TextEditingController(text: '');
  var apelidoController = TextEditingController(text: '');
  var emailController = TextEditingController(text: '');
  var passwordController = TextEditingController(text: '');
  var confirmPasswordController = TextEditingController(text: '');
  var phoneController = TextEditingController(text: '');
  var cepController = TextEditingController(text: '');
  var userCpfController = MaskedTextController(mask: '000.000.000-00', text: '');
  var cpfController = MaskedTextController(mask: '000.000.000-00', text: '');
  var cnpjController = MaskedTextController(mask: '00.000.000/0000-00', text: '');
  var rgController = TextEditingController(text: '');
  var addressController = TextEditingController(text: '');
  var complementController = TextEditingController(text: '');

  String type = 'J';

  @override
  SignUpState createState() => SignUpState();
}

class SignUpState extends State<Signup> {
  final List<GlobalKey<FormState>> _formKeys = [
    GlobalKey<FormState>(), GlobalKey<FormState>(), GlobalKey<FormState>()
  ];
  List<bool> stepsActive = [ true, false, false ];
  var stepsStates = [ StepState.indexed, StepState.indexed, StepState.indexed ];
  List<String> errorMessage = [ '', '', '' ];
  int _currentStep = 0;
  bool waitingValidation = false;
  bool isInternetOn = true;

  @override
  void initState() {
    super.initState();
    getConnection().then((value) => setState(() { isInternetOn = value; }));
  }

  @override
  void dispose() { super.dispose(); }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Cadastre-se')),
      body: isInternetOn == false
      ? NoConnection()
      : ListView( children: [ Container( child: ConstrainedBox(
        constraints: BoxConstraints.tightFor(height: MediaQuery.of(context).size.height * 0.88),
        child: Stepper(
          controlsBuilder: (BuildContext? context, {VoidCallback? onStepContinue, VoidCallback? onStepCancel}) {
            return Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget> [
                TextButton( onPressed: _currentStep == 0 ? null : onStepCancel, child: Text('Cancelar')),
                TextButton(
                  style: TextButton.styleFrom(
                    backgroundColor: Colors.blue[600],
                  ),
                  onPressed: onStepContinue,
                  child: checkWaitingValidation(waitingValidation, _currentStep),
                ),
              ],
            );
          },
          type: StepperType.horizontal,
          currentStep: _currentStep,
          onStepContinue: continueStep,
          onStepCancel: cancelStep,
          steps: stepsBuilder(widget, stepsStates, stepsActive, _formKeys, errorMessage)
        )
      ))])
    );
  }

  void continueStep() async {
    if(waitingValidation == true) return null;
    if (_formKeys[_currentStep].currentState!.validate()) {
      _currentStep == 0 ? stepZero() : stepsValidated();
    } else {
      setState(() {
        errorMessage[_currentStep] = 'Existem erros!';
        stepsStates[_currentStep] = StepState.error;
      });
    }
  }

  void cancelStep() {
    if (_currentStep <= 0) return;
    setState(() {
      stepsActive[_currentStep] = false;
      stepsStates[_currentStep] = StepState.indexed;
      _currentStep -= 1;
    });
  }

  void stepZero() async {
    setState(() { waitingValidation = true;});
    var res = await UsuariosServices.postValidaEmail(widget.emailController.text);
    if(res != null) { setState(() { waitingValidation = false; }); }
    res.statusCode == 200 ? errorDialog(context, 'Email já cadastrado') : stepsValidated();
  }

  void stepsValidated() async{
    _currentStep == 2 ? stepTwo() : setState(() {
      errorMessage[_currentStep] = ''; stepsStates[_currentStep] = StepState.complete;
      _currentStep++; stepsActive[_currentStep] = true;
    });
  }

  void stepTwo() async {
    setState(() {
      errorMessage[_currentStep] = '';
      stepsStates[_currentStep] = StepState.complete;
      waitingValidation = true;
    });

    // var res = await UsuarioController.signup( widget );
    // setState(() { waitingValidation = false; });
    // res.statusCode == 201
    // ? success(context)
    // : errorDialog(
    //   context,
    //   jsonDecode(res.body)['message'] ?? 'Ocorreu um erro'
    // );
  }
}