import 'package:flutter/material.dart';

class FinalizeData extends ChangeNotifier {
  String condicaoId = '';
  String formaId = '';
  late String cartId;
  late String date = '';

  String get getCondicaoId => condicaoId;
  String get getFormaId => formaId;
  String get getCartId => cartId;
  String get getDate => date;

  void setCartId(String id) {
    cartId = id;
    notifyListeners();
  }

  void setCondicaoId(String? id) {
    condicaoId = id!;
    notifyListeners();
  }

  void setFormaId(String? id) {
    formaId = id!;
    notifyListeners();
  }

  void setDate(String newdate) {
    date = newdate;
    notifyListeners();
  }
}