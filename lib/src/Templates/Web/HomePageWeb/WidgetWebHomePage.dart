import 'package:flutter/material.dart';
import 'package:commerce_app/src/Templates/Mobile/Categories/Categories.dart';
import 'package:commerce_app/src/Templates/Mobile/Drawer/Drawer.dart';
import 'package:commerce_app/src/Templates/Web/AppBar/appBar.dart';
import 'package:commerce_app/src/Templates/Web/ProductsWeb/WidgetProductsWeb.dart';

class WidgetWebHomePage extends StatefulWidget {
  @override
  _WidgetWebHomePageState createState() => _WidgetWebHomePageState();
}

class _WidgetWebHomePageState extends State<WidgetWebHomePage>{
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[100],
      appBar: appBar(context, MediaQuery.of(context).size),
      drawer: LeftDrawer(),
      body: SingleChildScrollView(
        child: Container(
          // padding: EdgeInsets.all(10),
          child: Column(
            children: [
              Categories(),
              SizedBox(height: 20),
              WidgetProductsWeb(),
            ]
          )
        )
      )
    );
  }
}