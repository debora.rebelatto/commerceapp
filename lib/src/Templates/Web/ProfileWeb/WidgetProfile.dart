import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:commerce_app/src/Templates/Components/ProfileInfo.dart';
import 'package:commerce_app/src/Templates/Mobile/Drawer/Drawer.dart';
import 'package:commerce_app/src/Templates/Web/AppBar/appBar.dart';
import 'package:commerce_app/src/Templates/Web/ComponentsWeb/header.dart';

class WidgetWebProfile extends StatefulWidget {
  @override
  _WidgetProfileState createState() => _WidgetProfileState();
}

class _WidgetProfileState extends State<WidgetWebProfile> {
  ScrollController? _scrollController;
  // ignore: unused_field
  double _scrollPosition = 0;
  String? nome;
  String? email;
  String? distribuidor;

  dynamic setInfo() async {
    var pref = await SharedPreferences.getInstance();
    setState(() {
      nome = pref.getString('nome');
      email = pref.getString('email');
      distribuidor = pref.getString('nomeDist');
    });
  }

  void _scrollListener() {
    setState(() {
      _scrollPosition = _scrollController!.position.pixels;
    });
  }

  @override
  void initState() {
    _scrollController = ScrollController();
    _scrollController!.addListener(_scrollListener);
    setInfo();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.grey[100],
      appBar: appBar(context, screenSize),
      drawer: LeftDrawer(),
      body: Container(
        padding: EdgeInsets.all(30),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Header(title: 'Perfil'),
              ],
            ),


            SizedBox(height: 20),
            Card(
              child: Container(
                padding: EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    ProfileInfo('Nome: ', nome),
                    ProfileInfo('Email: ', email),
                    ProfileInfo('Distribuidor: ', distribuidor),
                  ]
                ),
              )
            )
          ],
        )
      )
    );
  }
}
