import 'package:flutter/material.dart';
import 'package:commerce_app/src/Templates/Web/FinalizeCartWeb/Components/CardResumo.dart';
import 'package:commerce_app/src/Templates/Web/FinalizeCartWeb/Components/FormFinalize.dart';
import 'package:commerce_app/src/Templates/Web/FinalizeCartWeb/Components/finalizeButton.dart';

// ignore: must_be_immutable
class LargeScreenFinalize extends StatelessWidget {
  var formKey;
LargeScreenFinalize(this.formKey);
  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Expanded(
          child: Container(
            width: MediaQuery.of(context).size.width / 4,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [ FormFinalize(formKey) ],
            )
          )
        ),
        Container(
          width: MediaQuery.of(context).size.width / 3,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [ CardResumo(), finalizeButton(context, formKey) ],
          )
        )
      ],
    );
  }
}
