import 'package:flutter/material.dart';
import 'package:commerce_app/src/Templates/Web/FinalizeCartWeb/Components/CardResumo.dart';
import 'package:commerce_app/src/Templates/Web/FinalizeCartWeb/Components/FormFinalize.dart';
import 'package:commerce_app/src/Templates/Web/FinalizeCartWeb/Components/finalizeButton.dart';

// ignore: must_be_immutable
class SmallScreenFinalize extends StatelessWidget {
  var formKey;
  SmallScreenFinalize(this.formKey);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        FormFinalize(formKey),
        CardResumo(),
        Align(
          alignment: Alignment.topRight,
          child: finalizeButton(context, formKey)
        )
      ],
    );
  }
}
