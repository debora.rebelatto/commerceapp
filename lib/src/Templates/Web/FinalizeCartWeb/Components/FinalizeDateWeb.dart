import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:commerce_app/src/Templates/Components/validatorFields.dart';
import 'package:commerce_app/src/Templates/provider/FinalizeData.dart';

class FinalizeDateWeb extends StatefulWidget{

  FinalizeDateWeb({ Key? key }) : super(key: key);

  @override
  _DateState createState() => _DateState();
}

class _DateState extends State<FinalizeDateWeb> {
  var dateController = MaskedTextController(mask: '00/00/0000', text: '');
  DateFormat dateFormat = DateFormat("dd-MM-y");
  var date;

  @override
  void initState() {
    super.initState();
    var string = dateFormat.format(DateTime.now());
    setState(() {
      dateController.text = string;
    });
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    setDateProvider(dateController.text);
  }

  void setDateProvider(value) {
    Provider.of<FinalizeData>(context, listen: false).setDate(value);
  }

  @override
  Widget build(BuildContext context) {
    return ListTile(
      subtitle: Column( children: [
        Padding(
          padding: EdgeInsets.only(bottom: 15),
          child: TextFormField(
            controller: dateController,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            decoration: InputDecoration(
              filled: true,
              labelText: 'Data',
              fillColor: Colors.grey[100],
              labelStyle: TextStyle(color: Colors.grey[600]),
            ),
            validator: (value) => FieldValidator.dateField(value),
            onChanged: (value) {
              setDateProvider(value);
              setState(() { date = value; });
            },
          )
        )
      ])
    );
  }
}
