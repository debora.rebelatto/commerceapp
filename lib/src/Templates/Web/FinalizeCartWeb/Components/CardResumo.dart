import 'package:flutter/material.dart';
import 'package:commerce_app/src/Controllers/CarrinhoController.dart';
import 'package:commerce_app/src/Model/ItensCarrinho/ItensCarrinho.dart';
import 'package:commerce_app/src/core/AppStyles.dart';

import 'OverviewList.dart';

class CardResumo extends StatefulWidget {
  @override
  _CardResumoState createState() => _CardResumoState();
}

class _CardResumoState extends State<CardResumo> {
  var _future;
  @override
  void initState() {
    super.initState();
    _future = getData();
  }

  Future<ItensCarrinho> getData() {
    return CarrinhoController.getItensCarrinho();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.only(bottom: 10),
          child: Card(
            child: Container(
              padding: EdgeInsets.all(10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: EdgeInsets.only(left: 10),
                    child: Text('Resumo: ', style: AppStyles.boldWebText(20)),
                  ),
                  SizedBox(height: 10),
                  FutureBuilder(
                    future: _future,
                    builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                      switch (snapshot.connectionState) {
                        case ConnectionState.waiting:
                          return Center(child: CircularProgressIndicator());
                        case ConnectionState.done: default:
                          ItensCarrinho itensCarrinho = snapshot.data;
                          return OverviewList(itensCarrinho);
                      }
                    },
                  )
                ]
              )
            )
          ),
        ),
      ]
    );
  }
}
