import 'package:flutter/material.dart';
import 'package:jiffy/jiffy.dart';
import 'package:provider/provider.dart';
import 'package:commerce_app/src/Templates/provider/FinalizeData.dart';
import 'package:commerce_app/src/Templates/Web/HomePageWeb/WidgetWebHomePage.dart';
import 'package:commerce_app/src/Services/CarrinhoServices.dart';
import 'package:commerce_app/src/core/AppStyles.dart';

Widget finalizeButton(context, formKey) {
  return Container(
    height: 40,
    width: MediaQuery.of(context).size.width,
    margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
    child: TextButton(
      style: TextButton.styleFrom(
        backgroundColor: AppStyles.darkBlue,
      ),
      onPressed: () async {
        if (formKey.currentState.validate()) {
          formKey.currentState.save();
          initFinalize(context);
        }

      },
      child: Text('Concluir', style: TextStyle(color: Colors.white))
    )
  );
}

void initFinalize(context) async {
  var res;

  if(res != 201) { waitingDialog(context); }

  await CarrinhoServices.updateCarrinhoById(
    Provider.of<FinalizeData>(context, listen: false).getCartId,
    int.parse(Provider.of<FinalizeData>(context, listen: false).getCondicaoId),
    int.parse(Provider.of<FinalizeData>(context, listen: false).getFormaId),
    Jiffy(Provider.of<FinalizeData>(context, listen: false).getDate, "dd/MM/yyyy").format("y-MM-dd")
  );

  res = await CarrinhoServices.postFinalizeCarrinho(Provider.of<FinalizeData>(context, listen: false).cartId);

  if(res == 201) {
    Provider.of<FinalizeData>(context, listen: false).setCondicaoId('');
    Provider.of<FinalizeData>(context, listen: false).setFormaId('');
    Provider.of<FinalizeData>(context, listen: false).setCartId('');
    Provider.of<FinalizeData>(context, listen: false).setDate('');
    await Navigator.of(context).push(MaterialPageRoute(builder: (context) => WidgetWebHomePage()));
  } else {
    Navigator.pop(context);
    errorDialog(context);
  }
}

void waitingDialog(context) {
  showDialog(
    barrierDismissible: false,
    context: context,
    builder: (context) {
      return AlertDialog(
        title: Center( child: CircularProgressIndicator() ),
        content: Text('Aguarde. Finalizando o carrinho', textAlign: TextAlign.center )
      );
    },
  );
}

void errorDialog(context) {
  showDialog(
    context: context,
    builder: (context) {
      return AlertDialog(
        title: Center( child: Text('Erro!') ),
        content: Text('Ocorreu um erro. Tente novamente.', textAlign: TextAlign.center ),
        actions: [
          TextButton(
            onPressed:() { Navigator.pop(context); },
            child: Text('Continuar')
          )
        ],
      );
    },
  );
}