import 'package:flutter/material.dart';
import 'package:commerce_app/src/Model/ItensCarrinho/ItensCarrinho.dart';
import 'package:commerce_app/src/Templates/Components/currencyFormat.dart';
import 'package:commerce_app/src/Templates/Web/ComponentsWeb/header.dart';
import 'package:commerce_app/src/Templates/Web/ComponentsWeb/responsive.dart';
import 'package:commerce_app/src/core/AppStyles.dart';

class OverviewList extends StatefulWidget {
  final ItensCarrinho itensCarrinho;

  OverviewList(this.itensCarrinho);

  @override
  _OverviewListState createState() => _OverviewListState();
}

class _OverviewListState extends State<OverviewList> {
  var cardWidth;
  var sizeName;
  var sizeQuantity;
  var sizeTotal;

  @override
  void initState() {
    super.initState();
    getSizes();
  }

  void getSizes() async {
    var isSmallScreen = ResponsiveWidget.isSmallScreen(context);

    setState(() {
      cardWidth = MediaQuery.of(context).size.width;

      sizeName = isSmallScreen ? cardWidth / 3 : (cardWidth / 3) / 3;
      sizeQuantity = isSmallScreen ? cardWidth / 8 : (cardWidth / 3) / 4;
      sizeTotal = isSmallScreen ? cardWidth / 4 : (cardWidth / 3) / 6 ;
    });
  }

  @override
  Widget build(BuildContext context) {
    getSizes();

    return Container(
      child: Column(children: [
        tableHeader(cardWidth),
        ListView.builder(
          shrinkWrap: true,
          itemCount: widget.itensCarrinho.itemcarrinho!.length >= 11 ? 11 : widget.itensCarrinho.itemcarrinho!.length,
          itemBuilder: (context, index) {
            return Row(
              mainAxisSize: MainAxisSize.max,
              children: [
                index == 10
                ? Container(
                  padding: EdgeInsets.only(left: 10),
                  alignment: Alignment.topRight,
                  child: ButtonTheme(
                    padding: EdgeInsets.symmetric(vertical: 4.0, horizontal: 8.0),
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    minWidth: 0,
                    height: 0,
                    child: TextButton(
                      style: TextButton.styleFrom(
                        padding: EdgeInsets.all(5),
                      ),
                      onPressed: () async {
                        await showDialog( context: context, builder: (_) => dialog() );
                      },
                      child: Text('Ver mais...'),
                    )
                  )
                )

                : table(widget.itensCarrinho.itemcarrinho![index], cardWidth)
              ],
            );
          },
        ),
        total()
      ],)
    );
  }

  Widget dialog() {
    return AlertDialog(
      title: Header(title: 'Resumo', noPadding: true),
      contentPadding: EdgeInsets.all(20),
      content: Builder(
        builder: (context) {
          var widthCardContext = MediaQuery.of(context).size.width;
          return Container(
            width: widthCardContext,
            child: SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  tableHeader(widthCardContext, tableDialog: true),
                  Container(
                    height: MediaQuery.of(context).size.height / 3.5,
                    child: SingleChildScrollView(
                      child: ListView.builder(
                        shrinkWrap: true,
                        itemCount: widget.itensCarrinho.itemcarrinho!.length,
                        itemBuilder: (context, index) {
                          var item = widget.itensCarrinho.itemcarrinho![index];
                          return table(item, widthCardContext, tableDialog: true);
                        },
                      ),
                    )
                  ),

                  total(),

                  Container(
                    padding: EdgeInsets.only(left: 10),
                    alignment: Alignment.topRight,
                    child: TextButton(
                      style: TextButton.styleFrom(
                        backgroundColor: AppStyles.darkBlue,
                        primary: Colors.white
                      ),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text('Continuar'),
                    )
                  )
                ]
              )
            )
          );
        },
      ),
    );
  }

  Widget total() {
    return Container(
      alignment: Alignment.topRight,
      padding: EdgeInsets.all(10),
      child: Text('Total: ${currencyFormat.format(widget.itensCarrinho.carrinho.total)}',
        style: AppStyles.boldWebText(15)
      )
    );
  }

  Widget table(item, width, { tableDialog }) {
    var sizeTotalDialog = width / 4;

    if(tableDialog != null && width < 780) {
      sizeTotalDialog = width / 8;
    }

    return InkWell(
      onTap: () {},
      child: Row(
        children: [
          info( tableDialog != null ? width / 4 : sizeName, item.produto.nome ),
          info( tableDialog != null ? width / 4 : sizeQuantity, '${item.quantidade}', align: TextAlign.center ),
          info( tableDialog != null ? sizeTotalDialog : sizeTotal, '${currencyFormat.format(item.total)}', align: TextAlign.center ),
        ],
      )
    );
  }

  Widget tableHeader(width, { tableDialog }) {
    var sizeTotalDialog = width / 4;
    if(tableDialog != null && width < 780) {
      sizeTotalDialog = width / 8;
    }

    return Container(
      width: width,
      padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
      child: Row(children: [
        info( tableDialog != null ? width / 4 : sizeName, 'Nome', isBold: true),
        info( tableDialog != null ? width / 4 : sizeQuantity, 'Quantidade', isBold: true, align: TextAlign.center),
        info( tableDialog != null ? sizeTotalDialog : sizeTotal, 'Total', isBold: true,align: TextAlign.center),
      ])
    );
  }

  Widget info(double size, String text, { bool? isBold, align }) {
    return Container(
      padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
      child: SizedBox(
        width: size,
        child: Text( text,
          style: TextStyle(fontWeight: isBold != null ? FontWeight.bold : FontWeight.normal ),
          textAlign: align != true ? align : TextAlign.left,
          overflow: TextOverflow.ellipsis,
          softWrap: false
        )
      )
    );
  }
}
