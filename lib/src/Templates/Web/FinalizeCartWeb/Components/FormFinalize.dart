import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:commerce_app/src/Controllers/PaymentController.dart';
import 'package:commerce_app/src/Model/CondicaoPagamento/CondicaoPagamento.dart';
import 'package:commerce_app/src/Templates/Components/validatorFields.dart';
import '../../../provider/FinalizeData.dart';
import 'FinalizeDateWeb.dart';

// ignore: must_be_immutable
class FormFinalize extends StatefulWidget {
  var formKey;

  FormFinalize(this.formKey, { Key? key });

  @override
  _FormFinalizeState createState() => _FormFinalizeState();
}

class _FormFinalizeState extends State<FormFinalize> {
  var formaDropDownItems;
  int? selectedFormaId;
  int? selectedCondicaoId;
  var cartId;

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    cartId = Provider.of<FinalizeData>(context, listen: false).getCartId;
  }

  Future<List<CondicaoPagamento>> getCondicao() async {
    return await PaymentController.fetchCondicaoPagamento(cartId);
  }

  void getForma() async {
    var data = await PaymentController.fetchFormaPagamentoByCondicaoPagamento(
      cartId,
      selectedCondicaoId
    );

    setState(() {
      formaDropDownItems = data.map((map) => DropdownMenuItem(
        value: map.id,
        child: Text(map.nome!)
      )).toList();
    });
  }

  void setCondicaoProvider(String newValue) {
    Provider.of<FinalizeData>(context, listen: false).setCondicaoId(newValue);
  }

  void setFormaProvider(String newValue) {
    Provider.of<FinalizeData>(context, listen: false).setFormaId(newValue);
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Form(
        key: widget.formKey,
        autovalidateMode: AutovalidateMode.always,
        child: ListView(
          shrinkWrap: true,
          children: [
            ListTile(subtitle: Text('Condição de Pagamento')),

            ListTile(
              subtitle: FutureBuilder<List<CondicaoPagamento>>(
                future: getCondicao(),
                builder: (context, snapshot) {
                  switch(snapshot.connectionState) {
                    case ConnectionState.waiting:
                      return Center(child: CircularProgressIndicator());
                    case ConnectionState.done:

                      return snapshot.hasData
                        ? DropdownButtonFormField<dynamic>(
                          value: selectedCondicaoId,
                          hint: Text('Condição de Pagamento'),
                          onChanged: (dynamic newValue) {
                            setState(() {
                              selectedCondicaoId = newValue;
                              selectedFormaId = null;
                            });
                            getForma();
                            setCondicaoProvider(newValue.toString());
                          },
                          validator: (value) => FieldValidator.validateDropDown(value),
                          items: snapshot.data!.map((map) => DropdownMenuItem(
                            value: map.id,
                            child: Text(map.nome!)
                          )).toList(),
                        )
                        : Center(child: Icon(Icons.warning));
                    default: return Container();

                  }
                }
              ),
            ),

            ListTile(subtitle: Text('Forma de Pagamento')),

            ListTile(
              subtitle: Ink(child: InkWell(
                onTap: () {
                  getForma();
                },
                child: DropdownButtonFormField<dynamic>(
                  value: selectedFormaId,
                  hint: Text('Forma de Pagamento',),
                  onChanged: (dynamic newValue) {
                    setState(() { selectedFormaId = newValue; });
                    setFormaProvider(newValue.toString());
                  },
                  validator: (value) => FieldValidator.validateDropDown(value),
                  items: formaDropDownItems,
                )
              ))
            ),

            ListTile(subtitle: Text('Data de Entrega')),
            FinalizeDateWeb(),
          ]
        )
      ),
    );
  }
}
