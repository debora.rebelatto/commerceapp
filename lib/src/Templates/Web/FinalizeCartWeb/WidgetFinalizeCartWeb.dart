import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:commerce_app/src/Model/Carrinho/Carrinho.dart';
import 'package:commerce_app/src/Templates/Mobile/Drawer/Drawer.dart';
import 'package:commerce_app/src/Templates/Web/AppBar/appBar.dart';
import 'package:commerce_app/src/Templates/Web/ComponentsWeb/header.dart';
import 'package:commerce_app/src/Templates/Web/ComponentsWeb/responsive.dart';
import 'package:commerce_app/src/Templates/provider/FinalizeData.dart';
import 'Screen/LargeScreenFinalize.dart';
import 'Screen/SmallScreenFinalize.dart';

// ignore: must_be_immutable
class WidgetFinalizeCart extends StatefulWidget {
  Carrinho cart;

  WidgetFinalizeCart({ Key? key, required this.cart }) : super(key: key);

  @override
  _WidgetFinalizeCartState createState() => _WidgetFinalizeCartState();
}

class _WidgetFinalizeCartState extends State<WidgetFinalizeCart> {
  ScrollController? _scrollController;
  final formKey = GlobalKey<FormState>();

  @override
  void initState() {
    _scrollController = ScrollController();
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    Provider.of<FinalizeData>(context, listen: false).setCartId(widget.cart.id.toString());
  }

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.grey[100],
      appBar: appBar(context, screenSize),
      drawer: LeftDrawer(),
      body: SingleChildScrollView(
          controller: _scrollController,
          child: Container(
            padding: EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Header(title: 'Finalizar Carrinho'),
                ResponsiveWidget.isSmallScreen(context)
                  ? SmallScreenFinalize(formKey)
                  : LargeScreenFinalize(formKey),
              ],
            )
          )
      ),
    );
  }
}
