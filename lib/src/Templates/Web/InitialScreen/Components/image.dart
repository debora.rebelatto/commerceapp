import 'package:flutter/widgets.dart';

Widget image(width, border) {
  return Expanded(
    child: Container(
      width: width / 3,
      decoration: BoxDecoration(
        borderRadius: border,
        image: DecorationImage(
          image: AssetImage('assets/gradient2.jpg'), fit: BoxFit.cover
        )
      ),
    )
  );
}