import 'package:flutter/material.dart';
import 'package:commerce_app/src/Templates/Mobile/ConfiguracaoHost/HostPorta.dart';

class SettingsButton extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topRight,
      child: IconButton(icon: Icon(Icons.settings),
        onPressed: () async {
          await Navigator.push(context, MaterialPageRoute(builder: (context) => HostPorta()));
        },
      ),
    );
  }
}
