import 'package:flutter/material.dart';
import 'package:commerce_app/src/Controllers/LoginController.dart';
import 'package:commerce_app/src/Templates/Web/ComponentsWeb/responsive.dart';
import 'package:commerce_app/src/Templates/Web/InitialScreen/Components/image.dart';
import 'package:commerce_app/src/Templates/Web/LoginWeb/LoginFormWeb.dart';
import 'package:commerce_app/src/Templates/Web/SignupWeb/WidgetSignupWeb.dart';
import 'Components/settingsButton.dart';
import 'Components/tabs.dart';

class InitialScreenWidget extends StatefulWidget {
  static const String route = '/login';
  @override
  _InitialScreenWidgetScreen createState() => _InitialScreenWidgetScreen();
}

class _InitialScreenWidgetScreen extends State<InitialScreenWidget> {
  bool isLoginTab = true;
  var shouldAppear;

  @override
  void initState() {
    super.initState();
    LoginController.checkTokenValidity(context);
  }

  void changeTab(int activeTabNum) {
    setState(() {
      isLoginTab = !isLoginTab;
    });
  }

  void resize() {
    setState(() {
      shouldAppear = ResponsiveWidget.isSmallScreen(context) ? false : true;
    });
  }

  var border = BorderRadius.only(
    topRight: Radius.circular(40),
    bottomRight: Radius.circular(40),
  );

  @override
  Widget build(BuildContext context) {
    resize();
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colors.white,
      body: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          shouldAppear ? image(width, border) : Container(),
          Center(
            child: Container(
              alignment: Alignment.center,
              width: shouldAppear ? width / 2 : width,
              child: Stack(children: [
                Padding(
                  padding: const EdgeInsets.all(10),
                  child: Align(
                    alignment: Alignment.center,
                    child: SingleChildScrollView(
                      child: Column(children: [
                        SizedBox(height: 20),
                        Container(
                          constraints: BoxConstraints(maxWidth: 500),
                          child: Tabs(press: (value) { changeTab(value); })
                        ),
                        SizedBox(height: 20),
                        isLoginTab ? LoginForm() : WidgetSignupWeb(),
                      ])
                    )
                  ),
                ),
                SettingsButton(),
              ])
            )
          ),
        ]
      )
    );
  }
}