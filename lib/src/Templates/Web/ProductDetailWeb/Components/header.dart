import 'package:flutter/material.dart';
import 'package:commerce_app/src/Templates/Components/currencyFormat.dart';
import 'package:commerce_app/src/core/AppStyles.dart';

Widget header(item, context) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    mainAxisAlignment: MainAxisAlignment.start,
    children: [
      Text(item.produto.nome,
        overflow: TextOverflow.ellipsis,
        softWrap: false,
        maxLines: 2,
        style: AppStyles.boldWebText(28)
      ),
      Text('${currencyFormat.format(item.valorVenda)}',
        overflow: TextOverflow.ellipsis,
        softWrap: false,
        maxLines: 2,
        style: AppStyles.boldWebText(25, color: Theme.of(context).primaryTextTheme.subtitle2!.color)
      ),
    ]
  );
}