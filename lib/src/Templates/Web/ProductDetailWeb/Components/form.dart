import 'package:flutter/material.dart';
import 'package:commerce_app/src/Templates/Components/validatorFields.dart';

Widget formDetail(_formKey, context, quantidade, width) {
  return Form(
    key: _formKey,
    child: Container(
      width: width,
      child: TextFormField(
        controller: quantidade,
        validator: (value) => FieldValidator.validateIfEmpty(value),
        decoration: InputDecoration(filled: true, hintText: 'Quantidade'),
      )
    ),
  );
}