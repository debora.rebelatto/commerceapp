import 'package:flutter/material.dart';
import 'package:commerce_app/src/Templates/Components/WidgetImages/DownloadImage.dart';

Future imageDialog(context, id) {
  return showDialog(
    context: context,
    builder: (context){
      return AlertDialog(
        content: Container(child: Column(children: [
          Align(
            alignment: Alignment.centerRight,
            child: IconButton(
              icon: Icon(Icons.clear),
              onPressed: () { Navigator.pop(context); },
            ),
          ),
          Expanded(child: DownloadImage(id, width: 300, height: 300))
        ]))
      );
    }
  );
}