import 'package:flutter/material.dart';
import 'package:commerce_app/src/Templates/Components/WidgetImages/DownloadImage.dart';
import 'imageDialog.dart';

Widget imageProductDetail(context, id, { alignmment, width }) {
  return Container(
    alignment: alignmment,
    width: width,
    decoration: BoxDecoration(color: Colors.white),
    child: Column(children: [
      GestureDetector(
        onTap: () { imageDialog(context, id); },
        child: Center(
          child: Container(
            padding: EdgeInsets.all(20),
            width: MediaQuery.of(context).size.width - 20,
            child: DownloadImage(id, width: 300, height: 300)
          )
        ),
      )
    ])
  );
}