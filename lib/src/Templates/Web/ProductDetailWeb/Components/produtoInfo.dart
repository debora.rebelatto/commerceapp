import 'package:flutter/material.dart';
import 'package:commerce_app/src/Model/ItemTabelaPreco/ItemTabelaPreco.dart';
import 'package:commerce_app/src/core/AppStyles.dart';

Widget detalhes(ItemTabelaPreco item) {
  return Container( child: Column(
  crossAxisAlignment: CrossAxisAlignment.start,
  children: [
    produtoInfo('Descrição: ', item.produto!.informacaoAdicional),
    produtoInfo('Descrição Embalagem: ', item.produto!.descricaoEmbalagem),
    produtoInfo('Cest: ',item.produto!.cest),
    produtoInfo('Dun: ', item.produto!.dun),
    produtoInfo('Ncm ', item.produto!.ncm),
    produtoInfo('Grupo: ', item.produto!.grupo != null ? item.produto!.grupo!.nome : ''),
    produtoInfo('Marca: ', item.produto!.marca != null ? item.produto!.marca!.nome : ''),
    produtoInfo('Código', item.produto!.codigo),
    produtoInfo('Código de Barras ', item.produto!.barras),
  ],
));
}

Widget produtoInfo(String text, var info) {
  info ??= '';
  return Builder(builder: (context) {
    return Padding(
      padding: EdgeInsets.only(top: 5),
      child: RichText(
        text: TextSpan(
          text: text,
          style: AppStyles.boldWebText(18, color: Theme.of(context).primaryTextTheme.subtitle1!.color),
          children: [
            TextSpan( text: '$info',
              style: AppStyles.boldWebText(18, color: Theme.of(context).primaryTextTheme.subtitle1!.color )
            ),
          ],
        ),
      )
    );
  });
}

