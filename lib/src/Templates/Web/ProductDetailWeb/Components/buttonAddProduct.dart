import 'package:flutter/material.dart';
import 'package:commerce_app/src/Templates/Components/increseDetail.dart';
import 'package:commerce_app/src/core/AppStyles.dart';

Widget buttonAddProduct(_formKey, context, item, quantidade, size) {
  return Container(
    width: size,
    child: TextButton(
      style: TextButton.styleFrom(
        shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(10)),
        backgroundColor: AppStyles.darkBlue
      ),
      onPressed: () async {
        if (_formKey.currentState.validate()) {
          _formKey.currentState.save();
          ScaffoldMessenger.of(context).showSnackBar(
            await increse(item, quantidade)
          );
        }
      },
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(Icons.add_shopping_cart_outlined, color: Colors.white,),
          SizedBox(width: 20),
          Text('Adicionar ao carrinho', style: TextStyle(color: Colors.white),)
        ],
      ),
    ),
  );
}

