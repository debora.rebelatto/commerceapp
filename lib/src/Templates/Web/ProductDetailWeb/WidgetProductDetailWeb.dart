import 'package:flutter/material.dart';
import 'package:commerce_app/src/Model/ItemTabelaPreco/ItemTabelaPreco.dart';
import 'package:commerce_app/src/Templates/Web/ComponentsWeb/responsive.dart';
import 'Screens/LargeScreenProductDetail.dart';
import 'Screens/SmallScreenProductDetail.dart';

class WidgetProductDetailWeb extends StatefulWidget {
  final ItemTabelaPreco item;

  WidgetProductDetailWeb(this.item);

  @override
  _WidgetProductDetailWebState createState() => _WidgetProductDetailWebState();
}

class _WidgetProductDetailWebState extends State<WidgetProductDetailWeb>{
  ScrollController? _scrollController;

  @override
  void initState() {
    _scrollController = ScrollController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      controller: _scrollController,
      child: Container(
        padding: EdgeInsets.all(20),
        child: ResponsiveWidget.isSmallScreen(context)
          ? SmallScreenProductDetail(widget.item) : LargeScreenProductDetail(widget.item)
      )
    );
  }
}