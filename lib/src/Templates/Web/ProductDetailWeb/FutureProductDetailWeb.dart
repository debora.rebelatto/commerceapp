import 'package:flutter/material.dart';
import 'package:commerce_app/src/Controllers/ProductsController.dart';
import 'package:commerce_app/src/Model/ProdutoItemTabelaPreco/ProdutoItemTabelaPreco.dart';
import 'package:commerce_app/src/Templates/Mobile/Drawer/Drawer.dart';
import 'package:commerce_app/src/Templates/Mobile/ProductDetail/Components/Erro.dart';
import 'package:commerce_app/src/Templates/Web/AppBar/appBar.dart';
import 'WidgetProductDetailWeb.dart';

class FutureProductDetailWeb extends StatefulWidget {
  final String id;

  FutureProductDetailWeb(this.id);

  @override
  _FutureProductDetailWebState createState() => _FutureProductDetailWebState();
}

class _FutureProductDetailWebState extends State<FutureProductDetailWeb> {

  Future<dynamic>? _future;
  Future<dynamic> _getData() async => await ProductsController.getItem(widget.id.toString());

  @override
  void initState() {
    super.initState();
    _future = _getData();
  }

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.grey[50],
      drawer: LeftDrawer(),
      appBar: appBar(context, screenSize),
      body: FutureBuilder(
        future: _future,
        builder:(context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              return Center(child: CircularProgressIndicator());
            case ConnectionState.done: default:
              var item = snapshot.data as ProdutoItemTabelaPreco;
              return snapshot.hasError ? erro() : WidgetProductDetailWeb(item.itemTabelaPreco!);
          }
        }
      ),
    );
  }
}