import 'package:flutter/material.dart';
import 'package:commerce_app/src/Model/ItemTabelaPreco/ItemTabelaPreco.dart';
import 'package:commerce_app/src/Templates/Mobile/ProductDetail/Components/Detalhes.dart';
import 'package:commerce_app/src/Templates/Web/ProductDetailWeb/Components/buttonAddProduct.dart';
import 'package:commerce_app/src/Templates/Web/ProductDetailWeb/Components/form.dart';
import 'package:commerce_app/src/Templates/Web/ProductDetailWeb/Components/header.dart';
import 'package:commerce_app/src/Templates/Web/ProductDetailWeb/Components/imageDetail.dart';

class SmallScreenProductDetail extends StatelessWidget {
  final ItemTabelaPreco item;
  final _formKey = GlobalKey<FormState>();
  final quantidade = TextEditingController(text: '');

  SmallScreenProductDetail(this.item, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          imageProductDetail(context, item.produto!.id.toString(), alignmment: Alignment.center),
          SizedBox(height: 30), header(item, context),
          SizedBox(height: 20), formDetail(_formKey, context, quantidade, MediaQuery.of(context).size.width ),
          SizedBox(height: 20), buttonAddProduct(_formKey, context, item, quantidade, MediaQuery.of(context).size.width),
          SizedBox(height: 10), detalhes(item),
        ],
      )
    );
  }
}