import 'package:flutter/material.dart';
import 'package:commerce_app/src/Model/ItemTabelaPreco/ItemTabelaPreco.dart';
import 'package:commerce_app/src/Templates/Mobile/ProductDetail/Components/Detalhes.dart';
import 'package:commerce_app/src/Templates/Web/ProductDetailWeb/Components/buttonAddProduct.dart';
import 'package:commerce_app/src/Templates/Web/ProductDetailWeb/Components/form.dart';
import 'package:commerce_app/src/Templates/Web/ProductDetailWeb/Components/header.dart';
import 'package:commerce_app/src/Templates/Web/ProductDetailWeb/Components/imageDetail.dart';

class LargeScreenProductDetail extends StatelessWidget {
final ItemTabelaPreco item;
  final quantidade = TextEditingController(text: '');
  final _formKey = GlobalKey<FormState>();

  LargeScreenProductDetail(this.item);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        imageProductDetail(context, item.produto!.id.toString(), width: MediaQuery.of(context).size.width / 3 ),
        SizedBox(width: 50),
        Container(
          width: MediaQuery.of(context).size.width / 2,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              header(item, context),
              SizedBox(height: 20),
              formDetail(_formKey, context, quantidade, MediaQuery.of(context).size.width / 4),
              SizedBox(height: 20),
              buttonAddProduct(_formKey, context, item, quantidade, MediaQuery.of(context).size.width / 4),
              SizedBox(height: 10),
              detalhes(item),
            ]
          )
        )
      ],
    );
  }
}