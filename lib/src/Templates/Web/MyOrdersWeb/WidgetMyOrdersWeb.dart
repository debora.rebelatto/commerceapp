import 'package:flutter/material.dart';
import 'package:commerce_app/src/Model/Carrinho/Carrinho.dart';
import 'package:commerce_app/src/Templates/Web/ComponentsWeb/header.dart';

class WidgetMyOrdersWeb extends StatefulWidget {
  final List<Carrinho> listCarrinhos;

  WidgetMyOrdersWeb(this.listCarrinhos);

  @override
  _WidgetMyOrdersWebState createState() => _WidgetMyOrdersWebState();
}

class _WidgetMyOrdersWebState extends State<WidgetMyOrdersWeb> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(40),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Header(title: 'Meus Pedidos'),

          ListView.builder(
          shrinkWrap: true,
          itemCount: widget.listCarrinhos.length,
          itemBuilder: (context, index) {
            var carrinho = widget.listCarrinhos[index];
            return Material(
              child: Ink(
                child: InkWell(
                  child: Card(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('${carrinho.id}'),
                        Text("${carrinho.total}"),
                        Text("${carrinho.dataEntrega}"),
                        Text("${carrinho.condicaoPagamentoId}"),
                        Text("${carrinho.formaPagamentoId}")
                      ],
                    ),
                  )
                )
              ),
            );
          }
        )
      ],)
    );
  }
}