import 'package:flutter/material.dart';
import 'package:commerce_app/src/Controllers/CarrinhoController.dart';
import 'package:commerce_app/src/Model/Carrinho/Carrinho.dart';
import 'package:commerce_app/src/Templates/Web/AppBar/appBar.dart';

import 'WidgetMyOrdersWeb.dart';

class FutureMyOrdersWeb extends StatefulWidget {
  @override
  _FutureMyOrdersWebState createState() => _FutureMyOrdersWebState();
}

class _FutureMyOrdersWebState extends State<FutureMyOrdersWeb> {
  ScrollController? _scrollController;
  Future<dynamic>? _future;


  Future<dynamic> _getData() async => CarrinhoController.getCarrinhos();

  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController();
    _future = _getData();
  }

  @override
  Widget build(BuildContext context) {
  var screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.grey[100],
      appBar: appBar(context, screenSize),
      body: SingleChildScrollView(
        controller: _scrollController,
        child: future(),
      ),
    );
  }

  Widget future(){
    return FutureBuilder(
      future: _future,
      builder: (context, snapshot) {
        var list = snapshot.data as List<Carrinho>;
        switch (snapshot.connectionState) {
          case ConnectionState.waiting:
            return Center(child: CircularProgressIndicator());
          case ConnectionState.done: default:
            return WidgetMyOrdersWeb(list);
        }
      },
    );
  }
}