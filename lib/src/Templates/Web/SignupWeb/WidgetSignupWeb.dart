import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:commerce_app/src/Templates/Components/version.dart';
import 'package:commerce_app/src/core/AppStyles.dart';
import 'FormSignup.dart';
import 'fields/passwordFields.dart';
import 'fields/signupTextField.dart';

// ignore: must_be_immutable
class WidgetSignupWeb extends StatefulWidget {
  var nameController = TextEditingController(text: '');
  var apelidoController = TextEditingController(text: '');
  var emailController = TextEditingController(text: '');
  var passwordController = TextEditingController(text: '');
  var confirmPasswordController = TextEditingController(text: '');
  var phoneController = TextEditingController(text: '');
  var cepController = TextEditingController(text: '');
  var userCpfController = MaskedTextController(mask: '000.000.000-00', text: '');
  var cpfController = MaskedTextController(mask: '000.000.000-00', text: '');
  var cnpjController = MaskedTextController(mask: '00.000.000/0000-00', text: '');
  var rgController = TextEditingController(text: '');
  var addressController = TextEditingController(text: '');
  var complementController = TextEditingController(text: '');

  var type = 'J';
  @override
  _WidgetSignupWebState createState() => _WidgetSignupWebState();
}

class _WidgetSignupWebState extends State<WidgetSignupWeb> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints(maxWidth: 500),
      child: Form(
        key: _formKey,
        autovalidateMode: AutovalidateMode.disabled,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text('Cadastre-se', style: GoogleFonts.inriaSans(
              textStyle: TextStyle( fontSize: 30, fontWeight: FontWeight.bold)
            )),

            SizedBox(height: 20),

            signupTextField('Nome *', widget.nameController, context),
            signupTextField('Apelido', widget.apelidoController, context),
            signupTextField('E-mail *', widget.emailController, context),

            PasswordField('Senha *', widget.passwordController, widget.confirmPasswordController),
            PasswordField('Confirmar Senha *', widget.passwordController, widget.confirmPasswordController),

            continueSignup(context),

            SizedBox(height: 20),
            Align(alignment: Alignment.bottomCenter, child: version())
          ]
        )
      )
    );
  }

  Widget continueSignup(context) => Container(
    width: MediaQuery.of(context).size.width,
    child: TextButton(
      style: TextButton.styleFrom(
        backgroundColor: AppStyles.darkBlue,
        shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(10) ),
      ),
      onPressed: () async {
        if (_formKey.currentState!.validate()) {
          await Navigator.push(context, MaterialPageRoute(builder: (context) => FormSignup(widget)));
        }
      },
      child: Text( 'Continuar', style: TextStyle( color: Colors.white ))
    ),
  );
}