import 'package:flutter/material.dart';
import 'package:commerce_app/src/Templates/Components/validatorFields.dart';

Widget signupTextField(String label, controller, context) {
  final inputBorder = OutlineInputBorder(
    borderRadius: BorderRadius.circular(15.0)
  );

  return Padding(
    padding: EdgeInsets.only(bottom: 20),
    child: TextFormField(
      controller: controller,
      decoration: InputDecoration(
        labelText: label,
        labelStyle: TextStyle(color: Colors.grey[600]),
        focusedBorder: inputBorder,
        enabledBorder: inputBorder,
        border: inputBorder
      ),
      textInputAction: TextInputAction.next,
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
      validator: (value) => validate(label, value)
    ),
  );
}

dynamic validate(label, value) {
  switch(label) {
    case 'Apelido': return null;
    case 'CPF *': return FieldValidator.validateCPF(value);
    case 'E-mail *': return FieldValidator.validateEmail(value);
    default: return FieldValidator.validateIfEmpty(value);
  }
}