import 'package:flutter/material.dart';
import 'package:commerce_app/src/Templates/Components/validatorFields.dart';

class PasswordField extends StatefulWidget {
  final String label;
  final controllerPassword;
  final contollerConfirmPassword;

  PasswordField(
    this.label,
    this.controllerPassword,
    this.contollerConfirmPassword
  );

  @override
  _PasswordFieldState createState() => _PasswordFieldState();
}

class _PasswordFieldState extends State<PasswordField> {
  var obscure1 = true;
  var obscure2 = true;

  void _toggle() {
    widget.label == 'Senha *'
      ? setState(() { obscure1 = !obscure1; })
      : setState(() { obscure2 = !obscure2; });
  }

  @override
  Widget build(BuildContext context) {
    var obscureText = widget.label == 'Senha *' ? obscure1 : obscure2;
    return Padding(
      padding: EdgeInsets.only(bottom: 20),
      child: TextFormField(
        obscureText: obscureText,
        controller: widget.label == 'Senha *'
          ? widget.controllerPassword : widget.contollerConfirmPassword,
        decoration: decoration(obscureText),
        textInputAction: TextInputAction.next,
        onEditingComplete: () => FocusScope.of(context).nextFocus(),
        validator: (value) => validate(widget.label, value)
      ),
    );
  }

  dynamic validate(label, value, { firstPassword }) {
    switch(label) {
      case 'Senha *': return FieldValidator.validatePassword(value);
      case 'Confirmar Senha *': return FieldValidator.confirmPasswordSignup(
        widget.controllerPassword.text, widget.contollerConfirmPassword.text
      );
      default: return FieldValidator.validateIfEmpty(value);
    }
  }

  InputDecoration decoration(obscure) {
    return InputDecoration(
      suffixIcon: IconButton(
        icon: Icon( obscure ? Icons.visibility : Icons.visibility_off ),
        onPressed: _toggle,
      ),
      labelText: widget.label,
      labelStyle: TextStyle(color: Colors.grey[600]),
      focusedBorder: inputBorder,
      enabledBorder: inputBorder,
      border: inputBorder
    );
  }

  final inputBorder = OutlineInputBorder(
    borderRadius: BorderRadius.circular(15.0)
  );
}
