import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:commerce_app/src/Templates/Web/ComponentsWeb/responsive.dart';
import 'package:commerce_app/src/Templates/Web/InitialScreen/Components/image.dart';
import 'package:commerce_app/src/core/AppStyles.dart';
import 'WidgetSignupWeb.dart';
import 'fields/signupTextField.dart';

class FormSignup extends StatefulWidget {
  final WidgetSignupWeb form;
  FormSignup(this.form);
  @override
  _FormSignupState createState() => _FormSignupState();
}

class _FormSignupState extends State<FormSignup> {
  final _formKey = GlobalKey<FormState>();
  var shouldAppear;

  void resize() {
    setState(() {
      shouldAppear = ResponsiveWidget.isSmallScreen(context) ? false : true;
    });
  }

  var border = BorderRadius.only(
    topLeft: Radius.circular(40.0),
    bottomLeft: Radius.circular(40.0),
  );

  dynamic _setType(String value) { setState(() { widget.form.type = value; }); }

  @override
  Widget build(BuildContext context) {
    resize();
    var width = MediaQuery.of(context).size.width;

    return Scaffold(
      body: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Center(
            child: Container(
              alignment: Alignment.center,
              width: shouldAppear ? width / 2 : width,
              child: Stack(children: [
                Align(
                  alignment: Alignment.center,
                  child: SingleChildScrollView(
                    child: Container(
                      constraints: BoxConstraints(maxWidth: 500),
                      padding: EdgeInsets.all(40),
                      child: Form(
                        key: _formKey,
                        child: Column(
                          children: [
                            SizedBox(height: 20),
                            Text('Cadastro', style: GoogleFonts.inriaSans(
                              textStyle: TextStyle( fontSize: 30, fontWeight: FontWeight.bold)
                            )),
                            SizedBox(height: 30),
                            signupTextField('Telefone *', widget.form.phoneController, context),
                            signupTextField('Endereço *', widget.form.addressController, context),
                            signupTextField('Complemento *', widget.form.complementController, context),
                            signupTextField('RG *', widget.form.rgController, context),
                            signupTextField('CEP *', widget.form.cepController, context),
                            signupTextField('CPF *', widget.form.userCpfController, context),

                            Column( children: [
                              radio('F', 'Pessoa Física'), radio('J', 'Pessoa Jurídica')
                            ]),

                            SizedBox(height: 10),

                            widget.form.type == 'F'
                            ? signupTextField('CPF *', widget.form.cpfController, context)
                            : signupTextField('CNPJ *', widget.form.cnpjController, context),

                            continueSignup()

                          ]
                        )
                      )
                    )
                  )
                ),
              ])
            )
          ),
          shouldAppear ? image(width, border) : Container(),
        ]
      )
    );
  }

  Widget radio(String radioType, String text) {
    return Padding(
      padding: EdgeInsets.only(top: 10, bottom: 10),
      child: Material ( child: Ink( child: InkWell(
        onTap: () async { await _setType(radioType); },
        child: ListTile(
          leading: Radio(
            value: radioType,
            groupValue: widget.form.type,
            onChanged: (value) async {
              await _setType(value.toString());
            }
          ),
          title: Text(text, style: TextStyle(fontSize: 15),),
        ),
      )))
    );
  }


  Widget continueSignup() => Container(
    width: MediaQuery.of(context).size.width,
    child: TextButton(
      style: TextButton.styleFrom(
        backgroundColor: AppStyles.darkBlue,
        shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(10) ),
      ),
      onPressed: () async {
        if (_formKey.currentState!.validate()) {
          // var res = await UsuarioController.signup( widget.form );
          // res.statusCode == 201
          // ? success(context)
          // : errorDialog(
          //   context,
          //   jsonDecode(res.body)['message'] ?? 'Ocorreu um erro'
          // );
        }
      },
      child: Text( 'Concluír', style: TextStyle( color: Colors.white ))
    ),
  );
}