import 'package:flutter/material.dart';
import 'package:commerce_app/src/core/AppStyles.dart';

class Header extends StatelessWidget {
  final String? title;
  final bool? noPadding;

  Header({
    Key? key,
    this.title,
    this.noPadding
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: noPadding != null ? EdgeInsets.zero : EdgeInsets.only(left: 20, bottom: 10),
      child: Text(title!, style: AppStyles.boldWebText(22)),
    );
  }
}
