import 'package:flutter/material.dart';
import 'package:commerce_app/src/Controllers/FiltersController.dart';

Widget filter(widget) {
  return PopupMenuButton(
    icon: Icon(Icons.filter_list),
    onSelected: (result) async {
      if(result == 1) { await FiltersController.filter('Nome', 'asc', '50');
      } else if(result == 2) { await FiltersController.filter('Nome', 'desc', '50');
      } else if(result == 3) { await FiltersController.filter('ValorVenda', 'desc', '50');
      } else { FiltersController.filter('ValorVenda', 'asc', '50');
      }
      widget.press(result);
    },
    itemBuilder: (BuildContext context) => [
      PopupMenuItem( value: 1, child: Text('A - Z', textAlign: TextAlign.center)),
      PopupMenuItem( value: 2, child: Text('Z - A')),
      PopupMenuItem( value: 3, child: Text('Preço maior para menor')),
      PopupMenuItem( value: 4, child: Text('Preço menor para maior')),
    ],
  );
}