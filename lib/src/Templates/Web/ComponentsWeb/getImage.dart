import 'package:flutter/material.dart';
import 'package:commerce_app/src/Templates/Components/WidgetImages/DownloadImage.dart';

Widget getImage(id, { width, height }) => Column(children: [ Container(
  padding: EdgeInsets.all(18),
  child: ConstrainedBox(
    constraints: BoxConstraints( maxWidth: width ?? 150.0, maxHeight: height ?? 150),
    child: DownloadImage(id)
  )
)]);
