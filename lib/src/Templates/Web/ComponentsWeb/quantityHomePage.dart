import 'package:flutter/material.dart';
import 'package:commerce_app/src/Controllers/FiltersController.dart';

PopupMenuButton<int> quantityHomePage(widget) {
  var q = <int>[20, 40, 60, 80, 100];

  return PopupMenuButton(
    icon: Icon(Icons.format_list_numbered),
    onSelected: (result) async {
      await FiltersController.quantity(result);
      widget.press(true);
    },
    itemBuilder: (BuildContext context) => [
      PopupMenuItem( value: q[0], child: Text('${q[0]}')),
      PopupMenuItem( value: q[1], child: Text('${q[1]}')),
      PopupMenuItem( value: q[2], child: Text('${q[2]}')),
      PopupMenuItem( value: q[3], child: Text('${q[3]}')),
      PopupMenuItem( value: q[4], child: Text('${q[4]}')),
    ],
  );
}