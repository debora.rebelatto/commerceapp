import 'package:flutter/material.dart';
import 'package:commerce_app/src/Templates/Mobile/ForgotPassword/ForgotPassword.dart';

Widget forgotPasswordButton(context) {
  return TextButton(
    onPressed: () async {
      await Navigator.push(context, MaterialPageRoute(builder: (context) => WidgetForgotPassword()));
    },
    child: Text('Esqueci a senha'),
  );
}