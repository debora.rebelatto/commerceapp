import 'package:flutter/material.dart';

import './AppBar/appBar.dart';

class UndefinedRoute extends StatelessWidget {
  final String? name;
  const UndefinedRoute({ Key? key, this.name }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(context, MediaQuery.of(context).size),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text('404',
              style: TextStyle(fontSize: 150, fontWeight: FontWeight.bold, color: Colors.grey)
            ),
            Text('Nada encontrado aqui...',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)
            ),
          ],
        )
      ),
    );
  }
}