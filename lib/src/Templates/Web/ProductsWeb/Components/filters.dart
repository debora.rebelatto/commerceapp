import 'package:flutter/material.dart';
import 'package:commerce_app/src/Templates/Web/ComponentsWeb/filter.dart';
import 'package:commerce_app/src/Templates/Web/ComponentsWeb/header.dart';
import 'package:commerce_app/src/Templates/Web/ComponentsWeb/quantityHomePage.dart';

class Filters extends StatefulWidget {
  final ValueChanged? press;
  final String? title;
  final bool? isHomePage;

  Filters({ this.press, this.title, this.isHomePage });

  @override
  _FiltersState createState() => _FiltersState();
}

class _FiltersState extends State<Filters> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(10, 5, 10, 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Header(title: widget.title ?? 'Produtos' ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
            widget.isHomePage! ? quantityHomePage(widget) : SizedBox(),
            widget.isHomePage! ? filter(widget) : SizedBox(),
            SizedBox(width: 20)
          ])
        ],
      ),
    );
  }
}
