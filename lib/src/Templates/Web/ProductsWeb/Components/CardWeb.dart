import 'package:flutter/material.dart';
import 'package:commerce_app/src/Model/ProdutoItemTabelaPreco/ProdutoItemTabelaPreco.dart';
import 'package:commerce_app/src/Templates/Components/WidgetImages/DownloadImage.dart';
import 'package:commerce_app/src/Templates/Components/currencyFormat.dart';
import 'package:commerce_app/src/Templates/Mobile/Components/ProductInfo.dart';
import 'package:commerce_app/src/Templates/Mobile/Products/Components/QuantidadeProduto.dart';

class CardWeb extends StatelessWidget {
  final ProdutoItemTabelaPreco item;

  CardWeb(this.item);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        DownloadImage(item.produto.id.toString()),
        Expanded(child: Container(
          padding: EdgeInsets.only(left: 20, right:10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              productInfo('${item.produto.nome}', isBold: true),
              productInfo('${currencyFormat.format(item.itemTabelaPreco!.valorVenda)}', isBold: true, size: 16),
              Expanded(child: QuantidadeProduto(item))
            ]
          )
        )),
      ]
    );
  }
}
