import 'package:flutter/material.dart';
import 'package:commerce_app/src/Templates/Web/ProductDetailWeb/FutureProductDetailWeb.dart';
import 'package:commerce_app/src/Templates/Web/ProductsWeb/Components/CardWeb.dart';

Widget cardHomePage(context, itemTabela) {
  return Card(
    shape: RoundedRectangleBorder( borderRadius: BorderRadius.all( Radius.circular(20) )),
    child: InkWell(
      hoverColor: Colors.transparent,
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => FutureProductDetailWeb(itemTabela.produto.id.toString())
        ));
      },
      child: Container(padding: EdgeInsets.all(10), child: CardWeb(itemTabela))
    )
  );
}