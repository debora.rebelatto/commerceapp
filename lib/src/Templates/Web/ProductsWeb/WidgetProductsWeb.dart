import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:commerce_app/src/Controllers/FiltersController.dart';
import 'package:commerce_app/src/Controllers/ProductsController.dart';
import 'package:commerce_app/src/Model/ProdutoItemTabelaPreco/ProdutoItemTabelaPreco.dart';
import 'package:commerce_app/src/Templates/Web/ComponentsWeb/responsive.dart';
import 'package:commerce_app/src/Templates/Web/ProductsWeb/Components/filters.dart';

import 'Components/cardHomePage.dart';

class WidgetProductsWeb extends StatefulWidget {
  final List<ProdutoItemTabelaPreco>? listaProdutos;
  final title;

  WidgetProductsWeb({ Key? key, this.title, this.listaProdutos }) : super(key: key);

  @override
  _WidgetProductsWebState createState() => _WidgetProductsWebState();
}

class _WidgetProductsWebState extends State<WidgetProductsWeb> {
  ScrollController? _scrollController;
  double ratio = 1;
  var currentPage = 1;
  List<ProdutoItemTabelaPreco> data = [];
  bool loading = true;

  @override
  void initState() {
    _scrollController = ScrollController();
    _getData(clean: true);
    super.initState();
  }

  void _getData({ value, bool? clean }) async {
    setState(() {
      loading = true;
      data = [];
    });

    if(clean == true) await FiltersController.cleanFilters();
    var list = await ProductsController.fillProdutoList(page: currentPage,);

    list.forEach((e) => {
      if(e.itemTabelaPreco != null) setState(() { data.add(e); })
    });

    setState(() {
      data = list;
      loading = false;
    });
  }

  int getSize(context) {
    if(ResponsiveWidget.isMediumScreen(context)) { return 3;
    } else if(ResponsiveWidget.isSmallScreen(context)){ return 2;
    } else { return 4; }
  }

  void getRatio(context) {
    var size = MediaQuery.of(context).size;
    var height = size.height;
    var width = size.width;

    if(ResponsiveWidget.isSmallScreen(context)) {
      setState(() { ratio = width / 900; });
    } else if(ResponsiveWidget.isMediumScreen(context)) {
      setState(() { ratio = width / 1200; });
    } else {
      setState(() {
        ratio = height < 600 ? height / 700 : height / 900;
      });

    }
  }

  @override
  Widget build(BuildContext context) {
    getRatio(context);
    return SingleChildScrollView(
      controller: _scrollController,
      child: Column(
        children: [
          Filters(
            isHomePage: true,
            title: widget.title,
            press: (value) async { _getData(value: value); }
          ),
          data.isEmpty && !loading
            ? Center(child: Text('Não há mais itens'),)
            : Container(
                padding: EdgeInsets.only(right: 15, left: 25),
                child: GridView.builder(
                  controller: _scrollController,
                  shrinkWrap: true,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    childAspectRatio: ratio,
                    crossAxisCount: getSize(context),
                    crossAxisSpacing: 10.0,
                    mainAxisSpacing: 10.0,
                  ),
                  itemCount: loading == true ? 20 : data.length,
                  itemBuilder: (context, index) {
                    return loading == true
                      ? Container(
                        child: Shimmer.fromColors(
                          enabled: true,
                          baseColor: Colors.grey[100]!,
                          highlightColor: Colors.grey[300]!,
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.all( Radius.circular(20) ),
                              color: Colors.red[100],
                            ),
                          ),
                        ),
                      )
                      : cardHomePage(context, data[index]);
                  }
                ),
            ),

          SizedBox(height: 20),
          pagination()
        ],
      )
    );
  }


  Widget pagination() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        IconButton(
          icon: Icon(Icons.arrow_left),
          onPressed: currentPage <= 1 ? null : () {
            setState(() { currentPage -= 1; });
            _getData();
          }
        ),
        Text('$currentPage'),
        IconButton(
          icon: Icon(Icons.arrow_right),
          onPressed: data.isEmpty ? null : () {
            setState(() { currentPage += 1; });
            _getData();
          }
        )
      ],
    );
  }
}
