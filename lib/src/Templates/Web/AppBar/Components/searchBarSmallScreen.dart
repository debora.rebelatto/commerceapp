import 'package:flutter/material.dart';
import 'package:commerce_app/src/Templates/Web/SearchWeb/FutureSearchWeb.dart';

Widget searchBarSmallScreen(context) {
  var searchQuery = TextEditingController(text: '');

  var inputBorder = OutlineInputBorder(
    borderSide: BorderSide(color: Colors.transparent),
    borderRadius: BorderRadius.circular(15.0)
  );

  return Container(
    height: 50,
    padding: EdgeInsets.all(5),
    child: TextFormField(
      cursorColor: Colors.black,
      controller: searchQuery,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.only(top: 0),
        hintText: 'Pesquisa',
        prefixIcon: IconButton(
          icon: Icon(Icons.search),
          onPressed: () async {
            await Navigator.push(context, MaterialPageRoute(
              builder: (context) => FutureSearchWeb(searchQuery.text)
            ));
          },
        ),
        filled: true,
        fillColor: Colors.white,
        border: inputBorder,
        focusedBorder: inputBorder,
        enabledBorder: inputBorder,
      ),
    ),
  );
}