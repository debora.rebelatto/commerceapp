import 'package:flutter/material.dart';
import 'package:commerce_app/src/Templates/Web/SearchWeb/FutureSearchWeb.dart';

// ignore: must_be_immutable
class SearchBarLargeScreen extends StatelessWidget {
  var searchQuery = TextEditingController(text: '');
  var focusNode = FocusNode();

  final inputBorder = OutlineInputBorder(
    borderSide: BorderSide(color: Colors.transparent),
    borderRadius: BorderRadius.circular(15.0)
  );

  @override
  Widget build(BuildContext context) {
    return Expanded(child: Container(
      padding: EdgeInsets.only(left: 50, right: 50),
      child: Theme(
        data: Theme.of(context).copyWith(primaryColor: Colors.grey),
        child: TextFormField(
          cursorColor: Colors.black,
          controller: searchQuery,
          maxLines: 1,
          autovalidateMode: AutovalidateMode.always,
          decoration: InputDecoration(
            isDense: true,
            contentPadding: EdgeInsets.fromLTRB(20, 0, 0, 0),
            filled: true,
            fillColor: Colors.grey[200],
            hintText: 'Pesquisa',
            suffixIcon: IconButton(
              icon: Icon(Icons.search),
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(
                  builder: (context) => FutureSearchWeb(searchQuery.text)
                ));
              },
            ),
            focusedBorder: inputBorder,
            enabledBorder: inputBorder,
            border: inputBorder,
          )
        ),
      )
    ));
  }
}