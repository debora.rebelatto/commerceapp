import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:commerce_app/src/Controllers/CarrinhoController.dart';
import 'package:commerce_app/src/Controllers/SharedPrefController.dart';
import 'package:commerce_app/src/Templates/Mobile/ConfiguracaoHost/HostPorta.dart';
import 'package:commerce_app/src/Templates/Web/CarrinhoWeb/FutureCarrinhoWeb.dart';
import 'package:commerce_app/src/Templates/Web/HomePageWeb/WidgetWebHomePage.dart';
import 'package:commerce_app/src/Templates/Web/InitialScreen/InitialScreenWidget.dart';
import 'package:commerce_app/src/Templates/Web/ProfileWeb/WidgetProfile.dart';

import 'Components/searchBarLargeScreen.dart';

class TopBarContents extends StatefulWidget {
  @override
  _TopBarContentsState createState() => _TopBarContentsState();
}

class _TopBarContentsState extends State<TopBarContents> {
  String? nome;
  var howmuch = 0;

  Future<dynamic> _getData() async {
    var itensCarrinho = await CarrinhoController.getItensCarrinho();

    setState(() {
      howmuch = itensCarrinho.itemcarrinho!.length;
    });
  }

  @override
  void initState() {
    super.initState();
    setInfo();
    _getData();
  }

  dynamic setInfo() async {
    var pref = await SharedPreferences.getInstance();
    setState(() {
      nome = pref.getString('nome');
    });
  }

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    return PreferredSize(
      preferredSize: Size(screenSize.width, 1000),
      child: Container(
        color: Theme.of(context).appBarTheme.backgroundColor,
        child: Padding(
          padding: EdgeInsets.all(20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(width: 10),
              InkWell(
                splashColor: Colors.transparent,
                hoverColor: Colors.transparent,
                onTap: () async {
                  await Navigator.push(context,
                    MaterialPageRoute(builder: (context) => WidgetWebHomePage())
                  );
                },
                child: Text('Commerce App',
                  style: TextStyle(
                    fontSize: 25,
                    color: Colors.white,
                    fontFamily: 'DroidSans',
                    fontWeight: FontWeight.bold
                  )
                ),
              ),
              SearchBarLargeScreen(),
              Row(
                children: [
                  Container(
                    width: 200,
                    padding: EdgeInsets.only(right: 20),
                    child: TextButton(
                      onPressed: () async {
                        await Navigator.push(context,
                          MaterialPageRoute(builder: (context) => WidgetWebProfile())
                        );
                      },
                      child: Text(
                        'Olá, $nome',
                        textAlign: TextAlign.left,
                        overflow: TextOverflow.ellipsis,
                        softWrap: false,
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                  Ink(
                      child: InkWell(
                    splashColor: Colors.transparent,
                    hoverColor: Colors.transparent,
                    onTap: () async {
                      await Navigator.of(context).push(
                        MaterialPageRoute(builder: (context) => FutureCarrinhoWeb())
                      );
                    },
                    child: Badge(
                      badgeContent: Text(
                        '$howmuch',
                        style: TextStyle(color: Colors.white),
                      ),
                      child: Icon(Icons.shopping_cart_outlined,
                          color: Colors.white),
                    ),
                  )),
                  SizedBox(width: 20),
                  Ink(
                      child: InkWell(
                      splashColor: Colors.transparent,
                      hoverColor: Colors.transparent,
                      onTap: () async {
                        await Navigator.of(context).push(
                          MaterialPageRoute(builder: (context) => HostPorta())
                        );
                      },
                      child: Icon(Icons.settings, color: Colors.white),
                    )
                  ),
                  SizedBox(width: 15),
                  Ink(
                      child: InkWell(
                    splashColor: Colors.transparent,
                    hoverColor: Colors.transparent,
                    onTap: () async {
                      Navigator.pop(context);
                      await SharedPrefController.excluiArquivoUsuario();
                      await Navigator.of(context).push(
                        MaterialPageRoute(builder: (context) => InitialScreenWidget())
                      );
                    },
                    child:
                        Icon(Icons.exit_to_app_outlined, color: Colors.white),
                  )),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
