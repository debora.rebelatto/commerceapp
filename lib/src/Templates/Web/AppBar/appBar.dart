import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:commerce_app/src/Templates/Web/AppBar/topbar.dart';
import 'package:commerce_app/src/Templates/Web/ComponentsWeb/responsive.dart';
import 'Components/searchBarSmallScreen.dart';

PreferredSizeWidget appBar(BuildContext context, screenSize) {
  return ResponsiveWidget.isSmallScreen(context)
  ? AppBar(
    backgroundColor: Theme.of(context).appBarTheme.color,
    elevation: 0,
    centerTitle: true,
    title: searchBarSmallScreen(context)
  )
  : PreferredSize(
    preferredSize: Size(screenSize.width, 1000),
    child: TopBarContents(),
  );
}