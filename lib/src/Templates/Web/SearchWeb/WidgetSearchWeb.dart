import 'package:flutter/material.dart';
import 'package:commerce_app/src/Model/ProdutoItemTabelaPreco/ProdutoItemTabelaPreco.dart';
import 'package:commerce_app/src/Templates/Web/ComponentsWeb/filter.dart';
import 'package:commerce_app/src/Templates/Web/ProductsWeb/WidgetProductsWeb.dart';
import 'package:commerce_app/src/core/AppStyles.dart';

class WidgetSearchWeb extends StatefulWidget {
  final String searchQuery;
  final List<ProdutoItemTabelaPreco> listaProdutos;
  final ValueChanged? press;
  WidgetSearchWeb(this.searchQuery, this.listaProdutos, { this.press });

  @override
  _WidgetSearchWeb createState() => _WidgetSearchWeb();
}

class _WidgetSearchWeb extends State<WidgetSearchWeb>{
  ScrollController? _scrollController;

  @override
  void initState() {
    _scrollController = ScrollController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        controller: _scrollController,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: EdgeInsets.only(top: 20, left: 30),
                      child: Text('Pesquisando por \'${widget.searchQuery} \'',
                        style: AppStyles.boldWebText(24)
                      )
                    ),
                    Container(
                      padding: EdgeInsets.only(top: 10, left: 30, bottom: 10),
                      child: Text('Encontrados ${widget.listaProdutos.length} resultados',
                        style: AppStyles.boldWebText(15)
                      )
                    ),
                  ],
                ),

                Container(
                  padding: EdgeInsets.only(right: 30),
                  child: filter(widget),
                ),
              ],
            ),

            WidgetProductsWeb(listaProdutos: widget.listaProdutos)
          ]
        )
      )
    );
  }
}