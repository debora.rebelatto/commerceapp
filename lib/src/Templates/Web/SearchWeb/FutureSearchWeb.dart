import 'package:flutter/material.dart';
import 'package:commerce_app/src/Controllers/ProductsController.dart';
import 'package:commerce_app/src/Model/ProdutoItemTabelaPreco/ProdutoItemTabelaPreco.dart';
import 'package:commerce_app/src/Templates/Components/emptySearchList.dart';
import 'package:commerce_app/src/Templates/Mobile/Drawer/Drawer.dart';
import 'package:commerce_app/src/Templates/Web/AppBar/appBar.dart';

import 'WidgetSearchWeb.dart';

class FutureSearchWeb extends StatefulWidget {
  final String searchQuery;

  FutureSearchWeb( this.searchQuery );

  @override
  _FutureSearchWebState createState() => _FutureSearchWebState();
}

class _FutureSearchWebState extends State<FutureSearchWeb> {
  Future<dynamic>? _future;

  Future<List<ProdutoItemTabelaPreco>> _search() async =>
    await ProductsController.search(widget.searchQuery);

  @override
  void initState() {
    _future = _search();
    super.initState();
  }

  void updateData(value) {
    setState(() {
      _future = _search();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(context, MediaQuery.of(context).size),
      drawer: LeftDrawer(),
      body: FutureBuilder(
        future: _future,
        builder: (context, snapshot) {
          var list = snapshot.data as List<ProdutoItemTabelaPreco>;
          switch(snapshot.connectionState) {
            case ConnectionState.waiting:
              return Center(child: CircularProgressIndicator(),);
            case ConnectionState.done: default:
              return list.isEmpty
                ? emptySearchList(widget.searchQuery)
                : WidgetSearchWeb(
                  widget.searchQuery,
                  list,
                  press: (value) {
                    updateData(value);
                  },
                );
          }
        }
      )
    );
  }
}