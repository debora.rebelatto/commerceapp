import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:commerce_app/src/Controllers/CarrinhoController.dart';
import 'package:commerce_app/src/Model/ItemCarrinho/ItemCarrinho.dart';
import 'package:commerce_app/src/Model/ItensCarrinho/ItensCarrinho.dart';
import 'package:commerce_app/src/Templates/Components/emptyCart.dart';
import 'package:commerce_app/src/Templates/Mobile/Drawer/Drawer.dart';
import 'package:commerce_app/src/Templates/Web/AppBar/appBar.dart';
import 'WidgetCarrinhoWeb.dart';

class FutureCarrinhoWeb extends StatefulWidget {
  @override
  _FutureCarrinhoState createState() => _FutureCarrinhoState();
}

class _FutureCarrinhoState extends State<FutureCarrinhoWeb> {
  Future<dynamic>? _future;
  Future<dynamic> _getData() async => await CarrinhoController.getItensCarrinho();

  @override
  void initState() {
    super.initState();
    _future = _getData();
  }

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Colors.grey[100],
      drawer: LeftDrawer(),
      appBar: appBar(context, screenSize),
      body: FutureBuilder(
        future: _future,
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              return Center(child: CircularProgressIndicator());
            case ConnectionState.done:
              if(snapshot.data != null) {
                var itensCarrinho = snapshot.data as ItensCarrinho;
                var itemCarrinho = itensCarrinho.itemcarrinho as List<ItemCarrinho>;

                return itemCarrinho.isEmpty
                  ? EmptyCart()
                  : WidgetWebCarrinho(
                    itensCarrinho: itensCarrinho,
                    rebuild: (bool value) { if (value) setState(() { _future = _getData(); }); }
                  );
              } else {
                return EmptyCart();
              }

            default: return Container();
          }
        },
      )
    );
  }
}