import 'package:flutter/material.dart';
import 'package:commerce_app/src/Model/ItensCarrinho/ItensCarrinho.dart';
import 'package:commerce_app/src/Templates/Web/ComponentsWeb/header.dart';
import 'package:commerce_app/src/Templates/Web/ComponentsWeb/responsive.dart';
import 'Screens/LargeScreenCart.dart';
import 'Screens/SmallScreenCart.dart';

class WidgetWebCarrinho extends StatefulWidget {
  final ItensCarrinho? itensCarrinho;
  final Function(bool)? rebuild;

  WidgetWebCarrinho({ Key? key, this.itensCarrinho, this.rebuild }) : super(key: key);

  @override
  _WidgetCarrinhoState createState() => _WidgetCarrinhoState();
}

class _WidgetCarrinhoState extends State<WidgetWebCarrinho> {
  ScrollController? _scrollController;

  @override
  void initState() {
    _scrollController = ScrollController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      controller: _scrollController,
      child: Container(
        padding: EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Header(title: 'Carrinho'),
            ResponsiveWidget.isSmallScreen(context)
              ? SmallScreenCart(widget.itensCarrinho!, widget)
              : LargeScreenCart(widget.itensCarrinho!, widget),
          ],
        )
      )
    );
  }
}
