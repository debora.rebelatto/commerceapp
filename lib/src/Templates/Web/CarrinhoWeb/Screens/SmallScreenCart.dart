import 'package:flutter/material.dart';
import 'package:commerce_app/src/Model/ItensCarrinho/ItensCarrinho.dart';
import 'package:commerce_app/src/Templates/Web/CarrinhoWeb/Components/continueButton.dart';
import 'package:commerce_app/src/Templates/Web/CarrinhoWeb/Components/listCartBuilder.dart';
import 'package:commerce_app/src/Templates/Web/CarrinhoWeb/Components/totalCard.dart';
import '../WidgetCarrinhoWeb.dart';

class SmallScreenCart extends StatelessWidget {
  final ItensCarrinho itensCarrinho;
  final WidgetWebCarrinho carrinho;

  SmallScreenCart(this.itensCarrinho, this.carrinho);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        listCartBuilder(itensCarrinho.itemcarrinho!, carrinho),
        SizedBox(height: 15),
        Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            continueButton(context, itensCarrinho.carrinho),
            TotalCard(itensCarrinho.carrinho.total) ,
          ],
        )
      ],
    );
  }
}