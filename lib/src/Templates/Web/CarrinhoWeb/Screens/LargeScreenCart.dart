import 'package:flutter/material.dart';
import 'package:commerce_app/src/Model/ItensCarrinho/ItensCarrinho.dart';
import 'package:commerce_app/src/Templates/Web/CarrinhoWeb/Components/continueButton.dart';
import 'package:commerce_app/src/Templates/Web/CarrinhoWeb/Components/listCartBuilder.dart';
import 'package:commerce_app/src/Templates/Web/CarrinhoWeb/Components/totalCard.dart';
import '../WidgetCarrinhoWeb.dart';

class LargeScreenCart extends StatelessWidget {
  final ItensCarrinho item;
  final WidgetWebCarrinho carrinho;

  LargeScreenCart(this.item, this.carrinho);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Expanded(
          child: Container(
            width: MediaQuery.of(context).size.width / 4,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                listCartBuilder(item.itemcarrinho!, carrinho),
              ]
            )
          )
        ),
        Container(
          width: MediaQuery.of(context).size.width / 3,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              TotalCard(item.carrinho.total),
              continueButton(context, item.carrinho)
            ]
          )
        ),
      ]
    );
  }
}