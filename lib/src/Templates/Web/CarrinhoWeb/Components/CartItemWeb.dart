import 'package:flutter/material.dart';
import 'package:commerce_app/src/Model/ItemCarrinho/ItemCarrinho.dart';
import 'package:commerce_app/src/Templates/Components/WidgetImages/DownloadImage.dart';
import 'package:commerce_app/src/Templates/Components/currencyFormat.dart';
import 'package:commerce_app/src/Templates/Mobile/Components/ProductInfo.dart';
import 'package:commerce_app/src/Templates/Web/CarrinhoWeb/Components/QuantidadeCarrinhoWeb.dart';
import 'package:commerce_app/src/Templates/Web/ProductDetailWeb/FutureProductDetailWeb.dart';
import 'package:commerce_app/src/Services/ItemCarrinhoServices.dart';
import '../WidgetCarrinhoWeb.dart';

class CartItemWeb extends StatefulWidget {
  final ItemCarrinho itemCarrinho;
  final WidgetWebCarrinho carrinho;

  CartItemWeb(this.itemCarrinho, this.carrinho);

  @override
  _CartItemWebState createState() => _CartItemWebState();
}

class _CartItemWebState extends State<CartItemWeb>{
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      child: InkWell(
        hoverColor: Colors.white,
        onTap: () async {
          await Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => FutureProductDetailWeb(widget.itemCarrinho.produtoId.toString())
          ));
        },
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget> [
            DownloadImage(widget.itemCarrinho.produto!.id.toString()),
            info(context),
            iconDelete()
          ],
        )
      )
    );
  }

  Widget info(context) {
    return Expanded(child: Container(
      color: Colors.white,
      padding: EdgeInsets.all(10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          productInfo(widget.itemCarrinho.produto!.nome!, isBold: true),
          productInfo('${currencyFormat.format(widget.itemCarrinho.total)}', isBold: true),
          productInfo('Quantidade: ${widget.itemCarrinho.quantidade}', isBold: false, size: 16),
          SizedBox(height: 10),
          QuantidadeCarrinhoWeb(widget.itemCarrinho, widget.carrinho),
        ],
      ),)
    );
  }

  Widget iconDelete() {
    return Builder(builder: (context) {
      return Align(
        alignment: Alignment.topRight,
        child: IconButton(
          icon: Icon(Icons.delete_forever, color: Colors.black54),
          onPressed: () {
            removeProductDialog(context);
          }
        ),
      );
    },);
  }

  Future removeProductDialog(BuildContext context) => showDialog(
    context: context,
    builder: (context) {
      return AlertDialog(
        title: Text('Remover produto'),
        content: Text('Tem certeza que deseja Remover o Produto?'),
        actions: [ confirmDelete(widget.itemCarrinho.id), cancel() ],
      );
    },
  );

  Widget confirmDelete(id) {
      return Builder(builder: (context) {
        return TextButton(
          style: TextButton.styleFrom(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
            backgroundColor: Colors.grey,
          ),
          onPressed: () async {
            if(await ItemCarrinhoServices.deleteItemCarrinhoById('$id') == 200){
              Navigator.of(context).pop();
              widget.carrinho.rebuild!(true);
            }
          },
          child: Text( 'Sim', style: TextStyle( color: Colors.white )),
        );
      });
    }
}

dynamic cancel() => Builder(builder: (context) => TextButton(
  style: TextButton.styleFrom(
    shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(15.0) ),
    backgroundColor: Colors.red[700],
  ),
  onPressed: () async { Navigator.of(context).pop(); },
  child: Text('Cancelar', style: TextStyle( color: Colors.white) ),
));

