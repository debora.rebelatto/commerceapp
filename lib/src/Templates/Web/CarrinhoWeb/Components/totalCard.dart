import 'package:flutter/material.dart';
import 'package:commerce_app/src/Templates/Components/currencyFormat.dart';

class TotalCard extends StatelessWidget {
  final total;

  TotalCard(this.total);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.only(bottom: 10),
          child: Card(
            margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
            child: Container(
              padding: EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Total:", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20)),
                  SizedBox(height: 5),
                  Text('${currencyFormat.format(total)}', style: TextStyle(fontSize: 20)),
                ]
              )
            )
          ),
        )
      ]
    );
  }
}
