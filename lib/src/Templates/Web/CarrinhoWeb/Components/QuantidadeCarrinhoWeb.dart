import 'package:flutter/material.dart';
import 'package:commerce_app/src/Controllers/CarrinhoController.dart';
import 'package:commerce_app/src/Model/ItemCarrinho/ItemCarrinho.dart';
import 'package:commerce_app/src/Services/ItemCarrinhoServices.dart';
import 'package:commerce_app/src/Templates/Web/CarrinhoWeb/Components/snack.dart';
import 'package:commerce_app/src/core/AppStyles.dart';
import '../WidgetCarrinhoWeb.dart';
import 'CartItemWeb.dart';

class QuantidadeCarrinhoWeb extends StatefulWidget {
  final ItemCarrinho item;
  final WidgetWebCarrinho carrinho;

  QuantidadeCarrinhoWeb (this.item, this.carrinho, { Key? key }) : super(key: key);

  @override
  _QuantidadeCarrinhoWebState createState() => _QuantidadeCarrinhoWebState();
}

class _QuantidadeCarrinhoWebState extends State<QuantidadeCarrinhoWeb> {
  TextEditingController quantidadeContoller = TextEditingController(text: '');
  int? mult;

  @override
  void initState() {
    super.initState();
    setState(() {
      mult = (widget.item.produto!.quantidadeMultipla == 0
        ? 1 : widget.item.produto!.quantidadeMultipla)!;
      quantidadeContoller.text = widget.item.quantidade!.round().toString();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Row( children: [
      Padding(
        padding: EdgeInsets.only(top: 10),
        child: Row( children: [
          Container(
            decoration: BoxDecoration(
              border: Border.all( color: Colors.black26, width: 1.0 ),
              borderRadius: BorderRadius.all( Radius.circular(5.0) ),
            ),
            child: Row(children: [
              IconButton( icon: Icon(Icons.remove), color: AppStyles.slightlyDarkerRed, onPressed: decrease ),
              Text('${quantidadeContoller.text}', style: TextStyle( fontSize: 20 )),
              IconButton( icon: Icon( Icons.add ), color: AppStyles.slightlyDarkerRed, onPressed: increase ),
            ]),
          ),
        ]),
      ),
    ]);
  }

  dynamic decrease() async {
    if(int.parse(quantidadeContoller.text) == mult) {
      await removeProductDialog(widget.item.id.toString(), context);
    } else {
      var quantMult = mult;
      var atual = int.parse(quantidadeContoller.text);
      if (atual - quantMult! <= quantMult) {
        setState(() { quantidadeContoller.text = quantMult.toString(); });
      } else {
        atual -= quantMult;
        setState(() { quantidadeContoller.text = atual.toString(); });
      }
      await CarrinhoController.decreaseDebounce(
        widget.item, int.parse(quantidadeContoller.text), context, widget.carrinho
      );
    }
  }

  dynamic increase() async {
    var atual = int.parse(quantidadeContoller.text) + mult!;
    setState(() { quantidadeContoller.text = atual.toString(); });

    var quantidade = int.parse(quantidadeContoller.text);

    if(quantidade % mult! == 0) {
      await CarrinhoController.increaseDebounce(widget.item, quantidade, context, widget.carrinho);
    } else {
      snackWeb(
        widget.carrinho,
        context,
        widget.item.produto!.nome!,
        erro: 'Quantidade múltipla deste produto é $mult .\nVerificar quantidade.'
      );
    }
  }

  Future removeProductDialog(String id, BuildContext context) => showDialog(
    context: context,
    builder: (context) {
      return AlertDialog(
        title: Text('Remover produto'),
        content: Text('Tem certeza que deseja Remover o Produto?'),
        actions: [
          TextButton(
            style: TextButton.styleFrom(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
              backgroundColor: Colors.grey,
            ),
            onPressed: () async {
              if(await ItemCarrinhoServices.deleteItemCarrinhoById(id) == 200) Navigator.of(context).pop();
              widget.carrinho.rebuild!(true);
            },
            child: Text('Sim', style: TextStyle( color: Colors.white)),
          ),
          cancel()
        ],
      );
    },
  );
}
