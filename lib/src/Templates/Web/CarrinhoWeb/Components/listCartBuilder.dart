import 'package:flutter/material.dart';
import 'package:commerce_app/src/Model/ItemCarrinho/ItemCarrinho.dart';

import '../WidgetCarrinhoWeb.dart';
import 'CartItemWeb.dart';

Widget listCartBuilder(List<ItemCarrinho> listItemCarrinho, WidgetWebCarrinho carrinho) {
  return listItemCarrinho.isEmpty
    ? Center(child: Icon(Icons.warning))
    : Card(
      margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
      child: ListView.builder(
        shrinkWrap: true,
        itemCount: listItemCarrinho.length,
        itemBuilder: (context, index) {
          return Column(children: [
            CartItemWeb(listItemCarrinho[index], carrinho),
            Divider( color: Colors.black.withOpacity(1), thickness: 0.5, indent: 10, endIndent: 10,),
          ],);
        }
      )
    );
}