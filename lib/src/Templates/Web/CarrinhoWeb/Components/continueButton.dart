import 'package:flutter/material.dart';
import 'package:commerce_app/src/Templates/Web/FinalizeCartWeb/WidgetFinalizeCartWeb.dart';
import 'package:commerce_app/src/core/AppStyles.dart';


Widget continueButton(context, cart) {
  return Container(
    height: 40,
    width: MediaQuery.of(context).size.width,
    margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
    child: TextButton(
      style: TextButton.styleFrom(
        backgroundColor: AppStyles.darkBlue,
      ),
      onPressed: () async {
        await Navigator.push(context, MaterialPageRoute(builder: (context) => WidgetFinalizeCart(cart: cart)));
      },
      child: Text('Continuar', style: TextStyle(color: Colors.white),)
    )
  );
}