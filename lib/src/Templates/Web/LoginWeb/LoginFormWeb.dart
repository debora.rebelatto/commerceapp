import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:commerce_app/src/Controllers/LoginController.dart';
import 'package:commerce_app/src/Controllers/SharedPrefController.dart';
import 'package:commerce_app/src/Templates/Components/version.dart';
import 'package:commerce_app/src/core/AppStyles.dart';

import 'Components/Fields.dart';
import '../ComponentsWeb/forgotPasswordButton.dart';
import '../../Components/ErrorLogin.dart';

class LoginForm extends StatefulWidget {

  LoginForm({ Key? key }) : super(key: key);

  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final _email = TextEditingController(text: '');
  final _password = TextEditingController(text: '');
  final _formKey = GlobalKey<FormState>();

  var waiting = false;
  var error = '';
  var remember = false;

  @override
  void initState() {
    super.initState();
    getEmail();
  }

  void getEmail() async {
    var email = await SharedPrefController.checkIfEmailSaved();
    var newRemember = await SharedPrefController.checkRemember();

    setState(() {
      _email.text = email ?? _email.text;
      remember = newRemember ?? remember;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints(maxWidth: 500),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text('Entrar', style: GoogleFonts.inriaSans(
            textStyle: TextStyle( fontSize: 30, fontWeight: FontWeight.bold)
          )),
          waiting ? SizedBox(height: 0,) : errorLogin(error),
          SizedBox(height: 10), Form(key: _formKey, child: LoginFields(_email, _password)),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [ rememberUser(), forgotPasswordButton(context) ],
          ),
          login(),
          Align(alignment: Alignment.bottomCenter, child: version())
        ]
      )
    );
  }

  void setRemember() { setState(() { remember = !remember; }); }

  Widget rememberUser() {
    return Expanded( child: Material(
      color: Colors.transparent,
      child: Ink(child: InkWell(
        onTap: () { setRemember(); },
        child: Row(children: [
          Checkbox(
            value: remember,
            onChanged: (value) { setRemember(); }
          ),
          Text('Lembrar-se de mim', style: TextStyle())
        ])
      )),
    ));
  }

  Widget login() => Container(
    padding: EdgeInsets.only(top: 10, bottom: 10),
    width: MediaQuery.of(context).size.width,
    child: TextButton(
      style: TextButton.styleFrom(
        backgroundColor: AppStyles.darkBlue,
        shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(10) ),
      ),
      onPressed: waiting == true ? null : pressedLogin,
      child: waiting != false
        ? Center(child: SizedBox(height: 20.0, width: 20.0, child: CircularProgressIndicator()))
        : Text( 'Entrar', style: TextStyle( color: Colors.white ))
    ),
  );

  dynamic pressedLogin() async {
    if (_formKey.currentState!.validate()) {
      setState(() { waiting = !waiting; });
      var err = await LoginController.login(context, _email.text, _password.text, remember);
      setState(() {
        error = err ?? '';
        waiting = !waiting;
      });
    }
  }
}
