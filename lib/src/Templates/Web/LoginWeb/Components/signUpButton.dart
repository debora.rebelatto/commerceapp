import 'package:flutter/material.dart';
import 'package:commerce_app/src/Templates/Web/SignupWeb/WidgetSignupWeb.dart';

Widget signUpButton(context) {
  return TextButton(
    onPressed: () async {
      await Navigator.push(context, MaterialPageRoute(builder: (context) => WidgetSignupWeb()));
    },
    child: Text('Cadastre-se'),
  );
}