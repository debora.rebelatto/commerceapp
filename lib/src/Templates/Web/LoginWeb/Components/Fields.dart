import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:commerce_app/src/Templates/Components/validatorFields.dart';

class LoginFields extends StatefulWidget {
  final email;
  final password;

  LoginFields(this.email, this.password);

  @override
  _LoginFieldsState createState() => _LoginFieldsState();
}

class _LoginFieldsState extends State<LoginFields> {
  bool _obscureText = true;

  void _toggle() { setState(() { _obscureText = !_obscureText; }); }

  final inputBorder = OutlineInputBorder(
    borderRadius: BorderRadius.circular(15.0)
  );

  @override
  Widget build(BuildContext context) => Column(children: [
    field(widget.email, 'E-mail'),
    SizedBox(height: 10),
    field(widget.password, 'Senha', suffix: true)
  ]);

  Container field(var controller, String label, { bool? suffix }) {
    var suffixIcon = suffix == null ? null : IconButton(
      icon: Icon( _obscureText ? Icons.visibility : Icons.visibility_off ),
      onPressed: _toggle,
    );

    var inputDecoration = InputDecoration(
      prefixIcon: Icon( label == 'Senha' ? Icons.lock_outline : Icons.mail_outline_rounded),
      suffixIcon: suffixIcon,
      labelText: label,
      focusedBorder: inputBorder,
      enabledBorder: inputBorder,
      border: inputBorder
    );

    return Container(
      padding: EdgeInsets.only(top: 10),
      child: Theme(
        data: Theme.of(context).copyWith(primaryColor: Colors.grey),
        child: TextFormField(
        obscureText: label == 'Senha' ? _obscureText : false,
        keyboardType: TextInputType.emailAddress,
        controller: controller,
        decoration: inputDecoration,
        textInputAction: TextInputAction.next,
        onEditingComplete: () => FocusScope.of(context).nextFocus(),
        validator: (value) => validator(value, label)
      )),
    );
  }
}

String? validator(value, label) {
  switch(label) {
    case 'E-mail': return FieldValidator.validateEmail(value);
    case 'Senha': return FieldValidator.validateIfEmpty(value);
    default: return FieldValidator.validateIfEmpty(value);
  }
}
