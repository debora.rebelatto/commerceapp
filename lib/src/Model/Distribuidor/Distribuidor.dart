import 'package:json_annotation/json_annotation.dart';
import 'package:commerce_app/src/Model/PessoaJuridica/PessoaJuridica.dart';

part 'Distribuidor.g.dart';

@JsonSerializable()

class Distribuidor {
  int? pessoaJuridicaId;
  PessoaJuridica? pessoaJuridica;
  int? codigo;
  int id;

  Distribuidor({
    this.pessoaJuridicaId,
    this.pessoaJuridica,
    this.codigo,
    required this.id
  });

  factory Distribuidor.fromJson(Map<String, dynamic> json) => _$DistribuidorFromJson(json);
  Map<String, dynamic> toJson() => _$DistribuidorToJson(this);
}