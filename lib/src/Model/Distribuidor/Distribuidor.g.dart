// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Distribuidor.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Distribuidor _$DistribuidorFromJson(Map<String, dynamic> json) {
  return Distribuidor(
    pessoaJuridicaId: json['pessoaJuridicaId'] as int?,
    pessoaJuridica: json['pessoaJuridica'] == null
        ? null
        : PessoaJuridica.fromJson(
            json['pessoaJuridica'] as Map<String, dynamic>),
    codigo: json['codigo'] as int?,
    id: json['id'] as int,
  );
}

Map<String, dynamic> _$DistribuidorToJson(Distribuidor instance) =>
    <String, dynamic>{
      'pessoaJuridicaId': instance.pessoaJuridicaId,
      'pessoaJuridica': instance.pessoaJuridica,
      'codigo': instance.codigo,
      'id': instance.id,
    };
