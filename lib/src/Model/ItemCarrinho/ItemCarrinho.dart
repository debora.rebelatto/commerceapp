import 'package:json_annotation/json_annotation.dart';

import '../Produto/Produto.dart';

part 'ItemCarrinho.g.dart';

@JsonSerializable()

class ItemCarrinho {
  int? carrinhoId;
  int? produtoId;
  Produto? produto;
  double? quantidade;
  double? unitario;
  double? total;
  int id;

  ItemCarrinho({
    this.carrinhoId,
    this.produtoId,
    this.produto,
    this.quantidade,
    this.unitario,
    this.total,
    required this.id
  });

  factory ItemCarrinho.fromJson(Map<String, dynamic> json) => _$ItemCarrinhoFromJson(json);

  Map<String, dynamic> toJson() => _$ItemCarrinhoToJson(this);
}
