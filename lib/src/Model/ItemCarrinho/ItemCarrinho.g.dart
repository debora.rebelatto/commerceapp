// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ItemCarrinho.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ItemCarrinho _$ItemCarrinhoFromJson(Map<String, dynamic> json) {
  return ItemCarrinho(
    carrinhoId: json['carrinhoId'] as int?,
    produtoId: json['produtoId'] as int?,
    produto: json['produto'] == null
        ? null
        : Produto.fromJson(json['produto'] as Map<String, dynamic>),
    quantidade: (json['quantidade'] as num?)?.toDouble(),
    unitario: (json['unitario'] as num?)?.toDouble(),
    total: (json['total'] as num?)?.toDouble(),
    id: json['id'] as int,
  );
}

Map<String, dynamic> _$ItemCarrinhoToJson(ItemCarrinho instance) =>
    <String, dynamic>{
      'carrinhoId': instance.carrinhoId,
      'produtoId': instance.produtoId,
      'produto': instance.produto,
      'quantidade': instance.quantidade,
      'unitario': instance.unitario,
      'total': instance.total,
      'id': instance.id,
    };
