import 'package:json_annotation/json_annotation.dart';
import 'package:commerce_app/src/Model/CondicaoPagamento/CondicaoPagamento.dart';
import 'package:commerce_app/src/Model/Distribuidor/Distribuidor.dart';
import 'package:commerce_app/src/Model/FormaPagamento/FormaPagamento.dart';
import 'package:commerce_app/src/Model/TabelaPreco/TabelaPreco.dart';

import '../Cliente/Cliente.dart';

part 'Carrinho.g.dart';

@JsonSerializable()

class Carrinho {
  int? distribuidorId;
  Distribuidor? distribuidor;

  int? clienteId;
  Cliente? cliente;

  int? formaPagamentoId;
  FormaPagamento? formaPagamento;

  int? condicaoPagamentoId;
  CondicaoPagamento? condicaoPagamento;

  int? tabelaPrecoId;
  TabelaPreco? tabelaPreco;

  String? dataEntrega;
  double? total;
  int? usuario;
  bool? finalizado;
  bool? enviado;
  String? idPedido;
  int id;

  Carrinho({
    this.distribuidorId,
    this.distribuidor,

    this.clienteId,
    this.cliente,

    this.formaPagamentoId,
    this.formaPagamento,

    this.condicaoPagamentoId,
    this.condicaoPagamento,

    this.tabelaPrecoId,
    this.tabelaPreco,

    this.dataEntrega,
    this.total,
    this.usuario,
    this.finalizado,
    this.enviado,
    this.idPedido,
    required this.id
  });

  
  factory Carrinho.fromJson(Map<String, dynamic> json) => _$CarrinhoFromJson(json);
  Map<String, dynamic> toJson() => _$CarrinhoToJson(this);
}
