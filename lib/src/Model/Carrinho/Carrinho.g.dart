// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Carrinho.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Carrinho _$CarrinhoFromJson(Map<String, dynamic> json) {
  return Carrinho(
    distribuidorId: json['distribuidorId'] as int?,
    distribuidor: json['distribuidor'] == null
        ? null
        : Distribuidor.fromJson(json['distribuidor'] as Map<String, dynamic>),
    clienteId: json['clienteId'] as int?,
    cliente: json['cliente'] == null
        ? null
        : Cliente.fromJson(json['cliente'] as Map<String, dynamic>),
    formaPagamentoId: json['formaPagamentoId'] as int?,
    formaPagamento: json['formaPagamento'] == null
        ? null
        : FormaPagamento.fromJson(
            json['formaPagamento'] as Map<String, dynamic>),
    condicaoPagamentoId: json['condicaoPagamentoId'] as int?,
    condicaoPagamento: json['condicaoPagamento'] == null
        ? null
        : CondicaoPagamento.fromJson(
            json['condicaoPagamento'] as Map<String, dynamic>),
    tabelaPrecoId: json['tabelaPrecoId'] as int?,
    tabelaPreco: json['tabelaPreco'] == null
        ? null
        : TabelaPreco.fromJson(json['tabelaPreco'] as Map<String, dynamic>),
    dataEntrega: json['dataEntrega'] as String?,
    total: (json['total'] as num?)?.toDouble(),
    usuario: json['usuario'] as int?,
    finalizado: json['finalizado'] as bool?,
    enviado: json['enviado'] as bool?,
    idPedido: json['idPedido'] as String?,
    id: json['id'] as int,
  );
}

Map<String, dynamic> _$CarrinhoToJson(Carrinho instance) => <String, dynamic>{
      'distribuidorId': instance.distribuidorId,
      'distribuidor': instance.distribuidor,
      'clienteId': instance.clienteId,
      'cliente': instance.cliente,
      'formaPagamentoId': instance.formaPagamentoId,
      'formaPagamento': instance.formaPagamento,
      'condicaoPagamentoId': instance.condicaoPagamentoId,
      'condicaoPagamento': instance.condicaoPagamento,
      'tabelaPrecoId': instance.tabelaPrecoId,
      'tabelaPreco': instance.tabelaPreco,
      'dataEntrega': instance.dataEntrega,
      'total': instance.total,
      'usuario': instance.usuario,
      'finalizado': instance.finalizado,
      'enviado': instance.enviado,
      'idPedido': instance.idPedido,
      'id': instance.id,
    };
