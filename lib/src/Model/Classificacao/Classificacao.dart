import 'package:json_annotation/json_annotation.dart';

part 'Classificacao.g.dart';

@JsonSerializable()

class Classificacao {
  String? nome;
  int? codigo;
  int id;

  Classificacao({
    this.nome,
    this.codigo,
    required this.id
  });

  factory Classificacao.fromJson(Map<String, dynamic> json) => _$ClassificacaoFromJson(json);

  Map<String, dynamic> toJson() => _$ClassificacaoToJson(this);
}