// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Classificacao.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Classificacao _$ClassificacaoFromJson(Map<String, dynamic> json) {
  return Classificacao(
    nome: json['nome'] as String?,
    codigo: json['codigo'] as int?,
    id: json['id'] as int,
  );
}

Map<String, dynamic> _$ClassificacaoToJson(Classificacao instance) =>
    <String, dynamic>{
      'nome': instance.nome,
      'codigo': instance.codigo,
      'id': instance.id,
    };
