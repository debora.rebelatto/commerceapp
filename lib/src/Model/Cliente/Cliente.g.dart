// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Cliente.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Cliente _$ClienteFromJson(Map<String, dynamic> json) {
  return Cliente(
    pessoaId: json['pessoaId'] as int?,
    pessoa: json['pessoa'] == null
        ? null
        : Pessoa.fromJson(json['pessoa'] as Map<String, dynamic>),
    tipoEstabelecimentoId: json['tipoEstabelecimentoId'] as int?,
    tipoEstabelecimento: json['tipoEstabelecimento'] == null
        ? null
        : TipoEstabelecimento.fromJson(
            json['tipoEstabelecimento'] as Map<String, dynamic>),
    condicaoPagamentoId: json['condicaoPagamentoId'] as int?,
    condicaoPagamento: json['condicaoPagamento'] == null
        ? null
        : CondicaoPagamento.fromJson(
            json['condicaoPagamento'] as Map<String, dynamic>),
    formaPagamentoId: json['formaPagamentoId'] as int?,
    formaPagamento: json['formaPagamento'] == null
        ? null
        : FormaPagamento.fromJson(
            json['formaPagamento'] as Map<String, dynamic>),
    codigo: json['codigo'] as int?,
    id: json['id'] as int,
  );
}

Map<String, dynamic> _$ClienteToJson(Cliente instance) => <String, dynamic>{
      'pessoaId': instance.pessoaId,
      'pessoa': instance.pessoa,
      'tipoEstabelecimentoId': instance.tipoEstabelecimentoId,
      'tipoEstabelecimento': instance.tipoEstabelecimento,
      'condicaoPagamentoId': instance.condicaoPagamentoId,
      'condicaoPagamento': instance.condicaoPagamento,
      'formaPagamentoId': instance.formaPagamentoId,
      'formaPagamento': instance.formaPagamento,
      'codigo': instance.codigo,
      'id': instance.id,
    };
