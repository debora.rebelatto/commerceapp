import 'package:json_annotation/json_annotation.dart';
import 'package:commerce_app/src/Model/CondicaoPagamento/CondicaoPagamento.dart';
import 'package:commerce_app/src/Model/FormaPagamento/FormaPagamento.dart';
import 'package:commerce_app/src/Model/Pessoa/Pessoa.dart';
import 'package:commerce_app/src/Model/TipoEstabelecimento/TipoEstabelecimento.dart';

part 'Cliente.g.dart';

@JsonSerializable()

class Cliente {
  int? pessoaId;
  Pessoa? pessoa;

  int? tipoEstabelecimentoId;
  TipoEstabelecimento? tipoEstabelecimento;

  int? condicaoPagamentoId;
  CondicaoPagamento? condicaoPagamento;

  int? formaPagamentoId;
  FormaPagamento? formaPagamento;

  int? codigo;
  int id;

  Cliente({
    this.pessoaId,
    this.pessoa,

    this.tipoEstabelecimentoId,
    this.tipoEstabelecimento,

    this.condicaoPagamentoId,
    this.condicaoPagamento,

    this.formaPagamentoId,
    this.formaPagamento,

    this.codigo,
    required this.id
  });

  factory Cliente.fromJson(Map<String, dynamic> json) => _$ClienteFromJson(json);
  Map<String, dynamic> toJson() => _$ClienteToJson(this);
}