// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'TipoEstabelecimento.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TipoEstabelecimento _$TipoEstabelecimentoFromJson(Map<String, dynamic> json) {
  return TipoEstabelecimento(
    tabelapreco: json['tabelapreco'] == null
        ? null
        : TabelaPreco.fromJson(json['tabelapreco'] as Map<String, dynamic>),
    codigo: json['codigo'] as int?,
    nome: json['nome'] as String?,
    id: json['id'] as int,
  );
}

Map<String, dynamic> _$TipoEstabelecimentoToJson(
        TipoEstabelecimento instance) =>
    <String, dynamic>{
      'tabelapreco': instance.tabelapreco,
      'nome': instance.nome,
      'codigo': instance.codigo,
      'id': instance.id,
    };
