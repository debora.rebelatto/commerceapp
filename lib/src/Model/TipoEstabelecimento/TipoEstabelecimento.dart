import 'package:json_annotation/json_annotation.dart';
import 'package:commerce_app/src/Model/TabelaPreco/TabelaPreco.dart';

part 'TipoEstabelecimento.g.dart';

@JsonSerializable()

class TipoEstabelecimento {
  TabelaPreco? tabelapreco;
  String? nome;
  int? codigo;
  int id;

  TipoEstabelecimento ({
    this.tabelapreco,
    this.codigo,
    this.nome,
    required this.id,
  });

  factory TipoEstabelecimento.fromJson(Map<String, dynamic> json) => _$TipoEstabelecimentoFromJson(json);

  Map<String, dynamic> toJson() => _$TipoEstabelecimentoToJson(this);
}
