import 'package:json_annotation/json_annotation.dart';

part 'Classe.g.dart';

@JsonSerializable()

class Classe {
  String? nome;
  int? codigo;
  int id;

  Classe({
    this.nome,
    this.codigo,
    required this.id
  });

  factory Classe.fromJson(Map<String, dynamic> json) => _$ClasseFromJson(json);

  Map<String, dynamic> toJson() => _$ClasseToJson(this);
}