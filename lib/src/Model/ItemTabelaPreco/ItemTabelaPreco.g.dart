// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ItemTabelaPreco.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ItemTabelaPreco _$ItemTabelaPrecoFromJson(Map<String, dynamic> json) {
  return ItemTabelaPreco(
    id: json['id'] as int,
    tabelaPrecoId: json['tabelaPrecoId'] as int?,
    tabelaPreco: json['tabelaPreco'] == null
        ? null
        : TabelaPreco.fromJson(json['tabelaPreco'] as Map<String, dynamic>),
    produtoId: json['produtoId'] as int?,
    produto: json['produto'] == null
        ? null
        : Produto.fromJson(json['produto'] as Map<String, dynamic>),
    valorVenda: (json['valorVenda'] as num?)?.toDouble(),
  );
}

Map<String, dynamic> _$ItemTabelaPrecoToJson(ItemTabelaPreco instance) =>
    <String, dynamic>{
      'id': instance.id,
      'tabelaPrecoId': instance.tabelaPrecoId,
      'tabelaPreco': instance.tabelaPreco,
      'produtoId': instance.produtoId,
      'produto': instance.produto,
      'valorVenda': instance.valorVenda,
    };
