import 'package:json_annotation/json_annotation.dart';
import 'package:commerce_app/src/Model/TabelaPreco/TabelaPreco.dart';
import '../Produto/Produto.dart';

part 'ItemTabelaPreco.g.dart';

@JsonSerializable()

class ItemTabelaPreco {
  int id;
  int? tabelaPrecoId;
  TabelaPreco? tabelaPreco;
  int? produtoId;
  Produto? produto;
  double? valorVenda;

  ItemTabelaPreco({
    required this.id,
    this.tabelaPrecoId,
    this.tabelaPreco,
    this.produtoId,
    this.produto,
    this.valorVenda
  });

  factory ItemTabelaPreco.fromJson(Map<String, dynamic> json) => _$ItemTabelaPrecoFromJson(json);

  Map<String, dynamic> toJson() => _$ItemTabelaPrecoToJson(this);
}