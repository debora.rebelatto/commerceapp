// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'FormaPagamento.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FormaPagamento _$FormaPagamentoFromJson(Map<String, dynamic> json) {
  return FormaPagamento(
    nome: json['nome'] as String?,
    codigo: json['codigo'] as int?,
    id: json['id'] as int,
  );
}

Map<String, dynamic> _$FormaPagamentoToJson(FormaPagamento instance) =>
    <String, dynamic>{
      'nome': instance.nome,
      'codigo': instance.codigo,
      'id': instance.id,
    };
