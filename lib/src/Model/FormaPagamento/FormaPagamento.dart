import 'package:json_annotation/json_annotation.dart';

part 'FormaPagamento.g.dart';

@JsonSerializable()

class FormaPagamento {
  String? nome;
  int? codigo;
  int id;

  FormaPagamento ({
    this.nome,
    this.codigo,
    required this.id
  });

  factory FormaPagamento.fromJson(Map<String, dynamic> json) => _$FormaPagamentoFromJson(json);

  Map<String, dynamic> toJson() => _$FormaPagamentoToJson(this);
}