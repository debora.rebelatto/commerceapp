import 'package:json_annotation/json_annotation.dart';
import 'package:commerce_app/src/Model/Bairro/Bairro.dart';
import 'package:commerce_app/src/Model/Cidade/Cidade.dart';
import 'package:commerce_app/src/Model/PessoaFisica/PessoaFisica.dart';
import 'package:commerce_app/src/Model/PessoaJuridica/PessoaJuridica.dart';

part 'Pessoa.g.dart';

@JsonSerializable()

class Pessoa {
  String? tipo;
  int? bairroId;
  Bairro? bairro;
  int? cidadeId;
  Cidade? cidade;
  String? endereco;
  String? complemento;
  String? cep;
  String? telefone;
  PessoaFisica? pessoaFisica;
  PessoaJuridica? pessoaJuridica;
  int id;

  Pessoa({
    this.tipo,
    this.bairro,
    this.cidade,
    this.cep,
    this.telefone,
    this.pessoaFisica,
    this.pessoaJuridica,
    required this.id,
  });

  factory Pessoa.fromJson(Map<String, dynamic> json) => _$PessoaFromJson(json);

  Map<String, dynamic> toJson() => _$PessoaToJson(this);

}
