// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Pessoa.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Pessoa _$PessoaFromJson(Map<String, dynamic> json) {
  return Pessoa(
    tipo: json['tipo'] as String?,
    bairro: json['bairro'] == null
        ? null
        : Bairro.fromJson(json['bairro'] as Map<String, dynamic>),
    cidade: json['cidade'] == null
        ? null
        : Cidade.fromJson(json['cidade'] as Map<String, dynamic>),
    cep: json['cep'] as String?,
    telefone: json['telefone'] as String?,
    pessoaFisica: json['pessoaFisica'] == null
        ? null
        : PessoaFisica.fromJson(json['pessoaFisica'] as Map<String, dynamic>),
    pessoaJuridica: json['pessoaJuridica'] == null
        ? null
        : PessoaJuridica.fromJson(
            json['pessoaJuridica'] as Map<String, dynamic>),
    id: json['id'] as int,
  )
    ..bairroId = json['bairroId'] as int?
    ..cidadeId = json['cidadeId'] as int?
    ..endereco = json['endereco'] as String?
    ..complemento = json['complemento'] as String?;
}

Map<String, dynamic> _$PessoaToJson(Pessoa instance) => <String, dynamic>{
      'tipo': instance.tipo,
      'bairroId': instance.bairroId,
      'bairro': instance.bairro,
      'cidadeId': instance.cidadeId,
      'cidade': instance.cidade,
      'endereco': instance.endereco,
      'complemento': instance.complemento,
      'cep': instance.cep,
      'telefone': instance.telefone,
      'pessoaFisica': instance.pessoaFisica,
      'pessoaJuridica': instance.pessoaJuridica,
      'id': instance.id,
    };
