import 'package:json_annotation/json_annotation.dart';

part 'PessoaFisica.g.dart';

@JsonSerializable()

class PessoaFisica {
  int? pessoaId;
  String? cpf;
  String? rg;
  String? nome;
  String? apelido;
  int id;

  PessoaFisica({
    this.pessoaId,
    this.cpf,
    this.rg,
    this.nome,
    this.apelido,
    required this.id,
  });

  factory PessoaFisica.fromJson(Map<String, dynamic> json) => _$PessoaFisicaFromJson(json);

  Map<String, dynamic> toJson() => _$PessoaFisicaToJson(this);

}
