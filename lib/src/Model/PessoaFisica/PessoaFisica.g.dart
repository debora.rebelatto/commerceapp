// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'PessoaFisica.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PessoaFisica _$PessoaFisicaFromJson(Map<String, dynamic> json) {
  return PessoaFisica(
    pessoaId: json['pessoaId'] as int?,
    cpf: json['cpf'] as String?,
    rg: json['rg'] as String?,
    nome: json['nome'] as String?,
    apelido: json['apelido'] as String?,
    id: json['id'] as int,
  );
}

Map<String, dynamic> _$PessoaFisicaToJson(PessoaFisica instance) =>
    <String, dynamic>{
      'pessoaId': instance.pessoaId,
      'cpf': instance.cpf,
      'rg': instance.rg,
      'nome': instance.nome,
      'apelido': instance.apelido,
      'id': instance.id,
    };
