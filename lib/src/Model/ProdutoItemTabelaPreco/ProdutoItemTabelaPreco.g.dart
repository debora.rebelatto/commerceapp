// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ProdutoItemTabelaPreco.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProdutoItemTabelaPreco _$ProdutoItemTabelaPrecoFromJson(
    Map<String, dynamic> json) {
  return ProdutoItemTabelaPreco(
    produto: Produto.fromJson(json['produto'] as Map<String, dynamic>),
    itemTabelaPreco: json['itemTabelaPreco'] == null
        ? null
        : ItemTabelaPreco.fromJson(
            json['itemTabelaPreco'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$ProdutoItemTabelaPrecoToJson(
        ProdutoItemTabelaPreco instance) =>
    <String, dynamic>{
      'produto': instance.produto,
      'itemTabelaPreco': instance.itemTabelaPreco,
    };
