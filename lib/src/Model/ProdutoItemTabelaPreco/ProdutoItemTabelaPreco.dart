import 'package:json_annotation/json_annotation.dart';
import '../Produto/Produto.dart';
import 'package:commerce_app/src/Model/ItemTabelaPreco/ItemTabelaPreco.dart';

part 'ProdutoItemTabelaPreco.g.dart';

@JsonSerializable()

class ProdutoItemTabelaPreco {
  Produto produto;
  ItemTabelaPreco? itemTabelaPreco;

  ProdutoItemTabelaPreco({
    required this.produto,
    this.itemTabelaPreco,
  });

  factory ProdutoItemTabelaPreco.fromJson(Map<String, dynamic> json) => _$ProdutoItemTabelaPrecoFromJson(json);

  Map<String, dynamic> toJson() => _$ProdutoItemTabelaPrecoToJson(this);
}