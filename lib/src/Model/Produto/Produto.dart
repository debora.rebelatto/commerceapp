import 'package:json_annotation/json_annotation.dart';
import 'package:commerce_app/src/Model/Classe/Classe.dart';
import 'package:commerce_app/src/Model/Grupos/Grupos.dart';
import 'package:commerce_app/src/Model/Marcas/Marcas.dart';
import 'package:commerce_app/src/Model/Classificacao/Classificacao.dart';

part 'Produto.g.dart';

@JsonSerializable()

class Produto {
  String? barras;

  int? classeId;
  Classe? classe;

  int? classificacaoId;
  Classificacao? classificacao;

  int? unidadeVendaId;
  // UnidadeVenda unidadeVenda;

  String? descricaoEmbalagem;
  String? especificacao;
  String? nomeComercial;
  String? referencia;
  String? composicao;
  String? informacaoAdicional;
  String? codigoRegistro;
  double? pesoBruto;
  double? pesoLiquido;
  int? quantidadePadrao;
  int? quantidadeMultipla;
  String? ncm;
  String? cest;
  String? dun;
  int? quantidadeDun;

  int? marcaId;
  Marcas? marca;

  int? categoriaProdutoId;
  // CategoriaProduto categoriaProduto;

  int? tipoProdutoId;
  // TipoProduto tipoProduto;

  String? dataCadastro;

  int? grupoId;
  Grupos? grupo;

  int? listaDcbId;
  // VendaDcb vendaDcb;

  bool? vendaControlada;
  String? nome;
  int? codigo;
  int id;

  Produto({
    this.barras,
    this.classeId,
    this.classe,
    this.classificacaoId,
    // this.classificaco,
    this.unidadeVendaId,
    // this.unidadeVenda,
    this.descricaoEmbalagem,
    this.especificacao,
    this.nomeComercial,
    this.referencia,
    this.composicao,
    this.informacaoAdicional,
    this.codigoRegistro,
    this.pesoBruto,
    this.pesoLiquido,
    this.quantidadePadrao,
    this.quantidadeMultipla,
    this.ncm,
    this.cest,
    this.dun,
    this.quantidadeDun,
    this.marcaId,
    this.marca,
    this.dataCadastro,
    this.grupoId,
    this.grupo,
    this.listaDcbId,
    this.vendaControlada,
    this.nome,
    this.codigo,
    required this.id
  });

  factory Produto.fromJson(Map<String, dynamic> json) => _$ProdutoFromJson(json);

  Map<String, dynamic> toJson() => _$ProdutoToJson(this);
}