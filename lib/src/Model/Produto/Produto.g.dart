// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Produto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Produto _$ProdutoFromJson(Map<String, dynamic> json) {
  return Produto(
    barras: json['barras'] as String?,
    classeId: json['classeId'] as int?,
    classe: json['classe'] == null
        ? null
        : Classe.fromJson(json['classe'] as Map<String, dynamic>),
    classificacaoId: json['classificacaoId'] as int?,
    unidadeVendaId: json['unidadeVendaId'] as int?,
    descricaoEmbalagem: json['descricaoEmbalagem'] as String?,
    especificacao: json['especificacao'] as String?,
    nomeComercial: json['nomeComercial'] as String?,
    referencia: json['referencia'] as String?,
    composicao: json['composicao'] as String?,
    informacaoAdicional: json['informacaoAdicional'] as String?,
    codigoRegistro: json['codigoRegistro'] as String?,
    pesoBruto: (json['pesoBruto'] as num?)?.toDouble(),
    pesoLiquido: (json['pesoLiquido'] as num?)?.toDouble(),
    quantidadePadrao: json['quantidadePadrao'] as int?,
    quantidadeMultipla: json['quantidadeMultipla'] as int?,
    ncm: json['ncm'] as String?,
    cest: json['cest'] as String?,
    dun: json['dun'] as String?,
    quantidadeDun: json['quantidadeDun'] as int?,
    marcaId: json['marcaId'] as int?,
    marca: json['marca'] == null
        ? null
        : Marcas.fromJson(json['marca'] as Map<String, dynamic>),
    dataCadastro: json['dataCadastro'] as String?,
    grupoId: json['grupoId'] as int?,
    grupo: json['grupo'] == null
        ? null
        : Grupos.fromJson(json['grupo'] as Map<String, dynamic>),
    listaDcbId: json['listaDcbId'] as int?,
    vendaControlada: json['vendaControlada'] as bool?,
    nome: json['nome'] as String?,
    codigo: json['codigo'] as int?,
    id: json['id'] as int,
  )
    ..classificacao = json['classificacao'] == null
        ? null
        : Classificacao.fromJson(json['classificacao'] as Map<String, dynamic>)
    ..categoriaProdutoId = json['categoriaProdutoId'] as int?
    ..tipoProdutoId = json['tipoProdutoId'] as int?;
}

Map<String, dynamic> _$ProdutoToJson(Produto instance) => <String, dynamic>{
      'barras': instance.barras,
      'classeId': instance.classeId,
      'classe': instance.classe,
      'classificacaoId': instance.classificacaoId,
      'classificacao': instance.classificacao,
      'unidadeVendaId': instance.unidadeVendaId,
      'descricaoEmbalagem': instance.descricaoEmbalagem,
      'especificacao': instance.especificacao,
      'nomeComercial': instance.nomeComercial,
      'referencia': instance.referencia,
      'composicao': instance.composicao,
      'informacaoAdicional': instance.informacaoAdicional,
      'codigoRegistro': instance.codigoRegistro,
      'pesoBruto': instance.pesoBruto,
      'pesoLiquido': instance.pesoLiquido,
      'quantidadePadrao': instance.quantidadePadrao,
      'quantidadeMultipla': instance.quantidadeMultipla,
      'ncm': instance.ncm,
      'cest': instance.cest,
      'dun': instance.dun,
      'quantidadeDun': instance.quantidadeDun,
      'marcaId': instance.marcaId,
      'marca': instance.marca,
      'categoriaProdutoId': instance.categoriaProdutoId,
      'tipoProdutoId': instance.tipoProdutoId,
      'dataCadastro': instance.dataCadastro,
      'grupoId': instance.grupoId,
      'grupo': instance.grupo,
      'listaDcbId': instance.listaDcbId,
      'vendaControlada': instance.vendaControlada,
      'nome': instance.nome,
      'codigo': instance.codigo,
      'id': instance.id,
    };
