// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Bairro.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Bairro _$BairroFromJson(Map<String, dynamic> json) {
  return Bairro(
    nome: json['nome'] as String?,
    codigo: json['codigo'] as int?,
    id: json['id'] as int,
  );
}

Map<String, dynamic> _$BairroToJson(Bairro instance) => <String, dynamic>{
      'nome': instance.nome,
      'codigo': instance.codigo,
      'id': instance.id,
    };
