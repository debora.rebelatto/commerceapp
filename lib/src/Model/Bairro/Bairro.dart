import 'package:json_annotation/json_annotation.dart';

part 'Bairro.g.dart';

@JsonSerializable()

class Bairro {
  String? nome;
  int? codigo;
  int id;

  Bairro({
    this.nome,
    this.codigo,
    required this.id
  });

  factory Bairro.fromJson(Map<String, dynamic> json) => _$BairroFromJson(json);

  Map<String, dynamic> toJson() => _$BairroToJson(this);
}