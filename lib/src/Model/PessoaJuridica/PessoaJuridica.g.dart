// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'PessoaJuridica.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PessoaJuridica _$PessoaJuridicaFromJson(Map<String, dynamic> json) {
  return PessoaJuridica(
    pessoaId: json['pessoaId'] as int?,
    cnpj: json['cnpj'] as String?,
    ie: json['ie'] as String?,
    razaoSocial: json['razaoSocial'] as String?,
    fantasia: json['fantasia'] as String?,
    id: json['id'] as int,
  );
}

Map<String, dynamic> _$PessoaJuridicaToJson(PessoaJuridica instance) =>
    <String, dynamic>{
      'pessoaId': instance.pessoaId,
      'cnpj': instance.cnpj,
      'ie': instance.ie,
      'razaoSocial': instance.razaoSocial,
      'fantasia': instance.fantasia,
      'id': instance.id,
    };
