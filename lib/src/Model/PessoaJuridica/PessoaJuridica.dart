import 'package:json_annotation/json_annotation.dart';

part 'PessoaJuridica.g.dart';

@JsonSerializable()

class PessoaJuridica {
  int? pessoaId;
  String? cnpj;
  String? ie;
  String? razaoSocial;
  String? fantasia;
  int id;

  PessoaJuridica({
    this.pessoaId,
    this.cnpj,
    this.ie,
    this.razaoSocial,
    this.fantasia,
    required this.id,
  });

  factory PessoaJuridica.fromJson(Map<String, dynamic> json) => _$PessoaJuridicaFromJson(json);

  Map<String, dynamic> toJson() => _$PessoaJuridicaToJson(this);
}
