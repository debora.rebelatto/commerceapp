// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Cidade.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Cidade _$CidadeFromJson(Map<String, dynamic> json) {
  return Cidade(
    estadoId: json['estadoId'] as int?,
    estado: json['estado'] == null
        ? null
        : Estado.fromJson(json['estado'] as Map<String, dynamic>),
    codigoFiscal: json['codigoFiscal'] as String?,
    latitude: (json['latitude'] as num?)?.toDouble(),
    longitude: (json['longitude'] as num?)?.toDouble(),
    nome: json['nome'] as String?,
    codigo: json['codigo'] as int?,
    id: json['id'] as int,
  );
}

Map<String, dynamic> _$CidadeToJson(Cidade instance) => <String, dynamic>{
      'estadoId': instance.estadoId,
      'estado': instance.estado,
      'codigoFiscal': instance.codigoFiscal,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
      'nome': instance.nome,
      'codigo': instance.codigo,
      'id': instance.id,
    };
