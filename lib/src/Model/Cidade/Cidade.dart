import 'package:json_annotation/json_annotation.dart';
import 'package:commerce_app/src/Model/Estado/Estado.dart';

part 'Cidade.g.dart';

@JsonSerializable()

class Cidade {
  int? estadoId;
  Estado? estado;
  String? codigoFiscal;
  double? latitude;
  double? longitude;
  String? nome;
  int? codigo;
  int id;

  Cidade({
    this.estadoId,
    this.estado,
    this.codigoFiscal,
    this.latitude,
    this.longitude,
    this.nome,
    this.codigo,
    required this.id
  });

  factory Cidade.fromJson(Map<String, dynamic> json) => _$CidadeFromJson(json);

  Map<String, dynamic> toJson() => _$CidadeToJson(this);
}