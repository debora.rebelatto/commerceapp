import 'package:json_annotation/json_annotation.dart';

part 'CondicaoPagamento.g.dart';

@JsonSerializable()

class CondicaoPagamento {
  String? nome;
  int? codigo;
  int id;

  CondicaoPagamento ({
    this.nome,
    this.codigo,
    required this.id
  });

  factory CondicaoPagamento.fromJson(Map<String, dynamic> json) => _$CondicaoPagamentoFromJson(json);

  Map<String, dynamic> toJson() => _$CondicaoPagamentoToJson(this);
}

