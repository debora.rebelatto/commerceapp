// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'CondicaoPagamento.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CondicaoPagamento _$CondicaoPagamentoFromJson(Map<String, dynamic> json) {
  return CondicaoPagamento(
    nome: json['nome'] as String?,
    codigo: json['codigo'] as int?,
    id: json['id'] as int,
  );
}

Map<String, dynamic> _$CondicaoPagamentoToJson(CondicaoPagamento instance) =>
    <String, dynamic>{
      'nome': instance.nome,
      'codigo': instance.codigo,
      'id': instance.id,
    };
