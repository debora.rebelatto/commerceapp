// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ItensCarrinho.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ItensCarrinho _$ItensCarrinhoFromJson(Map<String, dynamic> json) {
  return ItensCarrinho(
    carrinho: Carrinho.fromJson(json['carrinho'] as Map<String, dynamic>),
    itemcarrinho: (json['itemcarrinho'] as List<dynamic>?)
        ?.map((e) => ItemCarrinho.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$ItensCarrinhoToJson(ItensCarrinho instance) =>
    <String, dynamic>{
      'carrinho': instance.carrinho,
      'itemcarrinho': instance.itemcarrinho,
    };
