import 'package:json_annotation/json_annotation.dart';

import '../Carrinho/Carrinho.dart';
import '../ItemCarrinho/ItemCarrinho.dart';

part 'ItensCarrinho.g.dart';

@JsonSerializable()

class ItensCarrinho {
  Carrinho carrinho;
  List<ItemCarrinho>? itemcarrinho;

  ItensCarrinho({
    required this.carrinho,
    this.itemcarrinho
  });

  factory ItensCarrinho.fromJson(Map<String, dynamic> json) => _$ItensCarrinhoFromJson(json);

  Map<String, dynamic> toJson() => _$ItensCarrinhoToJson(this);
}
