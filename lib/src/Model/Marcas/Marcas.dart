import 'package:json_annotation/json_annotation.dart';

part 'Marcas.g.dart';

@JsonSerializable()

class Marcas {
  String? nome;
  int? codigo;
  int id;

  Marcas({
    this.nome,
    this.codigo,
    required this.id
  });

  factory Marcas.fromJson(Map<String, dynamic> json) => _$MarcasFromJson(json);
  Map<String, dynamic> toJson() => _$MarcasToJson(this);
}