// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Marcas.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Marcas _$MarcasFromJson(Map<String, dynamic> json) {
  return Marcas(
    nome: json['nome'] as String?,
    codigo: json['codigo'] as int?,
    id: json['id'] as int,
  );
}

Map<String, dynamic> _$MarcasToJson(Marcas instance) => <String, dynamic>{
      'nome': instance.nome,
      'codigo': instance.codigo,
      'id': instance.id,
    };
