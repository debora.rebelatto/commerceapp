// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'TabelaPreco.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TabelaPreco _$TabelaPrecoFromJson(Map<String, dynamic> json) {
  return TabelaPreco(
    id: json['id'] as int,
    codigo: json['codigo'] as int?,
  )
    ..distribuidorId = json['distribuidorId'] as int?
    ..nome = json['nome'] as String?;
}

Map<String, dynamic> _$TabelaPrecoToJson(TabelaPreco instance) =>
    <String, dynamic>{
      'distribuidorId': instance.distribuidorId,
      'nome': instance.nome,
      'codigo': instance.codigo,
      'id': instance.id,
    };
