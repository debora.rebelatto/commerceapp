import 'package:json_annotation/json_annotation.dart';

part 'TabelaPreco.g.dart';

@JsonSerializable()

class TabelaPreco {
  int? distribuidorId;
  String? nome;
  int? codigo;
  int id;

  TabelaPreco ({
    required this.id,
    this.codigo,
  });

  factory TabelaPreco.fromJson(Map<String, dynamic> json) => _$TabelaPrecoFromJson(json);

  Map<String, dynamic> toJson() => _$TabelaPrecoToJson(this);
}