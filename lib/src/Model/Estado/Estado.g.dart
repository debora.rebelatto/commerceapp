// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Estado.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Estado _$EstadoFromJson(Map<String, dynamic> json) {
  return Estado(
    codigo: json['codigo'] as int?,
    nome: json['nome'] as String?,
    sigla: json['sigla'] as String?,
    pais: json['pais'] as int?,
    id: json['id'] as int,
  );
}

Map<String, dynamic> _$EstadoToJson(Estado instance) => <String, dynamic>{
      'codigo': instance.codigo,
      'nome': instance.nome,
      'sigla': instance.sigla,
      'pais': instance.pais,
      'id': instance.id,
    };
