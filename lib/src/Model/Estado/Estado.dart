import 'package:json_annotation/json_annotation.dart';

part 'Estado.g.dart';

@JsonSerializable()

class Estado {
  int? codigo;
  String? nome;
  String? sigla;
  int? pais;
  int id;

  Estado({
    this.codigo,
    this.nome,
    this.sigla,
    this.pais,
    required this.id
  });

  factory Estado.fromJson(Map<String, dynamic> json) => _$EstadoFromJson(json);
  Map<String, dynamic> toJson() => _$EstadoToJson(this);
}