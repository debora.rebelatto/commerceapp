import 'package:json_annotation/json_annotation.dart';

part 'Grupos.g.dart';

@JsonSerializable()

class Grupos {
  String? nome;
  int? codigo;
  int id;

  Grupos({
    this.nome,
    this.codigo,
    required this.id
  });

  factory Grupos.fromJson(Map<String, dynamic> json) => _$GruposFromJson(json);
  Map<String, dynamic> toJson() => _$GruposToJson(this);
}