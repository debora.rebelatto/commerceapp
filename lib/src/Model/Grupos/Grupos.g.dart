// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Grupos.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Grupos _$GruposFromJson(Map<String, dynamic> json) {
  return Grupos(
    nome: json['nome'] as String?,
    codigo: json['codigo'] as int?,
    id: json['id'] as int,
  );
}

Map<String, dynamic> _$GruposToJson(Grupos instance) => <String, dynamic>{
      'nome': instance.nome,
      'codigo': instance.codigo,
      'id': instance.id,
    };
