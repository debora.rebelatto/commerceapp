
import 'package:json_annotation/json_annotation.dart';

part 'Configuracao.g.dart';

@JsonSerializable()

class Configuracao {
  int? distribuidorId;
  int? tipoPedidoId;
  String? tipoMenuHome;
  int id;

  Configuracao({
    this.distribuidorId,
    this.tipoPedidoId,
    this.tipoMenuHome,
    required this.id
  });

  factory Configuracao.fromJson(Map<String, dynamic> json) => _$ConfiguracaoFromJson(json);

  Map<String, dynamic> toJson() => _$ConfiguracaoToJson(this);
}
