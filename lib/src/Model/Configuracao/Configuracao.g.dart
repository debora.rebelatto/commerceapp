// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Configuracao.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Configuracao _$ConfiguracaoFromJson(Map<String, dynamic> json) {
  return Configuracao(
    distribuidorId: json['distribuidorId'] as int?,
    tipoPedidoId: json['tipoPedidoId'] as int?,
    tipoMenuHome: json['tipoMenuHome'] as String?,
    id: json['id'] as int,
  );
}

Map<String, dynamic> _$ConfiguracaoToJson(Configuracao instance) =>
    <String, dynamic>{
      'distribuidorId': instance.distribuidorId,
      'tipoPedidoId': instance.tipoPedidoId,
      'tipoMenuHome': instance.tipoMenuHome,
      'id': instance.id,
    };
