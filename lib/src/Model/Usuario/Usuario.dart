import 'package:json_annotation/json_annotation.dart';

import 'package:commerce_app/src/Model/Pessoa/Pessoa.dart';

part 'Usuario.g.dart';

@JsonSerializable()

class Usuario {
  int id;
  String? nome;
  int? pessoaId;
  Pessoa? pessoa;
  String? email;
  String? senha;

  Usuario({
    required this.id,
    this.pessoa,
    this.nome,
    this.pessoaId,
    this.email,
    this.senha
  });

  factory Usuario.fromJson(Map<String, dynamic> json) => _$UsuarioFromJson(json);

  Map<String, dynamic> toJson() => _$UsuarioToJson(this);

}
