// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Usuario.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Usuario _$UsuarioFromJson(Map<String, dynamic> json) {
  return Usuario(
    id: json['id'] as int,
    pessoa: json['pessoa'] == null
        ? null
        : Pessoa.fromJson(json['pessoa'] as Map<String, dynamic>),
    nome: json['nome'] as String?,
    pessoaId: json['pessoaId'] as int?,
    email: json['email'] as String?,
    senha: json['senha'] as String?,
  );
}

Map<String, dynamic> _$UsuarioToJson(Usuario instance) => <String, dynamic>{
      'id': instance.id,
      'nome': instance.nome,
      'pessoaId': instance.pessoaId,
      'pessoa': instance.pessoa,
      'email': instance.email,
      'senha': instance.senha,
    };
