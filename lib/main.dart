import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:commerce_app/src/Templates/provider/FinalizeData.dart';
import 'package:commerce_app/src/CommerceApp.dart';

dynamic main() async {
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => FinalizeData()),
      ],
      child: CommerceApp(),
    ),
  );
}