#!/bin/bash

GREEN=`tput setaf 2`
reset=`tput sgr0`

PS3='Please enter your choice: '
options=(
  "Build Apk Release"
  "Build web no renderer"
  "Build web HTML renderer"
  "Build web CanvasKit renderer"
  "Quit"
)

select opt in "${options[@]}"
do
  case $opt in
    "Build Apk Release")
      echo "${GREEN}https://flutter.dev/docs/deployment/android ${reset}"
      echo "${GREEN}Release Apk can be found at ~/build/app/outputs/flutter-apk ${reset}"
      flutter build apk --release
      echo "${GREEN}Done!"
      break
      ;;

    "Build web no renderer")
      echo "${GREEN}https://flutter.dev/docs/deployment/web ${reset}"
      flutter build web --release
      echo "${GREEN}Done!"
      break
      ;;

    "Build web HTML renderer")
      echo "${GREEN}https://flutter.dev/docs/development/tools/web-renderers ${reset}"
      flutter build web --web-renderer canvaskit
      echo "${GREEN}Done!"
      break
      ;;

    "Build web CanvasKit renderer")
      echo "${GREEN}https://flutter.dev/docs/development/tools/web-renderers ${reset}"
      flutter build web --web-renderer canvaskit
      echo "${GREEN}Done!"
      break
      ;;

    "Quit")
      break
      ;;
    *) echo "invalid option $REPLY";;
  esac
done