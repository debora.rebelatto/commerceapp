#!/bin/bash

GREEN=`tput setaf 2`
reset=`tput sgr0`

# Choose the auto option (default) if you are optimizing for
# download size on mobile browsers and optimizing for performance on desktop browsers.

# Choose the html option if you are optimizing download size over performance on both
# desktop and mobile browsers.

# Choose the canvaskit option if you are prioritizing performance and pixel-perfect
# consistency on both desktop and mobile browsers.

PS3='Please enter your choice: '
options=(
  "HTML Renderer"
  "CanvasKit Renderer"
  "Quit"
)

select opt in "${options[@]}"
do
  case $opt in
    "HTML Renderer")
      echo "${GREEN}https://flutter.dev/docs/development/tools/web-renderers ${reset}"
      flutter run -d chrome --web-renderer html
      break
      ;;

    "CanvasKit Renderer")
      echo "${GREEN}https://flutter.dev/docs/development/tools/web-renderers ${reset}"
      flutter run -d chrome --web-renderer canvaskit
      break
      ;;

    "Quit")
      break
      ;;
    *) echo "invalid option $REPLY";;
  esac
done
